<?php
include('common.php');
if (!$cache_setting['web_state']) die('站点已关闭');
$cache_category = cache::get('category');
$cache_category_arr = cache::get('category_arr');
$cache_article_category = cache::get('article_category');
$cache_article_category_arr = cache::get('article_category_arr');
$cache_userlevel = cache::get('userlevel');
$cache_menu = cache::get('menu');
$cache_ad = cache::get('ad');
$cache_link = cache::get('link');
pe_lead('hook/ad.hook.php');
pe_lead('hook/category.hook.php');
pe_lead('hook/wechat.hook.php');

$user = pe_login('user');
$user_wx_openid = pe_dbhold($_GET['user_openid']);
$user_lbs = explode(',', pe_dbhold($_g_user_lbs));
$user['user_lat'] = floatval($user_lbs[0]);
$user['user_lng'] = floatval($user_lbs[1]);


if (is_file("{$pe['path']}module/{$module}/{$mod}.php")) {
	include("{$pe['path']}module/{$module}/{$mod}.php");
}
else {
	pe_404();
}
pe_result();
?>