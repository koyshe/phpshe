<?php
include("../../../../common.php");
require_once("API/qqConnectAPI.php");
$qc = new QC();
$access_token = $qc->qq_callback();
//获取openid
$openid = $qc->get_openid();
//获取qq资料
$qc = new QC($access_token, $openid);
$qqinfo = $qc->get_user_info();
//获取unionid
$json = json_decode(pe_curl_get("https://graph.qq.com/oauth2.0/me?access_token={$access_token}&unionid=1&fmt=json"), true);
$unionid = $json['unionid'];
//为h5端获取sessid
$arr = explode('@', $_g_state);
if ($arr[1]) $_GET['sessid'] = $arr[1];

$login3 = pe_get_session('login3');
$login3 = is_array($login3) ? $login3 : array();
$login3['user_name'] = $qqinfo['nickname'];
$login3['user_logo'] = $qqinfo['figureurl_qq_2'];
$login3['user_qq_openid'] = $openid;
$login3['user_qq_unionid'] = $unionid;
$login3['from'] = 'qq';
pe_set_session('login3', $login3);

//手机H5登录回调
if (pe_mobile()) {
	pe_goto(pe_url("/page/user/do_login3", 'app'));
	//pe_back_uniapp('/page/user/do_login3');
}
//网页扫码登录回调
else {
	pe_goto("{$pe['host']}user.php?mod=do&act=login3");
}