<?php
include("../../../../../common.php");
require_once("../../API/qqConnectAPI.php");
$qc = new QC();
$access_token = $qc->qq_callback();
//获取openid
$openid = $qc->get_openid();
//获取qq资料
$qc = new QC($access_token, $openid);
$qqinfo = $qc->get_user_info();
//获取unionid
$json = json_decode(pe_curl_get("https://graph.qq.com/oauth2.0/me?access_token={$access_token}&unionid=1&fmt=json"), true);
$unionid = $json['unionid'];
//检测用户是否存在
$user = $db->pe_select('user', array('user_id'=>pe_dbhold($unionid)));
if ($user['user_id']) {
	//$sql_set['user_qq_token'] = $access_token;
	//$sql_set['user_logo'] = $qqinfo["figureurl_qq_2"];
	//$db->pe_update('user', array('user_id'=>$info['user_id']), pe_dbhold($sql_set));
	user_login_callback('login', $user['user_id']);
	pe_success('登录成功！', $_s_from);
}
else {
	//$sql_set['user_qq_openid'] = $openid;
	//$sql_set['user_qq_unionid'] = $unionid;
	$sql_set['user_name'] = user_jsname($qqinfo["nickname"]);
	$sql_set['user_logo'] = $qqinfo["figureurl_qq_2"];
	$sql_set['user_ip'] = pe_ip();
	$sql_set['user_atime'] = time();
	if ($user_id = $db->pe_insert('user', pe_dbhold($sql_set))) {
		user_login_callback('reg', $user_id);
		pe_success('登录成功！', $_s_from);
	}
	else {
		pe_error('登录失败', "{$pe['host']}user.php");
	}
}
