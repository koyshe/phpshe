<?php
include('../../../../common.php');
pe_lead('include/class/upload.class.php');

if ($_g_action == 'config') {
	$config = json_decode(preg_replace("/\/\*[\s\S]+?\*\//", "", file_get_contents("config.json")), true);
	pe_apidata($config);
}

$upload = new upload($_FILES['file'], 'data/attachment/'.date('Y-m').'/');
if ($upload->error) {
	$result['state'] = 'error';
}
else {
	$result['state'] = 'SUCCESS';
	$result['url'] = $upload->filehost_full;
}
$result['title'] = $upload->filename;
$result['original'] = $upload->filename_old;
$result['type'] = $upload->filetail;
$result['size'] = $upload->file['size'];

pe_apidata($result);

/*
//先上传到本地服务器
$upload = new upload($_FILES['file'], 'data/attachment/'.date('Y-m').'/');
if ($upload->error) pe_apidata(array('state'=>$upload->error));
if ($cache_setting['upload_server'] == 'aliyun') {
	$config['accessid'] = $cache_setting['upload_aliyun_accesskey_id'];
	$config['accesskey'] = $cache_setting['upload_aliyun_accesskey_secret'];
	$config['url'] = trim($cache_setting['upload_aliyun_domain'], '/').'/';
	//设置policy参数（过期时间+文件大小+文件名限制）
	$policy_param['expiration'] = gmt_iso8601(time() + 1800);
	//最大文件大小.用户可以自己设置
	$policy_param['conditions'][] = array(0=>'content-length-range', 1=>0, 2=>1048576*500);
	// 表示用户上传的数据，必须是以$dir开始，不然上传会失败，这一步不是必须项，只是为了安全起见，防止用户通过policy上传到别人的目录。
	//$policy['conditions'][] = array(0=>'starts-with', 1=>'$key', 2=>$dir);
	$policy = base64_encode(json_encode($policy_param));
	$sign = base64_encode(hash_hmac('sha1', $policy, $config['accesskey'], true));
	$post['OSSAccessKeyId'] = $config['accessid'];
	$post['policy'] = $policy;
	$post['Signature'] = $sign;
	$post['key'] = $upload->filehost;
	$post['success_action_status'] = '200';
	//$post['x-oss-forbid-overwrite'] = 1;
	$post['ETag'] = '';
	$post['name'] = $upload->filename_old;
	$post['file'] = "@{$upload->filepath_full}";
	$post['filetype'] = $upload->filetail;
	if (pe_curl_post($config['url'], $post)) {
		$result['state'] = 'error';
	}
	else {
		$result['state'] = 'SUCCESS';
	}
	$result['url'] = $config['url'].$upload->filehost;
	unlink($upload->filepath_full);
}
else {
	$result['state'] = 'SUCCESS';
	$result['url'] = $upload->filehost_full;
}
$result['title'] = $upload->filename;
$result['original'] = $upload->filename_old;
$result['type'] = $upload->filetail;
$result['size'] = $upload->file['size'];

pe_apidata($result);



function gmt_iso8601($time)
{
    $dtStr = date("c", $time);
    $mydatetime = new DateTime($dtStr);
    $expiration = $mydatetime->format(DateTime::ISO8601);
    $pos = strpos($expiration, '+');
    $expiration = substr($expiration, 0, $pos);
    return $expiration."Z";
}
*/