/**
 * @copyright   2008-2019 简好网络 <http://www.phpshe.com>
 * @creatdate   2019-0916 koyshe <koyshe@gmail.com>
 */
(function($){
	$.fn.pe_upload = function(my_option) {
		var _this = this;
		var upload_idarr = [];
		_this.find(".upload_box,.upload_file_box").each(function(){
			var upload_id = 'upload_' + parseInt((Math.random() + 1) * Math.pow(10,20-1));
			upload_idarr.push(upload_id);
			if ($(this).find(".upload_btn").length) {
				$(this).append('<a href="javascript:;" class="upload_del"></a><div class="upload_jindu"></div>');			
			}
			else {
				$(this).append('<div class="upload_btn"><i></i></div><a href="javascript:;" class="upload_del"></a><div class="upload_jindu"></div>');			
			}
			$(this).find(".upload_btn").attr("id", upload_id);
			if ($(this).find(".upload_val").val() == '') {
				$(this).find(".upload_img").hide();
				$(this).find(".upload_del").hide();
			}
			else {
				$(this).find(".upload_img").show();
				$(this).find(".upload_del").show();
			}
			//修复文件上传删除按钮位置
			if ($(this).hasClass("upload_file_box")) {
				var width = $(this).find(".upload_val").innerWidth();
				$(this).find(".upload_del").css("left", (width-19)+'px');
			}
		})
		$.fn.pe_upload.defaults = {
			'btn': '.upload_btn',
			'upload_type' : 'image',
			'filetype': ''
		};
		var _this = this;
		var option = $.extend({}, $.fn.pe_upload.defaults, my_option);
		var filetype_arr = {};
		filetype_arr.image = {'extensions': 'gif,jpg,jpeg,bmp,png', 'mimetypes' : 'image/*'};
		filetype_arr.video = {'extensions': 'mp4,flv,m3u8', 'mimetypes' : 'video/*'};
		filetype_arr.audio = {'extensions': 'mp3', 'mimetypes' : 'audio/*'};
		//实例化一个plupload上传对象
		var uploader = new plupload.Uploader({
			browse_button : upload_idarr,
			url : "include/plugin/plupload/local/upload.php",
			flash_swf_url : "include/plugin/plupload/local/upload.swf",
			//silverlight_xap_url : 'js/Moxie.xap',
			filters: {
				mime_types : [
					{title : "图片文件", extensions : filetype_arr.image.extensions},
					{title : "视频文件", extensions : filetype_arr.video.extensions},
					{title : "音频文件", extensions : filetype_arr.audio.extensions}
				]
			},
			multipart_params: {
				filetype : option.filetype
			}
		});
		//初始化
		uploader.init();
		//绑定添加队列
		uploader.bind('FilesAdded',function(uploader,files){
			for (var i = 0; i < files.length; i++){
				if (_this.find(".upload_box,.upload_file_box").has(".upload_del:hidden").length == 0) break;
				//_this.find(".upload_box,.upload_file_box").has(".upload_del:hidden").eq(0).attr("id", files[i].id).find(".upload_del").show();
				_this.find(".upload_box,.upload_file_box").has(".upload_del:hidden").eq(0).attr("id", files[i].id).find(".upload_del").show();
				if ($("#"+files[i].id).hasClass("upload_file_box")) {
					$("#"+files[i].id).find(".upload_jindu").show().html("（上传中 0%）").css("width", '0%');				
				}
				else {
					$("#"+files[i].id).find(".upload_jindu").show().html("（上传中 0%）");				
				}
			}
			uploader.start(); //开始上传
		});
		//绑定上传进度
		uploader.bind('UploadProgress',function(uploader,file){
			if ($("#"+file.id).hasClass("upload_file_box")) {
				var bili = $("#"+file.id).find(".upload_val").innerWidth() * file.percent / 100;
				$("#"+file.id).find(".upload_jindu").html('（上传中' + file.percent + '%）').css("width", bili + 'px');				
			}
			else {
				$("#"+file.id).find(".upload_jindu").html('（上传中' + file.percent + '%）');			
			}
		})
		//绑定上传完成
		uploader.bind('FileUploaded',function(uploader,file,json){
			var json = JSON.parse(json.response);
			var fileimg = json.data.fileimg;
			var fileurl = json.data.fileurl;	
			$("#"+file.id).find(".upload_img").attr("src", fileimg).show();
			$("#"+file.id).find(".upload_val").val(fileurl);
			$("#"+file.id).find(".upload_del").show();
			$("#"+file.id).find(".upload_jindu").hide();
			//$("#"+file.id).find(".upload_jindu").html('上传成功');
			/*setTimeout(function(){
				$("#"+file.id).find(".upload_jindu").hide();
			}, 1000)*/
		})
		//删除
		_this.find(".upload_del").click(function(){
			var upload_box = $(this).parents(".upload_box,.upload_file_box");
			upload_box.find(".upload_img").attr("src", "").hide();
			upload_box.find(".upload_val").val('');
			upload_box.find(".upload_del").hide();
			upload_box.find(".upload_jindu").hide();
		})
	}	
})(jQuery);