<?php
//plupload上传接口
include('../../../../common.php');
pe_lead('include/class/upload.class.php');
if ($_FILES['file']['size']) {
	switch ($_REQUEST['filetype']) {
		case 'avatar':	
			$upload = new upload($_FILES['file'], 'data/attachment/avatar/'.date('Y-m').'/');	
			$data['fileimg'] = pe_thumb($upload->filehost, 100, 100, 'avatar');
			$data['fileurl'] = $upload->filehost;
		break;
		case 'brand':	
			$upload = new upload($_FILES['file'], 'data/attachment/brand/');
			$data['fileimg'] = pe_thumb($upload->filehost);
			$data['fileurl'] = $upload->filehost;
		break;
		case 'video':	
			$upload = new upload($_FILES['file'], 'data/attachment/video/'.date('Y-m').'/');
			$data['fileimg'] = "{$pe['host']}include/plugin/upload/images/video.jpg";
			$data['fileurl'] = $upload->filehost;
		break;
		default:
			$upload = new upload($_FILES['file'], 'data/attachment/'.date('Y-m').'/');
			$data['fileimg'] = pe_thumb($upload->filehost);
			$data['fileurl'] = $upload->filehost;
		break;
	}
	if ($upload->error) {
		pe_apidata(array('code'=>0, 'msg'=>$upload->error));
	}
	else {
		pe_apidata(array('code'=>1, 'data'=>$data));
	}
}
?>