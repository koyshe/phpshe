<?php
//plupload上传接口
include('../../../common.php');
$upload_server = $cache_setting['upload_server'];
switch ($upload_server) {
	case 'aliyun':
		$config['accessid'] = $cache_setting['upload_aliyun_accesskey_id'];
		$config['accesskey'] = $cache_setting['upload_aliyun_accesskey_secret'];
		$config['url'] = trim($cache_setting['upload_aliyun_domain'], '/').'/';
		$config['callbackurl'] = "{$pe['host']}include/plupload/aliyun/aliyun.php?act=callback";
		//设置callback
		$callback_param['callbackUrl'] = $config['callbackurl'];
		$callback_param['callbackBody'] = 'filename=${object}&size=${size}&mimeType=${mimeType}&height=${imageInfo.height}&width=${imageInfo.width}';
		$callback_param['callbackBodyType'] = "application/x-www-form-urlencoded";
		$callback = base64_encode(json_encode($callback_param));
		//设置policy参数（过期时间+文件大小+文件名限制）
		$policy_param['expiration'] = gmt_iso8601(time() + 1800);
		//最大文件大小.用户可以自己设置
		$policy_param['conditions'][] = array(0=>'content-length-range', 1=>0, 2=>1048576*500);
		// 表示用户上传的数据，必须是以$dir开始，不然上传会失败，这一步不是必须项，只是为了安全起见，防止用户通过policy上传到别人的目录。
		//$policy['conditions'][] = array(0=>'starts-with', 1=>'$key', 2=>$dir);
		$policy = base64_encode(json_encode($policy_param));
		//$string_to_sign = $base64_policy;
		$sign = base64_encode(hash_hmac('sha1', $policy, $config['accesskey'], true));

		$result = array();
		$result['accessid'] = $config['accessid'];
		$result['url'] = $config['url'];
		$result['policy'] = $policy;
		$result['sign'] = $sign;
		//$result['expire'] = $end;
		$result['callback'] = $callback;
		$filename = filename(urldecode($_GET['filename']));
		$filetype = _filetype($_GET['filename']);
		if ($filetype == 'image') {
			$result['filename'] = 'data/attachment/'.date('Y-m').'/'.$filename;		
			$result['fileurl'] = $result['url'] . $result['filename'];
			$result['fileimg'] = $result['fileurl'];		
		}
		else {
			$result['filename'] = 'data/video/'.date('Y-m').'/'.$filename;		
			$result['fileurl'] = $result['url'] . $result['filename'];
			$result['fileimg'] = "{$result['fileurl']}?x-oss-process=video/snapshot,t_1,f_jpg,w_800,h_800,m_fast,ar_auto";
		}
		pe_apidata($result);
	break;
	default:
		$info['url'] = "{$pe['host']}include/plugin/plupload/{$cache_setting['upload_server']}/upload.php";
		pe_apidata(array('code'=>1, 'data'=>$info));
	break;
}


function gmt_iso8601($time)
{
    $dtStr = date("c", $time);
    $mydatetime = new DateTime($dtStr);
    $expiration = $mydatetime->format(DateTime::ISO8601);
    $pos = strpos($expiration, '+');
    $expiration = substr($expiration, 0, $pos);
    return $expiration."Z";
}

//上传文件重命名
function filename($filename)
{
	$rand_arr = array_merge(range('a','z'),range('A','Z'),range(0,1));
	$name_tmp1 = $rand_arr[array_rand($rand_arr, 1)].$rand_arr[array_rand($rand_arr, 1)].$rand_arr[array_rand($rand_arr, 1)];
	$name_tmp2 = microtime().rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9);
	$name_tmp3 = base64_encode($filename).uniqid(mt_rand(), true);
	$name_tmp = md5($name_tmp1.$name_tmp2.$name_tmp3);
	return date("YmdHis") . '_' .$name_tmp . '.' . filetail($filename);
}

//获取文件扩展名
function filetail($filename)
{
	$filearr = explode('.', $filename);
	return strtolower($filearr[count($filearr) - 1]);
}

//获取文件类型
function _filetype($filename) 
{
	$filetail = filetail($filename);
	if (in_array($filetail, array('jpeg', 'jpg', 'gif', 'png', 'bmp'))) {
		return 'image';
	}
	elseif (in_array($filetail, array('mp4', 'm3u8', 'flv'))) {
		return 'video';
	}
}

?>