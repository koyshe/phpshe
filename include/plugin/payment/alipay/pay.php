<?php
include('../../../../common.php');
include('func.php');
$config = alipay_config();;

$info = $db->pe_select('pay', array('pay_id'=>pe_dbhold($_g_id)));
$post['app_id'] = $config['alipay_appid'];
$post['method'] = $_g_from == 'h5' ? 'alipay.trade.wap.pay' : 'alipay.trade.page.pay';
$post['format'] = 'JSON';
$post['return_url'] = "{$pe['host']}include/plugin/payment/alipay/return_url.php";
$post['charset'] = 'utf-8';
$post['sign_type'] = 'RSA2';
$post['timestamp'] = date('Y-m-d H:i:s');
$post['version'] = '1.0';
$post['biz_content']['notify_url'] = "{$pe['host']}include/plugin/payment/alipay/notify_url.php";
$post['biz_content']['subject'] = $info['order_name'];
$post['biz_content']['out_trade_no'] = $info['pay_id'];
$post['biz_content']['timeout_express'] = '48h';
$post['biz_content']['total_amount'] = $info['order_money'];
$post['biz_content']['quit_url'] = $pe['host'];//手机版专属参数
$post['biz_content']['product_code'] = 'FAST_INSTANT_TRADE_PAY';
$post['sign'] = alipay_sign($post);

$html = "<form action='https://openapi.alipay.com/gateway.do?charset=utf-8' method='post' id='form'>";
foreach ($post as $k=>$v) {
	if (is_array($v)) $v = json_encode($v);
	$html .= "<input type='hidden' name='{$k}' value='{$v}'/>";
}
$html .= "<input type='submit' style='display:none;''></form><script>document.forms['form'].submit();</script>";
echo $html;
