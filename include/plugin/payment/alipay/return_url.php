<?php
include('../../../../common.php');
include('func.php');
$cache_payment = cache::get('payment');
$config = $cache_payment['alipay']['payment_config'];

$info = $db->pe_select('pay', array('pay_id'=>pe_dbhold($_g_out_trade_no)));
$data['app_id'] = $config['alipay_appid'];
$data['method'] = 'alipay.trade.query';
$data['format'] = 'JSON';
$data['charset'] = 'utf-8';
$data['sign_type'] = 'RSA2';
$data['timestamp'] = date('Y-m-d H:i:s');
$data['version'] = '1.0';
$data['biz_content']['out_trade_no'] = $info['pay_id'];
$data['sign'] = alipay_sign($data);

foreach ($data as $k=>$v) {
	if ($k == 'biz_content') continue;
	$arg .= "{$k}=".urlencode($v)."&";
}
$arg = trim($arg, '&');
$post = "biz_content=".urlencode(json_encode($data['biz_content']));

$json = json_decode(pe_curl_post("https://openapi.alipay.com/gateway.do?{$arg}", $post), true);

if ($json['alipay_trade_query_response']['code'] == 10000 && $json['alipay_trade_query_response']['msg'] == 'Success') {
	$order_outid = pe_dbhold($json['alipay_trade_query_response']['trade_no']);
	pay_success($info['pay_id'], 'alipay', $order_outid);
	//手机H5
	if (pe_mobile()) {
		pe_goto(pe_url(pay_jump($info['pay_id']), 'app'));
		//pe_back_uniapp(pay_jump($info['pay_id']));
	}
	//网页
	else {
		pe_success('支付成功', pay_jump($info['pay_id']));
	}	
}
else {
	$msg = $json['alipay_trade_query_response']['sub_msg'];
	//手机H5
	if (pe_mobile()) {
		pe_goto(pe_url('/page/index/404?msg='+urlencode($msg), 'app'));
		//pe_back_uniapp('/page/index/404?msg='+urlencode($msg));
	}
	//网页
	else {
		pe_404($msg);
	}
}