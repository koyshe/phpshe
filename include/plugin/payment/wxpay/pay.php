<?php
include('../../../../common.php');
pe_lead('hook/wechat.hook.php');
$user = pe_login('user');
$pay_id = pe_dbhold($_g_id);
switch ($_g_from) {
	case 'h5':
		pe_apidata(wechat_h5pay($pay_id));
	break;
	case 'h5_wx':
		pe_apidata(wechat_jspay($pay_id));
	break;
	case 'minapp':
		pe_apidata(wechat_jspay($pay_id, true));
	break;
	default:
		$info = $db->pe_select('pay', array('pay_id'=>$pay_id));
		$json = wechat_pcpay($info['pay_id']);
		$info['qrcode'] = $json['qrcode'];
		if ($json['code'] == 0) pe_404($json['msg']);
$html = <<<html
	<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>微信扫码支付</title>
	<script type="text/javascript" src="{$pe['host']}include/js/jquery.js"></script>
	<script type="text/javascript" src="{$pe['host']}include/js/global.js"></script>
	<script type="text/javascript" src="{$pe['host']}include/plugin/layer/layer.js"></script>
	</head>
	<body style="background:#2E3B41">
		<div class="sm_box">
			<div><img src="{$info['qrcode']}" /></div>
			<p>请用微信扫描二维码完成支付</p>
			<div class="smje">支付金额：<span>¥ {$info['order_money']}</span></div>
		</div>
		<style type="text/css">
		.sm_box{width:380px; margin:70px auto 0; background:#fff; border-radius:4px; text-align:center; height:480px; font-family:微软雅黑}
		.sm_box img{width:270px; height:270px; margin-top:50px}
		.sm_box p{font-size:22px; color:#666}
		.smje{font-size:18px; color:#666}
		.smje span{color:#ff6600; font-size:22px;}
		.layui-layer{width:160px; !important}
		</style>
		<script type="text/javascript">
		$(function(){
			setInterval(function(){
				pe_get("{$pe['host']}include/plugin/payment/wxpay/pay_result.php?id={$info['pay_id']}", function(json){
					if (json.code == 1) {
						layer.msg(json.msg, {icon: 1});
						setTimeout(function(){
							pe_open(json.data.url);
						}, 2000)
					}
				})
			}, 3000);
		})
		</script>
	</body>
	</html>
html;
		echo $html;
	break;
}