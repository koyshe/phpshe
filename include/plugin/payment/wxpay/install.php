<?php
$config['name'] = '微信支付';
$config['type'] = 'wxpay';
$config['desc'] = '实现微信PC扫码支付/H5支付/公众号支付/小程序支付（v2版本）';
$config['model']['wxpay_appid']['name'] = '公众号AppID';
$config['model']['wxpay_appid']['type'] = 'text';
$config['model']['wxpay_minapp_appid']['name'] = '小程序AppID';
$config['model']['wxpay_minapp_appid']['type'] = 'text';
$config['model']['wxpay_mchid']['name'] = '商户号';
$config['model']['wxpay_mchid']['type'] = 'text';
$config['model']['wxpay_apikey']['name'] = 'API密钥';
$config['model']['wxpay_apikey']['type'] = 'text';
return $config;