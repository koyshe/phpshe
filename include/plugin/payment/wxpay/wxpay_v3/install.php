<?php
$config['name'] = '微信支付';
$config['type'] = 'wxpay';
$config['desc'] = '实现微信PC扫码支付/H5支付/公众号支付/小程序支付（v3版本）';
$config['model']['wxpay_appid']['name'] = '公众号AppID';
$config['model']['wxpay_appid']['type'] = 'text';
$config['model']['wxpay_minapp_appid']['name'] = '小程序AppID';
$config['model']['wxpay_minapp_appid']['type'] = 'text';
$config['model']['wxpay_mchid']['name'] = '商户号';
$config['model']['wxpay_mchid']['type'] = 'text';
$config['model']['wxpay_my_private_key']['name'] = 'API证书私钥';
$config['model']['wxpay_my_private_key']['type'] = 'textarea';
$config['model']['wxpay_apisn']['name'] = 'API证书序号';
$config['model']['wxpay_apisn']['type'] = 'text';
$config['model']['wxpay_apikey']['name'] = 'APIv3密钥';
$config['model']['wxpay_apikey']['type'] = 'text';
return $config;