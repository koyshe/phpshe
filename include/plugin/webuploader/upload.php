<?php
//webuploader上传接口
include('../../../common.php');
if ($_FILES['file']['size']) {
	pe_lead('include/class/upload.class.php');
	if ($_REQUEST['uptype'] == 'avatar') {
		$upload = new upload($_FILES['file'], 'data/avatar/'.date('Y-m').'/');	
		echo json_encode(array('code'=>1, 'img'=>pe_thumb($upload->filehost, '_200', '_200', 'avatar'), 'val'=>$upload->filehost));
	}
	elseif ($_REQUEST['uptype'] == 'video') {
		$upload = new upload($_FILES['file'], 'data/video/'.date('Y-m').'/');	
		echo json_encode(array('code'=>1, 'img'=>"{$pe['host']}include/plugin/webuploader/images/video.jpg", 'val'=>$upload->filehost));
	}
	else {
		$upload = new upload($_FILES['file'], 'data/attachment/'.date('Y-m').'/');
		echo json_encode(array('code'=>1, 'bigimg'=>pe_thumb($upload->filehost), 'img'=>pe_thumb($upload->filehost), 'val'=>$upload->filehost));
	}
}
?>