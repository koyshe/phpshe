/**
 * @copyright   2008-2015 简好网络 <http://www.phpshe.com>
 * @creatdate   2010-1001 koyshe <koyshe@gmail.com>
 */
(function($){

	$.fn.pe_upload = function(my_option) {
		$.fn.pe_upload.defaults = {     
			'client': 'pc',    
			'num': 1,
			'btn': '.upload_btn',
			'filetype' : 'image',
			'type': ''
		};
		var _this = this;
		var option = $.extend({}, $.fn.pe_upload.defaults, my_option);
		var filetype_arr = {};
		filetype_arr.image = {'extensions': 'gif,jpg,jpeg,bmp,png', 'mimetypes' : 'image/*'}
		filetype_arr.video = {'extensions': 'mp4,flv', 'mimetypes' : 'video/*'}
		// 初始化Web Uploader
		var uploader = WebUploader.create({
		    // 自动上传。
		    auto: true,
		    // swf文件路径
		    swf: "include/plugin/webuploader/Uploader.swf",
		    // 文件接收服务端。
		    server: "include/plugin/webuploader/upload.php?uptype="+option.type,	
		    // 选择文件的按钮。可选。
		    // 内部根据当前运行是创建，可能是input元素，也可能是flash.
		    pick: {id:option.btn, multiple:(option.num == 1 ? false : true)},
		    fileNumLimit : option.num,
		    // 只允许选择文件，可选。
		    accept: {
		        title: '请选择要上传的文件',
		        //extensions: 'gif,jpg,jpeg,bmp,png',
		        //mimeTypes: 'image/*'
		        extensions: filetype_arr[option.filetype].extensions,
		        mimeTypes: filetype_arr[option.filetype].mimetypes
		    }
		});
		// 当有文件添加进来的时候
		uploader.on('fileQueued', function( file ) {

			if (option.num > 1) {
				if (_this.find(".upload_html[lock=0]:eq(0)").length == 0) {
					uploader.removeFile( file ,true);
				}
				_this.find(".upload_html[lock=0]:eq(0)").attr("id", file.id).attr("lock", 1);		
			}
			else {
				$("#rt_"+file.source.ruid).parents(".upload_html").attr("id", file.id);		
			}
		    $("#"+file.id).find(".upload_jindu").show().html("（上传中 0%）");
		});
		// 文件上传过程中创建进度条实时显示。
		uploader.on('uploadProgress', function( file, percentage ) {
		    $("#"+file.id).find(".upload_jindu").html('（上传中' + parseInt(percentage * 100) + '%）' );
		});
		// 文件上传成功，给item添加成功class, 用样式标记上传成功。
		uploader.on('uploadSuccess', function( file, response) {
		    if (response.result == true) {
		        $("#"+file.id).find(".upload_jindu").html('（上传成功）').hide();
		        $("#"+file.id).find(".upload_logo").attr("src", response.img);
		        $("#"+file.id).find(".upload_value").val(response.val);
		        $("#"+file.id).find(".upload_bg").show();
		        if (option.client == 'app') $("#"+file.id).find(".upload_appdel").show();
		        //$("#"+file.id).find(".upload_do").show();
		    }
		    else {
		        $("#"+file.id).attr('lock', 0);
		        $("#"+file.id).find(".upload_jindu").html('（上传失败）');
		        $("#"+file.id).find(".upload_bg").hide();
		        if (option.client == 'app') $("#"+file.id).find(".upload_appdel").hide();
		        //$("#"+file.id).find(".upload_do").hide();
		    }
		});
		//文件上传失败，现实上传出错。
		uploader.on('uploadError', function(file){
		    $("#"+file.id).attr('lock', 0);
		    $("#"+file.id).find(".upload_jindu").html('（上传失败）');
		    $("#"+file.id).find(".upload_bg").hide();
		    if (option.client == 'app') $("#"+file.id).find(".upload_appdel").hide();
		    //$("#"+file.id).find(".upload_do").hide();
		});
		//所有文件上次完成
		uploader.on('uploadFinished', function(){
			uploader.reset();
		})
		//初始化
		_this.find(".upload_value").each(function(){
			var upload_html = $(this).parents(".upload_html");
			if ($(this).val()) {
				upload_html.attr("lock", 1);
				upload_html.find(".upload_bg").show();
				if (option.client == 'app') upload_html.find(".upload_appdel").show();		
				//upload_html.find(".upload_do").show();
			}
			else {
				upload_html.attr("lock", 0);
		 		upload_html.find(".upload_bg").hide();
		 		if (option.client == 'app') upload_html.find(".upload_appdel").hide();
				//upload_html.find(".upload_do").hide();
			}
		})
		//显示操作
		_this.find(".upload_html").mouseenter(function(){
			if (option.client == 'app') return false;
			if ($(this).find(".upload_bg").is(":visible")) {
				$(this).find(".upload_do").show();
			}
		}).mouseleave(function(){
			if (option.client == 'app') return false;
		    $(".upload_do").hide();
		});
		/*$(".upload_html").click(function(){
			if (client == 'pc') return false;
			if ($(this).find(".upload_bg").is(":visible")) {
				$(this).find(".upload_do").show();
			}
		})*/
		//左移
		_this.find(".upload_left").live("click", function(){
			if (option.num == 1) return false;
		    var current = $(this).parents(".upload_html");
		    var prev = current.prev();
		    if (current.index() > 0) {
		    	current.find(".upload_do").hide();
		    	current.insertBefore(prev);
		    }
		})
		//右移
		_this.find(".upload_right").live("click", function(){
			if (option.num == 1) return false;
		    var current = $(this).parents(".upload_html");
		    var next = $(this).parents(".upload_html").next();
		    if (current.index() < $(".upload_html", _this).length - 1) {
		    	current.find(".upload_do").hide();
		    	current.insertAfter(next);
		    }
		})
		//删除
		_this.find(".upload_del, .upload_appdel").click(function(){
			var upload_html = $(this).parents(".upload_html");
		    upload_html.attr("lock", 0);
			if (option.filetype == 'video') {
		    	upload_html.find(".upload_logo").attr("src", "include/plugin/webuploader/images/up_video.jpg");		
			}
			else {
				upload_html.find(".upload_logo").attr("src", "include/plugin/webuploader/images/up_bg.jpg");		
			}
		    upload_html.find(".upload_value").val('');
		    upload_html.find(".upload_bg").hide();
		    upload_html.find(".upload_do").hide();
		 	if (option.client == 'app') upload_html.find(".upload_appdel").hide();
		})
	}
})(jQuery);