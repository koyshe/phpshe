var rule_phone = /^1[0-9]{10}$/;

//获取url全局参数
function pe_urlarg(url, other_arg) {
	var arg = 'client=h5';
	if (typeof(other_arg) != 'undefined') arg = arg + other_arg;
	return url = (url.indexOf("?") != -1) ? url+'&'+arg : url+'?'+arg;
}

//数字处理
function pe_num(num, type, len, fix) {
	if (typeof(len) == 'undefined') len = 0;
	if (typeof(fix) == 'undefined') fix = false;
	var pow = Math.pow(10, len);	
	var num = parseFloat(num);
	if (isNaN(num)) num = 0;
	if (type == 'round') {
		num = Math.round(num * pow) / pow;
	}
	else if (type == 'ceil') {
		num = Math.ceil(num * pow) / pow;
	}
	else if (type == 'floor') {
		num = Math.floor(num * pow) / pow;
	}
	if (fix == true) {
		num = num.toFixed(len);
	}
	return num;
}

//打开页面
function app_open(url, time) {
	if (typeof(time) == 'undefined') time = 1;
	setTimeout(function(){
		if (url == 'back') {
			window.history.go(-1);
		}
		else if (url == 'reload') {
			window.location.reload();
		}
		else if (url == 'dialog') {
			top.location.reload();
		}
		else {
			window.location.href = url;		
		}
	}, time);
}

//ajax获取信息
function app_getinfo(url, func, page) {
	var arg = '';
	if (typeof(page) == 'undefined') page = 0;
	if (page) {
		if (page == 1) {
			var page_more = true;
			_vue.data.list = [];
			//_vue.data.lists = [];
		}
		else {
			var page_more = (typeof(_vue.data.page_more) == 'undefined' ? true : _vue.data.page_more);		
		}
		_vue.data.page = page;
		_vue.data.page_more = page_more;
		_vue.data.nodata = false;
		arg += '&page=' + page;
		if (page_more == false) return;
	}
	url = pe_urlarg(url, arg);
	axios.get(url).then(function (json) {
		json = json.data;
		/*if (json.result == false && json.code == 'nologin') {
			if (_this.route.indexOf('page/user/') >= 0) {
				app_open('/page/index/wxlogin/wxlogin', 0 , true);
			}
			else {
				app_open('/page/index/wxlogin/wxlogin');				
			}
			return false;
		}*/
		if (page >= 1) {
    		if (json.list == null || json.list.length == 0) {
				_vue.data.page_more = false;
				if (page == 1) {
					_vue.data.nodata = true;				
				}
    		}
    		else {
    			
    		//	console.log(JSON.parse(JSON.stringify(_vue.data.list)).concat(json.list))
    			
			//	_vue.data.list = JSON.parse(JSON.stringify(_vue.data.list)).concat(json.list);
	//	console.log(_vue.data.list);
				_vue.data.page = page + 1;
				if (json.list.length < 10) {
					_vue.data.page_more = false;   			
				}
    		}			
		}
    	if (func && typeof(func) == "function") {
    		func(json);
		}
	}).catch(function (json) {
		app_alert(json);
	});
}

//ajax表单post提交
function app_submit(url, func) {
	app_tip('处理中', 'loading');
//	if (typeof(e.detail.formId) != 'undefined') app_getinfo(host+"api.php?mod=wechat&act=add_formid&value="+e.detail.formId);
	setTimeout(function(){
		//var url = e.currentTarget.dataset.action;
		//var arg = 'client=minapp&user_openid='+app_getval('user_openid')+'&user_lbs='+app_getval('user_lbs')+'&city='+app_getval('city');
		//console.log(url);
		url = pe_urlarg(url);
		var user_sessid = app_getval('user_sessid');
		var header = {};
		header['content-type'] = 'application/x-www-form-urlencoded';
		if (user_sessid) header['Cookie'] = 'PHPSESSID='+user_sessid;
    	var form_data = new FormData();		
		var form = document.getElementById("form").getElementsByTagName("input");
		console.log(form)
		for (var i in form) {
			if (isNaN(parseInt(i)) || form[i].name == '' || form[i].name == 'undefined') continue;
			form_data.append(form[i].name,form[i].value);
		}
        axios({
		    method:"post",
		    url:url,
		    headers: {
			  "Content-Type": "multipart/form-data"
		    },
		    withCredentials:true,
		    data:form_data
		}).then(function(json){
					app_tip_close();
					json = json.data;
					if (json.result == false && json.code == 'nologin') {
						app_open('/page/index/wxlogin/wxlogin');
						return false;
					}
					if (typeof(json.show) != 'undefined' && json.show != '') {
						if (json.result == true) {
							app_tip(json.show, 'success');
						}
						else {
							app_tip(json.show);
						}
					}
			    	if (func && typeof(func) == "function") {
			    		func(json);
			    	}	
		
		}).catch(function (json) {
			app_alert(json);
		});     

		
	}, 100)
}

//监听滚动到底部
function app_scrollmore(func) {
	window.scrollbottom = 0;
	window.onscroll = function() {
   		//变量scrollTop是滚动条滚动时，距离顶部的距离
   		var scrollTop = document.documentElement.scrollTop || document.body.scrollTop;
   		//变量windowHeight是可视区的高度
   		var windowHeight = document.documentElement.clientHeight || document.body.clientHeight;
   		//变量scrollHeight是滚动条的总高度
   		var scrollHeight = document.documentElement.scrollHeight || document.body.scrollHeight;

console.log(scrollTop + windowHeight + '---' + scrollHeight);
console.log(scrollTop + windowHeight + '---+' + window.scrollbottom);
        //滚动条到底部的条件
        if (scrollTop + windowHeight >= scrollHeight - 30 && scrollTop + windowHeight >= window.scrollbottom) {
	

	
			//防抖动重复执行
			window.scrollbottom = scrollHeight + 10;
			console.log(scrollHeight + '====' + window.scrollbottom + '----' + document.documentElement.scrollHeight);
			func();
        }
	}
}

//tip提示信息
function app_tip(show, type, time) {
	app_tip_close();
	if (typeof(show) == 'undefined' || show == '') return;
	if (type == 'success') {
		vant.Toast.success(show)
	}
	else if (type == 'loading') {
		if (typeof(time) == 'undefined') time = 10000;
		vant.Toast.loading({
			message: show,
			forbidClick: true,
			duration: time
		});
	}
	else {
		vant.Toast(show);
	}
};

//tip关闭信息
function app_tip_close() {
	vant.Toast.clear();
} 

//alert确认框
function app_alert(show, func) {
	vant.Dialog.alert({
	  message: show
	}).then(() => {
		if (func && typeof(func) == "function") {
			func();
		}
	});
}

//确认提醒
function app_confirm(show, func_url) {
	vant.Dialog.confirm({
		message: '您确认'+show+'吗?',
	}).then(() => {
    	if (func_url && typeof(func_url) == "function") {
			func_url();
		}
		else if (func_url) {
			app_getinfo(func_url, function(json){
				if (typeof(json.show) != 'undefined' && json.show != '') {
					if (json.result) {
						app_tip(json.show, 'success');			
					}
					else {
						app_tip(json.show);						
					}
				}
				if (json.result) {
					app_open('reload', 1000);
				}
			})
		}
	}).catch(() => {

	});
}

//存值
function app_setval(name, val) {
	localStorage.setItem(name, val);
}

//取值
function app_getval(name) {
	return localStorage.getItem(name);
}

//发送短信验证码
//const yzm_waittime = 0;
//const yzm_timeout = '';
function app_sendyzm(url, func) {
	if (window.yzm_state == false) return false;
	if (typeof(window.yzm_waittime) != 'undefined' && window.yzm_waittime > 0) return false;
	window.yzm_state = false;
	_vue.yzm_show = '发送中...';
	app_getinfo(url, function(json){
		if (typeof(json.show) != 'undefined' && json.show != '') app_tip(json.show);
		if (json.result) {
			window.yzm_waittime = 60;
		    window.yzm_timeout = setInterval(function() {
		    	window.yzm_waittime--;
		    	if (window.yzm_waittime > 0) {
		    		window.yzm_state = false;
					_vue.yzm_show = "重新发送(" + window.yzm_waittime + ")";
		    	}
			    else {
		    		clearTimeout(window.yzm_timeout);	 
		    		window.yzm_waittime = 0;
		    		window.yzm_state = true;
					_vue.yzm_show = "获取验证码";
			    }
		    }, 1000);			
			if (func && typeof(func) == "function") {
				func(json);	
			}
		}
		else {
    		window.yzm_state = true;
			_vue.yzm_show = "获取验证码";
		}
	})
}

function app_str_length(str) {
	return str.replace(/[\u0391-\uFFE5]/g,"aa").length;
}
//检测是否在数组中
function app_inarray(val, arr){
	return arr.indexOf(val) == -1 ? false : true;
}
