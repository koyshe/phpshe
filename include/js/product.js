/**
 * @copyright   2008-2015 简好网络 <http://www.phpshe.com>
 * @creatdate   2010-1001 koyshe <koyshe@gmail.com>
 */

//####################// 后台商品模块 //####################//
//价格修改
function money_edit(_this, ruledata_index, type) {
	prodata_list[ruledata_index][type] = $(_this).val();
}

//价格批量修改
function money_edit_all(type, name) {
	var value = window.prompt("批量设置" + name);
	if (value == '') {
		alert('不能为空!');		
		return;
	}
	if (value == null) {return;}
	$(":input[name='" + type + "[]']").val(value);
	for (var i in prodata_list) {
		prodata_list[i][type] = value;		
	}
}

//规格添加
function rule_add() {
	if (typeof product_rule == 'undefined') product_rule = new Array();
	product_rule.push({'name':'', 'list':[]});
	product_rule_refresh();
	//prodata_list_refresh();
}

//规格修改
function rule_edit(_this, index) {
	product_rule[index].name = $(_this).val();
	if (product_rule[index].list.length == 0) return;
	prodata_list_refresh();
}

//规格删除
function rule_del(_this, index) {
	product_rule.splice(index, 1);
	product_rule_refresh();
	prodata_list_refresh();
}

//规格选项添加
function ruledata_add(_this, rule_index, ruledata_index) {
	if ($(_this).val() == '') return;
	product_rule[rule_index].list.push({'name': $(_this).val()})
	product_rule_refresh();
	prodata_list_refresh();
}

//规格选项修改
function ruledata_edit(_this, rule_index, ruledata_index) {
	if ($(_this).val() == '') {
		ruledata_del(rule_index, ruledata_index);
	}
	else {
		product_rule[rule_index].list[ruledata_index].name = $(_this).val();
	}	
	//product_rule[rule_index].list[ruledata_index].name = $(_this).val();
	product_rule_refresh();
	prodata_list_refresh();
}

//规格选项删除
function ruledata_del(rule_index, ruledata_index) {
	product_rule[rule_index].list.splice(ruledata_index, 1)
	console.log(product_rule);
	for (var i in prodata_list) {
		var id_arr = prodata_list[i].product_ruleid.split(';');
		if (id_arr[rule_index] == ruledata_index) delete prodata_list[i];
	}	
	product_rule_refresh();
	prodata_list_refresh();
}

//刷新规格集合
function product_rule_refresh() {
	/*for (var i in product_rule) {
		if (product_rule[i].list.length == 0) {
			product_rule[i].list = [{'name':''}];
		}
		else {
			var add_btn = false;
			for (var ii in product_rule[i].list) {
				if (product_rule[i].list[ii].name == '') {
					add_btn = true;
				}
			}
			if (add_btn == false) {
				product_rule[i].list.push({'name':''});
			}
		}
	}*/
	pe_jsontpl('product_rule', {'data':product_rule});
}

//刷新商品规格列表
function prodata_list_refresh() {
	pe_jsontpl('prodata_list', {'rule':product_rule, 'list':prodata_zuhe(product_rule, prodata_list)});
}

//规格数组集合组成商品规格列表
function prodata_zuhe(product_rule, prodata_list) {
	//如果只有规格，没有规格选项的时候，默认规格选项为空
	/*for (var i in product_rule) {
		if (product_rule[i].list.length == 0) product_rule[i].list = [{'name':''}];
	}*/
	if (product_rule.length == 0) {
		var rule_list = prodata_zuhe_callback();
	}
	else if (product_rule.length == 1) {
		var rule_list = prodata_zuhe_callback(product_rule[0].list);
	}
	else {
		var rule_list = product_rule[0].list;
		for (var i=1; i<product_rule.length; i++) {
			rule_list = prodata_zuhe_callback(rule_list, product_rule[i].list);
		}	
	}
	for (var i in rule_list) {
		rule_list[i].product_ruleid = rule_list[i].rule_id.join(';');
		rule_list[i].product_rulename = rule_list[i].rule_arr.join(';');
		var prodata = prodata_list[rule_list[i].product_ruleid];
		if (typeof prodata == 'undefined') {
			rule_list[i].product_logo = '';
			rule_list[i].product_money = '';
			//rule_list[i].product_smoney = '';
			rule_list[i].product_ymoney = '';
			//rule_list[i].product_jsmoney = '';
			//rule_list[i].product_wlmoney = '';
			rule_list[i].product_point = '';
			rule_list[i].product_sn = '';
			rule_list[i].product_num = '';
			rule_list[i].huodong_money = '';
		}
		else {
			rule_list[i].product_logo = prodata.product_logo;
			rule_list[i].product_money = prodata.product_money;
			//rule_list[i].product_smoney = prodata.product_smoney;
			rule_list[i].product_ymoney = prodata.product_ymoney;
			//rule_list[i].product_jsmoney = prodata.product_jsmoney;
			//rule_list[i].product_wlmoney = prodata.product_wlmoney;
			rule_list[i].product_point = prodata.product_point;
			rule_list[i].product_sn = prodata.product_sn;
			rule_list[i].product_num = prodata.product_num;
			rule_list[i].huodong_money = prodata.huodong_money;
		}
	}
	return rule_list;
}

function prodata_zuhe_callback(data1, data2) {
	if (typeof data1 == 'undefined') {
		return [{'rule_id': [0], 'rule_arr': [''], 'logo':[''], 'colspan': [0]}];
	}
	var data1 = pe_clone(data1);
	for (var i in data1) {
		if (typeof data1[i].rule_id == 'undefined') {
			data1[i] = {'rule_id': [parseInt(i)], 'rule_arr': [data1[i].name], 'logo':[data1[i].logo], 'colspan': [1]}
		}
	}
	if (typeof data2 == 'undefined' || data2.length == 0) return data1;
	var data = new Array();
	for (var i in data1) {
		for (var ii in data2) {
			var rule_id = data1[i].rule_id.concat(parseInt(ii));
			var rule_arr = data1[i].rule_arr.concat(data2[ii].name);
			var logo = data1[i].logo;
			//循环所有colspan重新计算乘积值
			for (var iii in data1[i].colspan) {
				if (ii >= 1) {
					data1[i].colspan[iii] = 0;
				}
				else {
					data1[i].colspan[iii] *= data2.length;
				}
			}
			var colspan = data1[i].colspan.concat(1);
			data.push({'rule_id': rule_id, 'rule_arr': rule_arr, 'logo': logo, 'colspan': colspan});
		}
	}
	return data;
}