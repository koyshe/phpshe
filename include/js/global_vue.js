var rule_phone = /^1[0-9]{10}$/;
var dialog_default = {
	open: false,
	title: '',
	url: '',
	width: 0,
	height: 0
}

//获取url全局参数
function pe_urlarg(url, other_arg) {
	var arg = 'client=pc';
	if (typeof(other_arg) != 'undefined') arg = arg + other_arg;
	return url = (url.indexOf("?") != -1) ? url+'&'+arg : url+'?'+arg;
}

//数字处理
function pe_num(num, type, len, fix) {
	if (typeof(len) == 'undefined') len = 0;
	if (typeof(fix) == 'undefined') fix = false;
	var pow = Math.pow(10, len);	
	var num = parseFloat(num);
	if (isNaN(num)) num = 0;
	if (type == 'round') {
		num = Math.round(num * pow) / pow;
	}
	else if (type == 'ceil') {
		num = Math.ceil(num * pow) / pow;
	}
	else if (type == 'floor') {
		num = Math.floor(num * pow) / pow;
	}
	if (fix == true) {
		num = num.toFixed(len);
	}
	return num;
}

//打开页面
function pe_open(url, time) {
	if (typeof(time) == 'undefined') time = 1;
	setTimeout(function(){
		if (url == 'back') {
			window.history.go(-1);
		}
		else if (url == 'reload') {
			window.location.reload();
		}
		else if (url == 'dialog') {
			top.location.reload();
		}
		else {
			window.location.href = url;		
		}
	}, time);
}

//ajax获取信息
function pe_get(url, func, page) {
	var arg = '';
	/*if (typeof(page) == 'undefined') page = 0;
	if (page) {
		if (page == 1) {
			var page_more = true;
			_vue.data.list = [];
			//_vue.data.lists = [];
		}
		else {
			var page_more = (typeof(_vue.data.page_more) == 'undefined' ? true : _vue.data.page_more);		
		}
		_vue.data.page = page;
		_vue.data.page_more = page_more;
		_vue.data.nodata = false;
		arg += '&page=' + page;
		if (page_more == false) return;
	}*/
	url = pe_urlarg(url, arg);
	axios.get(url).then(function(json) {
		var json = json.data;
		/*if (page >= 1) {
    		if (json.list == null || json.list.length == 0) {
				_vue.data.page_more = false;
				if (page == 1) {
					_vue.data.nodata = true;				
				}
    		}
    		else {
				_vue.data.page = page + 1;
				if (json.list.length < 10) {
					_vue.data.page_more = false;   			
				}
    		}			
		}*/

    	if (typeof(func) == "function") {
    		func(json);
		}
	}).catch(function (json) {
		pe_alert(json);
	});
}

//ajax表单post提交
function pe_submit(url, func) {
//	pe_tip('处理中', 'loading');
	console.log(_vue);
	_vue.btn_loading = true;
//	if (typeof(e.detail.formId) != 'undefined') app_getinfo(host+"api.php?mod=wechat&act=add_formid&value="+e.detail.formId);
	setTimeout(function(){
		//var url = e.currentTarget.dataset.action;
		//var arg = 'client=minapp&user_openid='+app_getval('user_openid')+'&user_lbs='+app_getval('user_lbs')+'&city='+app_getval('city');
		//console.log(url);
		url = pe_urlarg(url);
		var user_sessid = pe_getval('user_sessid');
		var header = {};
		header['content-type'] = 'application/x-www-form-urlencoded';
		if (user_sessid) header['Cookie'] = 'PHPSESSID='+user_sessid;
		/*var data = new FormData();
		for (var i in _vue.form) {
			if (typeof _vue.form[i] == 'undefined') _vue.form[i] = '';
			data.append(i, _vue.form[i]);
		}*/
		var form_obj = document.getElementById("form").getElementsByTagName("*");
		var form_name = new Array();
		for (var i in form_obj) {
			if (typeof form_obj[i] == 'object') {
				var name = form_obj[i].getAttribute("name");
				if (name != null) form_name.push(name);			
			}
		}
		var data = new FormData();
		for (var i in _vue.data) {
			if (pe_inarray(i, form_name)) {
				if (typeof _vue.data[i] == 'undefined') _vue.data[i] = '';
				data.append(i, _vue.data[i]);			
			}
		}
		data.append('pesubmit', true);
		data.append('pe_token', pe_token);
        axios({
		    method:"post",
		    url:url,
		    headers: {
				"Content-Type": "multipart/form-data"
		    },
		    withCredentials:true,
		    data:data
		}).then(function(json){
			pe_tip_close();
			json = json.data;
			if (typeof(json.msg) != 'undefined' && json.msg != '') {
				if (json.code == 1) {
					pe_tip(json.msg, 'success');
				}
				else {
					pe_tip(json.msg);
				}
			}
	    	if (func && typeof(func) == "function") {
	    		func(json);
	    	}
	    		_vue.btn_loading = false;
		}).catch(function (json) {
			pe_alert(json);
				_vue.btn_loading = false;
		});
	}, 300)
}

//监听滚动到底部
function app_scrollmore(func) {
	window.scrollbottom = 0;
	window.onscroll = function() {
   		//变量scrollTop是滚动条滚动时，距离顶部的距离
   		var scrollTop = document.documentElement.scrollTop || document.body.scrollTop;
   		//变量windowHeight是可视区的高度
   		var windowHeight = document.documentElement.clientHeight || document.body.clientHeight;
   		//变量scrollHeight是滚动条的总高度
   		var scrollHeight = document.documentElement.scrollHeight || document.body.scrollHeight;

console.log(scrollTop + windowHeight + '---' + scrollHeight);
console.log(scrollTop + windowHeight + '---+' + window.scrollbottom);
        //滚动条到底部的条件
        if (scrollTop + windowHeight >= scrollHeight - 30 && scrollTop + windowHeight >= window.scrollbottom) {
	

	
			//防抖动重复执行
			window.scrollbottom = scrollHeight + 10;
			console.log(scrollHeight + '====' + window.scrollbottom + '----' + document.documentElement.scrollHeight);
			func();
        }
	}
}

function pe_dialog(title, url, width, height) {
	_vue.dialog = {
		open: true,
		title: title,
		url: url,
		width: width,
		height: height,
	}
}

//tip提示信息
function pe_tip(msg, type, time) {
	pe_tip_close();
	if (type == 'success') {
		_vue._message = _vue.$message({
			message: msg,
			type: 'success',
	    })
	}
	else if (type == 'error') {
		_vue._message = _vue.$message({
			message: msg,
			type: 'error',
	    })
	}
	else if (type == 'loading') {
		_vue._message = _vue.$message({
			message: msg,
			iconClass: 'el-icon-loading',
	    })
	}
	else if (type == 'loading') {
		if (typeof(time) == 'undefined') time = 10000;
        _vue._message = _vue.$loading({
			lock: false,
			text: msg,
			spinner: 'el-icon-loading',
			//background: 'rgba(0, 0, 0, 0.7)'
        });
	}
	else {
		_vue._message = _vue.$message({
			message: msg,
			type: 'info'
	    })
	}
	return false;
	if (typeof(type) != 'undefined') {
		switch (type) {
			case 'success':
				_vue._message = _vue.$message({
					message: text,
					type: 'success'
			    })
			break;
			case 'error':
				_vue._message = _vue.$message({
					message: text,
					type: 'error'
			    })
			break;			
		}
	}
	else {
		_vue._message = _vue.$message({
			message: text,
			type: 'info'
	    })
	}
};

//tip关闭信息
function pe_tip_close() {
	if (typeof _vue._message != 'undefined'){
		_vue._message.close();
	}
} 

//alert确认框
function pe_alert(msg, func) {
    _vue.$alert(msg, {
		confirmButtonText: '确定',
		callback: action => {
			if (func && typeof(func) == "function") {
				func();
			}
      	}
    })
}

//确认提醒
function pe_confirm(msg, success_func, fail_func) {
    _vue.$confirm(msg, '提示', {
		confirmButtonText: '确定',
		cancelButtonText: '取消',
		type: 'warning'
    }).then(() => {
    	if (success_func && typeof(success_func) == "function") {
			success_func();
		}
		else if (success_func) {
			pe_get(success_func, function(json){
				if (typeof(json.msg) != 'undefined' && json.msg != '') {
					if (json.code == 1) {
						pe_tip(json.msg, 'success');			
					}
					else {
						pe_tip(json.msg);						
					}
				}
				if (json.code == 1) {
					pe_open('reload', 1000);
				}
			})
		}
	}).catch(() => {
    	if (fail_func && typeof(fail_func) == "function") {
			fail_func();
		}
		else if (fail_func) {
			pe_get(fail_func, function(json){
				if (typeof(json.msg) != 'undefined' && json.msg != '') {
					if (json.code == 1) {
						pe_tip(json.msg, 'success');			
					}
					else {
						pe_tip(json.msg);						
					}
				}
				if (json.code == 1) {
					pe_open('reload', 1000);
				}
			})
		}      
	});
}

//存值
function pe_setval(name, val) {
	localStorage.setItem(name, val);
}

//取值
function pe_getval(name) {
	return localStorage.getItem(name);
}

//发送短信验证码
//const yzm_waittime = 0;
//const yzm_timeout = '';
function app_sendyzm(url, func) {
	if (window.yzm_state == false) return false;
	if (typeof(window.yzm_waittime) != 'undefined' && window.yzm_waittime > 0) return false;
	window.yzm_state = false;
	_vue.yzm_show = '发送中...';
	app_getinfo(url, function(json){
		if (typeof(json.show) != 'undefined' && json.show != '') app_tip(json.show);
		if (json.result) {
			window.yzm_waittime = 60;
		    window.yzm_timeout = setInterval(function() {
		    	window.yzm_waittime--;
		    	if (window.yzm_waittime > 0) {
		    		window.yzm_state = false;
					_vue.yzm_show = "重新发送(" + window.yzm_waittime + ")";
		    	}
			    else {
		    		clearTimeout(window.yzm_timeout);	 
		    		window.yzm_waittime = 0;
		    		window.yzm_state = true;
					_vue.yzm_show = "获取验证码";
			    }
		    }, 1000);			
			if (func && typeof(func) == "function") {
				func(json);	
			}
		}
		else {
    		window.yzm_state = true;
			_vue.yzm_show = "获取验证码";
		}
	})
}

//计算字符长度
function pe_str_length(str) {
	return str.replace(/[\u0391-\uFFE5]/g,"aa").length;
}

//检测是否在数组中
function pe_inarray(val, arr){
	return arr.indexOf(val) == -1 ? false : true;
}


function pe_extend(arr1, arr2) {
	return Object.assign({}, arr1, arr2);
}


//预加载loading
function page_loading() {
	$(document).ready(function(){
		// WAIT FOR EVERYTHING TO LOAD
		//$(window).load(function(){
			$('#page_loading').fadeOut(5000, function(){
			})
		//});		
	})
}