<?php
/**
 * @copyright   2008-2021 简好网络 <http://www.phpshe.com>
 * @creatdate   2012-0501 koyshe <koyshe@gmail.com>
 */
include('common.php');
$adminmenu[1] = array(
	'headnav'=>'商品管理',
	'subnav'=>array(
		array('name'=>'商品列表', 'menumark'=>'product', 'modact'=>array('product','prodata','prokey','buyer'), 'url'=>'admin.php?mod=product'),
		array('name'=>'商品分类', 'menumark'=>'category', 'modact'=>array('category'), 'url'=>'admin.php?mod=category'),
		array('name'=>'品牌管理', 'menumark'=>'brand', 'modact'=>array('brand'),  'url'=>'admin.php?mod=brand'),
		/*array('name'=>'标签管理', 'menumark'=>'tag', 'url'=>'admin.php?mod=tag'),*/
		/*array('name'=>'规格管理', 'menumark'=>'rule', 'modact'=>array('rule'), 'url'=>'admin.php?mod=rule'),*/
		/*array('name'=>'咨询管理', 'menumark'=>'ask', 'url'=>'admin.php?mod=ask'),*/
		array('name'=>'评价管理', 'menumark'=>'comment', 'modact'=>array('comment'), 'url'=>'admin.php?mod=comment')
	)
);
$adminmenu[2] = array(
	'headnav'=>'交易中心',
	'subnav'=>array(
		array('name'=>'订单列表', 'menumark'=>'order', 'modact'=>array('order','cainiao'), 'url'=>'admin.php?mod=order'),
		array('name'=>'退款/退货', 'menumark'=>'refund', 'modact'=>array('refund', 'refund_addr'), 'url'=>'admin.php?mod=refund'),
		array('name'=>'收款记录', 'menumark'=>'pay', 'modact'=>array('pay'), 'url'=>'admin.php?mod=pay&state=success'),
		array('name'=>'提现管理', 'menumark'=>'cashout', 'modact'=>array('cashout'), 'url'=>'admin.php?mod=cashout'),
		array('name'=>'钱包明细', 'menumark'=>'moneylog', 'modact'=>array('moneylog'), 'url'=>'admin.php?mod=moneylog'),
		array('name'=>'积分明细', 'menumark'=>'pointlog', 'modact'=>array('pointlog'), 'url'=>'admin.php?mod=pointlog')
	)
);
$adminmenu[3] = array(
	'headnav'=>'营销中心',
	'subnav'=>array(
		array('name'=>'优惠券码', 'menumark'=>'quan', 'modact'=>array('quan'), 'url'=>'admin.php?mod=quan&state=1'),
		array('name'=>'签到活动', 'menumark'=>'sign', 'modact'=>array('sign'), 'url'=>'admin.php?mod=sign')
	)
);
$adminmenu[4] = array(
	'headnav'=>'用户中心',
	'subnav'=>array(
		array('name'=>'会员列表', 'menumark'=>'user', 'modact'=>array('user'), 'url'=>'admin.php?mod=user'),
		array('name'=>'会员等级', 'menumark'=>'userlevel', 'modact'=>array('userlevel'), 'url'=>'admin.php?mod=userlevel'),
		array('name'=>'管理帐号', 'menumark'=>'admin', 'modact'=>array('admin'), 'url'=>'admin.php?mod=admin'),
		array('name'=>'管理权限', 'menumark'=>'adminlevel', 'modact'=>array('adminlevel'), 'url'=>'admin.php?mod=adminlevel')
	)
);
$adminmenu[5] = array(
	'headnav'=>'文章中心',
	'subnav'=>array(
		array('name'=>'文章列表', 'menumark'=>'article', 'modact'=>array('article'), 'url'=>'admin.php?mod=article&type=news'),
		array('name'=>'文章分类', 'menumark'=>'article_category', 'modact'=>array('article_category'), 'url'=>'admin.php?mod=article_category&type=news'),
		array('name'=>'单页管理', 'menumark'=>'page', 'modact'=>array('page'), 'url'=>'admin.php?mod=page')
	)
);
$adminmenu[6] = array(
	'headnav'=>'控制面板',
	'subnav'=>array(
		array('name'=>'网站设置', 'menumark'=>'setting', 'modact'=>array('setting','notice'), 'url'=>'admin.php?mod=setting&act=base'),
		array('name'=>'支付设置', 'menumark'=>'payment', 'modact'=>array('payment'), 'url'=>'admin.php?mod=payment'),
		array('name'=>'导航管理', 'menumark'=>'menu', 'modact'=>array('menu'), 'url'=>'admin.php?mod=menu'),
		array('name'=>'广告管理', 'menumark'=>'ad', 'modact'=>array('ad'), 'url'=>'admin.php?mod=ad'),
		array('name'=>'运费模板', 'menumark'=>'wlmb', 'modact'=>array('wlmb'), 'url'=>'admin.php?mod=wlmb'),
		array('name'=>'友情链接', 'menumark'=>'link', 'modact'=>array('link'), 'url'=>'admin.php?mod=link'),
		array('name'=>'模板管理', 'menumark'=>'moban', 'modact'=>array('moban'), 'url'=>'admin.php?mod=moban'),
		array('name'=>'数据备份', 'menumark'=>'db', 'modact'=>array('db'), 'url'=>'admin.php?mod=db'),
		array('name'=>'数据统计', 'menumark'=>'tongji', 'modact'=>array('tongji'), 'hide'=>true),
		array('name'=>'缓存管理', 'menumark'=>'cache', 'modact'=>array('cache'), 'hide'=>true)
	)
);
$adminmenu_modact_list = array();
$adminmenu_menumark_list = array();
foreach ($adminmenu as $v){
	foreach ($v['subnav'] as $vv) {
		$adminmenu_modact_list[$vv['modact'][0]] = $vv['modact'];
		$adminmenu_menumark_list[$vv['modact'][0]] = $vv['menumark'];		
		if (!is_array($vv['list'])) continue; 
		foreach ($vv['list'] as $vvv) {
			$adminmenu_modact_list[$vvv['modact'][0]] = $vvv['modact'];
		}
	}
}

$admin = pe_login('admin');
if ($admin['admin_id'] && $mod == 'do' && $act != 'logout') pe_goto('admin.php');
if (!$admin['admin_id'] && $mod != 'do') pe_goto('admin.php?mod=do&act=login');

//检测管理权限
$cache_adminlevel = cache::get('adminlevel');
if (!in_array($mod, array('index', 'do')) && $admin['adminlevel_id'] != 1) {
	$admin_result = true;
	$adminlevel_modact = $cache_adminlevel[$admin['adminlevel_id']]['adminlevel_modact'];
	$adminlevel_modact = $adminlevel_modact ? unserialize($adminlevel_modact) : array();
	$adminlevel_modact_sel = array();
	foreach ($adminlevel_modact as $v) {
		if (!is_array($adminmenu_modact_list[$v]))continue;
		foreach ($adminmenu_modact_list[$v] as $vv) {
			$adminlevel_modact_sel[] = $vv;
		}
	}
	$modact = "{$mod}-{$act}";
	if (!in_array($mod, $adminlevel_modact_sel) && !in_array($modact, $adminlevel_modact_sel)) {
		$admin_result = false;	
	}
	if (!$admin_result) pe_error('权限不足...', 'admin.php');
}
//检测菜单隐藏
$adminlevel_menumark = $cache_adminlevel[$admin['adminlevel_id']]['adminlevel_menumark'];
$adminlevel_menumark = $adminlevel_menumark ? unserialize($adminlevel_menumark) : array();
foreach ($adminmenu as $k=>$v) {
	foreach ($v['subnav'] as $kk=>$vv) {
		if ($vv['hide']) continue;
		if ($admin['adminlevel_id'] == 1 or in_array($vv['menumark'], $adminlevel_menumark)) {
			$adminmenu[$k]['show'] = true;
			$adminmenu[$k]['subnav'][$kk]['show'] = true;
		}
	}
}

if (is_file("{$pe['path']}module/{$module}/{$mod}.php")) {
	include("{$pe['path']}module/{$module}/{$mod}.php");
}
else {
	pe_404();
}
pe_result();
?>