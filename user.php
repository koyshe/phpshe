<?php
include('common.php');
if (!$cache_setting['web_state']) die('站点已关闭');
$cache_category = cache::get('category');
$cache_category_arr = cache::get('category_arr');
$cache_userlevel = cache::get('userlevel');
$cache_menu = cache::get('menu');
pe_lead('hook/wechat.hook.php');

$user = pe_login('user');
$user_wx_openid = pe_dbhold($_GET['user_openid']);
$user_lbs = explode(',', pe_dbhold($_g_user_lbs));
$user['user_lat'] = floatval($user_lbs[0]);
$user['user_lng'] = floatval($user_lbs[1]);
//$user = $db->pe_select('user', array('user_id'=>20));

if ($pe['client']) {
	if (!$user['user_id'] && $mod != 'do') pe_apidata(array('code'=>'nologin', 'msg'=>'请先登录'));
}
else {
	if ($user['user_id'] && $mod == 'do' && $act != 'logout') pe_goto('user.php');
	if (!$user['user_id'] && $mod != 'do') pe_goto('user.php?mod=do&act=login');
}

if (is_file("{$pe['path']}module/{$module}/{$mod}.php")) {
	include("{$pe['path']}module/{$module}/{$mod}.php");
}
else {
	pe_404();
}
pe_result();
?>