<?php
//热销商品
function product_hotlist($num = 5) {
    global $db;
    $list = $db->pe_selectall('product', array('product_state'=>1, 'order by'=>'`product_sellnum` desc'), '*', array($num));
    foreach ($list as $k=>$v) {
        $list[$k]['product_logo'] = pe_thumb($v['product_logo'], 400, 400);
        $list[$k]['product_money'] = product_money($v);
    }
    return $list;
}

//新品推荐
function product_newlist($num = 5) {
    global $db;
    $list = $db->pe_selectall('product', array('product_state'=>1, 'order by'=>'product_xinpin desc, product_order asc, product_id desc'), '*', array($num));
    foreach ($list as $k=>$v) {
        $list[$k]['product_logo'] = pe_thumb($v['product_logo'], 400, 400);
        $list[$k]['product_money'] = product_money($v);
    }
    return $list;
}

//获取购买商品信息
function product_buyinfo($product_guid) {
    global $db;
    $sql_field = "a.`product_id`, a.`product_guid`, a.`product_rulename` as product_rule, a.`product_money`, a.`product_ymoney`, a.`product_num`, a.`product_sn`, b.`product_name`, b.`product_type`, b.`product_logo`, b.`product_zhekou`, b.`product_virtual`, b.`huodong_id`, b.`huodong_type`, b.`huodong_tag`, b.`huodong_stime`, b.`huodong_etime`, b.`wlmb_id`";
    $sql = "select {$sql_field} from `".dbpre."prodata` a, `".dbpre."product` b where a.`product_id` = b.`product_id` and a.`product_guid` = '{$product_guid}' and b.`product_state` = 1";
    $info = $db->sql_select($sql);
    if ($info['huodong_id']) {
    	$huodong = $db->pe_select('huodong_prodata', array('product_id'=>$info['product_id'], 'product_guid'=>$info['product_guid'], 'huodong_id'=>$info['huodong_id']));
    	$info['product_money'] = $huodong['product_money'];
      	$info['product_ymoney'] = $huodong['product_ymoney'];  	
    	$info['product_num'] = $huodong['product_num'];
    }
    return $info;
}

//商品数量更新
function product_jsnum($id, $type, $num = 1) {
    global $db;
    switch ($type) {
        case 'add_num':
        case 'del_num':
            $product_guid = pe_dbhold($id);
            $info = $db->pe_select('prodata', array('product_guid'=>$product_guid), 'product_id, product_num');
            if ($type == 'add_num') {
                $product_num = $info['product_num'] + $num;
            }
            else {
                $product_num = $info['product_num'] > $num ? ($info['product_num'] - $num) : 0;
            }
            $db->pe_update('prodata', array('product_guid'=>$product_guid), array('product_num'=>$product_num));
            $product = $db->pe_select('prodata', array('product_id'=>$info['product_id']), 'sum(product_num) as `num`');
            $db->pe_update('product', array('product_id'=>$info['product_id']), array('product_num'=>$product['num']));
		break;
        case 'add_sellnum':
        case 'del_sellnum':
            $product_id = intval($id);
            $info = $db->pe_select('product', array('product_id'=>$product_id), 'product_id, product_sellnum');
            if ($type == 'add_sellnum') {
                $product_sellnum = $info['product_sellnum'] + $num;
            }
            else {
                $product_sellnum = $info['product_sellnum'] > $num ? ($info['product_sellnum'] - $num) : 0;
            }
            $db->pe_update('product', array('product_id'=>$info['product_id']), array('product_sellnum'=>$product_sellnum));
		break;
        case 'clicknum':
            $product_id = intval($id);
            $db->pe_update('product', array('product_id'=>$product_id), "`product_clicknum` = `product_clicknum` + ".rand(3, 5)."");
		break;
        default:
            $product_id = intval($id);
            if (in_array($type, array('collectnum', 'asknum'))) {
                $num = $db->pe_num(substr($type, 0, -3), array('product_id'=>$product_id));
                $db->pe_update('product', array('product_id'=>$product_id), array("product_{$type}"=>$num));
            }
            else if($type == 'commentnum') {
                $hao_num = $db->pe_num('comment', array('product_id'=>$product_id, 'comment_star'=>array(4,5)));
                $zhong_num = $db->pe_num('comment', array('product_id'=>$product_id, 'comment_star'=>3));
                $cha_num = $db->pe_num('comment', array('product_id'=>$product_id, 'comment_star'=>array(1,2)));
                $comment = $db->pe_select('comment', array('product_id'=>$product_id), "sum(comment_star) as comment_star");
                $sql_comment['product_commentnum'] = $hao_num + $zhong_num + $cha_num;
                $sql_comment['product_commentrate'] = "{$hao_num},{$zhong_num},{$cha_num}";
                $sql_comment['product_commentstar'] = $comment['comment_star'];
                $db->pe_update('product', array('product_id'=>$product_id), $sql_comment);
            }
		break;
    }
}

//商品价格
function product_money($info = array()) {
    global $user, $cache_userlevel;
    if ($info['huodong_id']) return $info['huodong_money'];
    if (!$user['user_id']) return $info['product_money'];
    $product_zhekou = $info['product_zhekou'] ? json_decode($info['product_zhekou'], true) : array();
    if ($product_zhekou[$user['userlevel_id']] > 0) {
        $zhe = $product_zhekou[$user['userlevel_id']];
    }
    else {
        $zhe = $cache_userlevel[$user['userlevel_id']]['userlevel_zhe'];
    }
    $money = $zhe ? pe_num($info['product_money'] * $zhe, 'round', 2) : $info['product_money'];
    return $money;
}

//更新商品活动信息
function product_huodong_callback() {
    global $db;
    $nowtime = time();
	//更新活动状态
	$db->pe_update('huodong', "and huodong_state != 'wstart' and huodong_stime > '{$nowtime}'", array('huodong_state'=>'wstart'));
	$db->pe_update('huodong', "and huodong_state != 'work' and huodong_stime <= '{$nowtime}' and huodong_etime > '{$nowtime}'", array('huodong_state'=>'work'));
	$db->pe_update('huodong', "and huodong_state != 'over' and huodong_etime <= '{$nowtime}'", array('huodong_state'=>'over'));
    //同步有效商品
    $huodong_idarr = array_keys($db->index('huodong_id')->pe_selectall('huodong', array('huodong_state'=>'work')));
    $sql = "update `".dbpre."product` a left join (select * from `".dbpre."huodong_product` where ".pe_sqlin('huodong_id', $huodong_idarr).") b on a.product_id = b.product_id set a.huodong_id = b.huodong_id, a.huodong_type = b.huodong_type, a.huodong_tag = b.huodong_tag, a.huodong_money = b.product_money, a.huodong_stime = b.huodong_stime, a.huodong_etime = b.huodong_etime where ".pe_sqlin('b.huodong_id', $huodong_idarr);
    $db->sql_update($sql);
    //删除失效商品
    //$product_idarr = array_keys($db->index('product_id')->pe_selectall('huodong_product', array('huodong_id'=>$huodong_idarr)));
    $db->pe_update('product', " and huodong_id > 0 and ".pe_sqlin('huodong_id', $huodong_idarr, false), "huodong_id = 0, huodong_type = '', huodong_tag = '', huodong_money = 0, huodong_stime = 0, huodong_etime = 0");
}

//商品标签
function product_tag($info) {
	$tag_arr = array();
	if ($info['huodong_tag']) $tag_arr[] = $info['huodong_tag'];
	if ($info['product_tuijian']) $tag_arr[] = '推荐';
	if ($info['product_xinpin']) $tag_arr[] = '新品';
	return $tag_arr;
}

//检测卡密过期
function prokey_check($product_id = 0) {
    global $db;
	$nowdate = date('Y-m-d');
	$db->pe_update('prokey', " and `product_id` = '{$product_id}' and `prokey_state` = 'new' and `prokey_edate` < '{$nowdate}'", array('prokey_state'=>'expire'));
}
