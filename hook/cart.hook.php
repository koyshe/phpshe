<?php
//购物车列表及检测
function cart($cart_id = null) {
	global $db, $user;
	$list = $del_list = array();
	if ($cart_id) {
		$sql_where['cart_id'] = pe_dbhold($cart_id);
	}
	else {
		$sql_where['cart_act'] = 'add';
	}
	$sql_where['user_id'] = $user['user_id'];
	$cart_list = $db->pe_selectall('cart', pe_dbhold($sql_where));
	$cart_num = count($cart_list);	
	foreach ($cart_list as $k=>$v) {
		$error = array();
		$product_guid = pe_dbhold($v['product_guid']);
		$product = product_buyinfo($product_guid);
		//未参团按原价购买
		if ($product['huodong_type'] == 'pintuan' && $v['cart_type'] != 'pintuan') {
			$product['product_money'] = $product['product_smoney'];
			$product['huodong_id'] = 0;
			$product['huodong_type'] = '';
			$product['huodong_tag'] = '';
		}
		$buy['cart_id'] = $v['cart_id'];
		//检测商品是否失效或删除并格式化商品数据
		if ($product['product_id'] && $product['product_guid']) {
			$buy['product_id'] = $product['product_id'];
			$buy['product_guid'] = $product['product_guid'];
			//$buy['product_type'] = $product['product_type'];
			$buy['product_name'] = $product['product_name'];
			$buy['product_rule'] = $product['product_rule'];
			$buy['product_logo'] = $product['product_logo'];
			$buy['product_money'] = $product['product_money'];
			//$buy['product_ymoney'] = $product['product_ymoney'];
			$buy['product_point'] = $product['product_point'];			
			$buy['product_allnum'] = $product['product_num'];
			$buy['product_num'] = $v['product_num'];
			$buy['product_allmoney'] = $buy['product_money'] * $buy['product_num'];
			$buy['huodong_id'] = $product['huodong_id'];
			$buy['huodong_type'] = $product['huodong_type'];
			$buy['huodong_tag'] = $product['huodong_tag'];
			$buy['wlmb_id'] = $product['wlmb_id'];
			if (($product['product_virtual'] or $product['huodong_type'] == 'pintuan') && $cart_num > 1) {
				$error = array('msg'=>"只能单买", 'del'=>true);
			}
			elseif ($v['cart_type'] == 'pintuan') {
				$ret = pintuan_check($product['huodong_id'], $v['pintuan_id']);
				if ($ret['code']!=1) $error = array('msg'=>$ret['msg'], 'del'=>true);
			}
			elseif ($error['del'] == false && $buy['product_num'] > $buy['product_allnum']) {
				$error = array('msg'=>"仅剩{$buy['product_allnum']}件");			
			}
		}
		else {
			$buy['product_id'] = $v['product_id'];
			$buy['product_guid'] = $v['product_guid'];
			$buy['product_name'] = $v['product_name'];
			$buy['product_rule'] = $v['product_rule'];
			$buy['product_logo'] = $v['product_logo'];
			$buy['product_money'] = $v['product_money'];
			$buy['product_point'] = $v['product_point'];
			$buy['product_allnum'] = $v['product_num'];
			$buy['product_num'] = $v['product_num'];
			$buy['product_allmoney'] = $buy['product_money'] * $buy['product_num'];
			$error = array('msg'=>'已失效', 'del'=>true);
		}
		$list[$product_guid] = $buy;
		$list[$product_guid]['error'] = $error;
        if ($error['del']) $del_list[$product_guid] = $list[$product_guid];
		$info['order_type'] = $v['cart_type'];
		//$info['order_from'] = $v['cart_from'];
		$info['order_virtual'] = intval($product['product_virtual']);
		$info['order_name'][] = $product['product_name'];
		//$info['order_product_id'][] = $buy['product_id'];
		//$info['order_product_num'] += $buy['product_num'];
		$info['order_product_money'] += $buy['product_allmoney'];
		//$info['order_wl_money'] += $product['product_wlmoney'];
		//$info['order_wl_money'] = $info['order_wl_money'] ? $info['order_wl_money'] : $product['product_wlmoney'];
		$info['order_wl_money'] = $info['order_wl_money'] >= $product['product_wlmoney'] ? $info['order_wl_money'] : $product['product_wlmoney'];
		$info['order_point_get'] += $buy['product_point'] * $buy['product_num'];
		$info['huodong_id'] = $v['huodong_id'];
		$info['pintuan_id'] = $v['pintuan_id'];
		$info['cart_id'][] = $v['cart_id'];
	}	
	//格式化显示数据
	$info['order_name'] = is_array($info['order_name']) ? implode(';', $info['order_name']) : '';
	$info['order_product_money'] = pe_num($info['order_product_money'], 'round', 2, true);
	$info['order_wl_money'] = pe_num($info['order_wl_money'], 'round', 2, true);
	$info['order_money'] = pe_num($info['order_product_money'] + $info['order_wl_money'], 'round', 2, true);
	$info['order_point'] = intval($info['order_point']);
	$info['list'] = array_values($list);
	$info['del_list'] = array_values($del_list);
	return $info;
}

//获取购物车商品数
function cart_num() {
	global $db, $user;
	$user_id = $user['user_id'] ? $user['user_id'] : pe_user_id();
	$info_list = $db->pe_selectall('cart', array('cart_act'=>'add', 'user_id'=>$user_id));
	foreach ($info_list as $v) {
		$num += $v['product_num'];
	}
	return intval($num);
}
