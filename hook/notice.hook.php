<?php
//发送邮件/短信/微信通知
function add_noticelog($user_id, $type, $info = array()) {
	global $db, $cache_setting;
	$cache_notice = cache::get('notice');
	$notice_list = $cache_notice[$type];
	$user = $db->pe_select('user', array('user_id'=>$user_id), 'user_phone, user_email');
	$user['user_phone'] = $info['user_phone'] ? $info['user_phone'] : $user['user_phone'];
	$user['user_email'] = $info['user_email'] ? $info['user_email'] : $user['user_email'];
	foreach ($notice_list as $notice) {
		if (!$notice['notice_state']) continue;
		if ($notice['notice_user'] == 'admin') {
			//发送管理员短信通知
			if ($notice['notice_way'] == 'sms' && $cache_setting['sms_admin']) {
				foreach (explode(',', $cache_setting['sms_admin']) as $v) {
					$list['noticelog_way'] = 'sms';
					$list['noticelog_user'] = $v;
					$list['noticelog_name'] = '';
					$list['noticelog_atime'] = time();
					$list['noticelog_text'] = "【{$cache_setting['sms_sign']}】".notice_tag_replace($notice['notice_text'], $info);
					$sql_set[] = pe_dbhold($list);	
				}		
			}
			//发送管理员邮件通知
			if ($notice['notice_way'] == 'email' && $cache_setting['email_admin']) {
				foreach (explode(',', $cache_setting['email_admin']) as $v) {
					$list['noticelog_way'] = 'email';
					$list['noticelog_user'] = $v;			
					$list['noticelog_atime'] = time();
					$list['noticelog_name'] = notice_tag_replace($notice['notice_title'], $info);
					$list['noticelog_text'] = notice_tag_replace($notice['notice_text'], $info);
					$sql_set[] = pe_dbhold($list, array('noticelog_text'));	
				}		
			}
		}
		else {
			//发送用户短信通知
			if ($notice['notice_way'] == 'sms' && $user['user_phone']) {
				$list['noticelog_way'] = 'sms';
				$list['noticelog_user'] = $user['user_phone'];
				$list['noticelog_name'] = '';
				$list['noticelog_atime'] = time();
				$list['noticelog_text'] = "【{$cache_setting['sms_sign']}】".notice_tag_replace($notice['notice_text'], $info);
				$sql_set[] = pe_dbhold($info_list);
			}		
			//发送用户邮件通知
			if ($notice['notice_way'] == 'email' && $user['user_email']) {
				$list['noticelog_way'] = 'email';
				$list['noticelog_user'] = $user['user_email'];
				$list['noticelog_atime'] = time();
				$list['noticelog_name'] = notice_tag_replace($notice['notice_title'], $info);
				$list['noticelog_text'] = notice_tag_replace($notice['notice_text'], $info);
				$sql_set[] = pe_dbhold($list, array('noticelog_text'));
			}
		}
	}
	if (is_array($sql_set)) $db->pe_insert('noticelog', $sql_set);
	add_wechat_noticelog($user_id, $type, $info);
}

//发送微信模板通知
function add_wechat_noticelog($user_id, $type, $info = '') {
	global $db, $pe, $cache_setting;
	$cache_notice = cache::get('wechat_notice');
	$notice = $cache_notice[$type];
	$user = $db->pe_select('user_openid', array('user_id'=>$user_id));
	$admin_list = $db->pe_selectall('user_openid', array('user_id'=>explode(',', pe_dbhold($cache_setting['wechat_admin_userid']))));
	switch($type) {
		case 'order_add':
			//统计订单商品数量
			$tj = $db->pe_select('orderdata', array('order_id'=>$info['order_id']), 'sum(product_num) as `num`');
			/*if ($notice['user']['notice_state'] && $user['user_wx_openid']) {
				$list['noticelog_way'] = 'wechat';
				$list['noticelog_user'] = $user['user_name'];
				$list['noticelog_name'] = '';
				$list['noticelog_text'] = '亲，您的订单已提交成功，请及时付款哦';
				$list['noticelog_atime'] = time();
				$noticelog_data['data']['first'] = array('value'=>'亲，您的订单已提交成功，请及时付款哦');
				$noticelog_data['data']['keyword1'] = array('value'=>$info['order_id']);
				$noticelog_data['data']['keyword2'] = array('value'=>$info['order_name']);
				$noticelog_data['data']['keyword3'] = array('value'=>"{$tj['num']}件");
				$noticelog_data['data']['keyword4'] = array('value'=>"{$info['order_money']}元");
				$noticelog_data['data']['keyword5'] = array('value'=>$info['order_payment_name']);
				$noticelog_data['url'] = "{$pe['host']}user.php?mod=order&act=view&id={$info['order_id']}";
				$noticelog_data['template_id'] = $notice['user']['notice_tplid'];
				$noticelog_data['touser'] = $user['user_wx_openid'];
				$list['noticelog_data'] = serialize($noticelog_data);
				$sql_set[] = pe_dbhold($list, array('noticelog_data'));
			}*/
			if ($notice['admin']['notice_state'] && count($admin_list)) {
				foreach ($admin_list as $k=>$v) {
					$noticelog_data['data']['first'] = array('value'=>'您好，您收到了一个新订单');
					$noticelog_data['data']['keyword1'] = array('value'=>$info['order_id']);
					$noticelog_data['data']['keyword2'] = array('value'=>$info['order_name']);
					$noticelog_data['data']['keyword3'] = array('value'=>"{$tj['num']}件");
					$noticelog_data['data']['keyword4'] = array('value'=>"{$info['order_money']}元");
					$noticelog_data['data']['keyword5'] = array('value'=>$info['order_payment_name']);
					$noticelog_data['data']['remark'] = array('value'=>'付款状态：未支付');
					$noticelog_data['template_id'] = $notice['admin']['notice_tplid'];
					$noticelog_data['touser'] = $v['user_wx_h5_openid'];
					$noticelog['noticelog_data'] = serialize($noticelog_data);
					$noticelog['noticelog_way'] = 'wechat';
					$noticelog['noticelog_user'] = $user['user_name'];
					$noticelog['noticelog_text'] = '您好，您收到了一个新订单';
					$noticelog['noticelog_atime'] = time();
					$sql_set[] = pe_dbhold($noticelog, array('noticelog_data'));
				}
			}
		break;
		case 'order_pay':
			/*if ($notice['user']['notice_state'] && $user['user_wx_openid']) {
				$info_list[0]['noticelog_data']['keyword1'] = array('value'=>$info['order_id']);
				$info_list[0]['noticelog_data']['keyword2'] = array('value'=>$info['order_name']);
				$info_list[0]['noticelog_data']['keyword3'] = array('value'=>"{$info['order_money']}元");
				$info_list[0]['noticelog_data']['keyword4'] = array('value'=>$info['order_payment_name']);
				$info_list[0]['noticelog_url'] = "/page/user/order_view/order_view?id={$info['order_id']}";
				$info_list[0]['noticelog_tpl'] = $notice['user']['notice_tpl'];
				$info_list[0]['noticelog_tplid'] = $notice['user']['notice_tplid'];
				$info_list[0]['user_id'] = $user['user_id'];
				$info_list[0]['user_name'] = $user['user_name'];
				$info_list[0]['user_wx_openid'] = $user['user_wx_openid'];
			}*/
			if ($notice['admin']['notice_state'] && count($admin_list)) {
				foreach ($admin_list as $k=>$v) {
					$noticelog_data['data']['first'] = array('value'=>'您好，您有一笔订单收款成功');
					$noticelog_data['data']['keyword1'] = array('value'=>$info['user_name']);
					$noticelog_data['data']['keyword2'] = array('value'=>$info['order_id']);
					$noticelog_data['data']['keyword3'] = array('value'=>"{$info['order_money']}元");
					$noticelog_data['data']['keyword4'] = array('value'=>pe_date(time()));
					$noticelog_data['template_id'] = $notice['admin']['notice_tplid'];
					$noticelog_data['touser'] = $v['user_wx_h5_openid'];
					$noticelog['noticelog_data'] = serialize($noticelog_data);
					$noticelog['noticelog_way'] = 'wechat';
					$noticelog['noticelog_user'] = $user['user_name'];
					$noticelog['noticelog_text'] = '您好，您收到了一个新订单';
					$noticelog['noticelog_atime'] = time();
					$sql_set[] = pe_dbhold($noticelog, array('noticelog_data'));
				}
			}
		break;
		case 'order_send':
			if ($notice['user']['notice_state'] && $user['user_wx_openid']) {
				$noticelog_data['data']['keyword1'] = array('value'=>$info['order_id']);
				$noticelog_data['data']['keyword2'] = array('value'=>$info['order_name']);
				$noticelog_data['data']['keyword3'] = array('value'=>"{$info['order_money']}元");
				$noticelog_data['data']['keyword4'] = array('value'=>$info['order_wl_name']);
				$noticelog_data['data']['keyword5'] = array('value'=>$info['order_wl_id']);
				$noticelog_data['url'] = "{$pe['host']}user.php?mod=order&act=view&id={$info['order_id']}";
				$noticelog_data['template_id'] = $notice['user']['notice_tplid'];
				$noticelog_data['touser'] = $user['user_wx_h5_openid'];
				$noticelog['noticelog_data'] = serialize($noticelog_data);
				$noticelog['noticelog_way'] = 'wechat';
				$noticelog['noticelog_user'] = $user['user_name'];
				$noticelog['noticelog_text'] = '亲，您的订单已发货，请注意接收';
				$noticelog['noticelog_atime'] = time();
				$sql_set[] = pe_dbhold($noticelog, array('noticelog_data'));
			}
		break;
		case 'order_close':
			if ($notice['user']['notice_state'] && $user['user_wx_openid']) {
				$noticelog_data['data']['keyword1'] = array('value'=>$info['order_name']);
				$noticelog_data['data']['keyword2'] = array('value'=>$info['order_id']);
				$noticelog_data['data']['keyword3'] = array('value'=>$info['order_closetext']);
				$noticelog_data['url'] = "{$pe['host']}user.php?mod=order&act=view&id={$info['order_id']}";
				$noticelog_data['template_id'] = $notice['user']['notice_tplid'];
				$noticelog_data['touser'] = $user['user_wx_h5_openid'];
				$noticelog['noticelog_data'] = serialize($noticelog_data);
				$noticelog['noticelog_way'] = 'wechat';
				$noticelog['noticelog_user'] = $user['user_name'];
				$noticelog['noticelog_text'] = '亲，您的订单已被关闭';
				$noticelog['noticelog_atime'] = time();
				$sql_set[] = pe_dbhold($noticelog, array('noticelog_data'));
			}
		break;
		/*case 'money_change':
			if ($notice['user']['notice_state'] && $user['user_wx_openid']) {
				$info_list[0]['noticelog_data']['keyword1'] = array('value'=>$info['moneylog_in'] > 0 ? "+ {$info['moneylog_in']}元" : "- {$info['moneylog_out']}元");
				$info_list[0]['noticelog_data']['keyword2'] = array('value'=>pe_date($info['moneylog_atime']));
				$info_list[0]['noticelog_data']['keyword3'] = array('value'=>$info['moneylog_text']);
				$info_list[0]['noticelog_url'] = "/page/user/moneylog_list";
				$info_list[0]['noticelog_tpl'] = $notice['user']['notice_tpl'];
				$info_list[0]['noticelog_tplid'] = $notice['user']['notice_tplid'];
				$info_list[0]['user_id'] = $user['user_id'];
				$info_list[0]['user_name'] = $user['user_name'];
				$info_list[0]['user_wx_openid'] = $user['user_wx_openid'];
			}
		break;
		case 'point_change':
			if ($notice['user']['notice_state'] && $user['user_wx_openid']) {
				$info_list[0]['noticelog_data']['keyword1'] = array('value'=>$info['pointlog_in'] > 0 ? "+ {$info['pointlog_in']}积分" : "- {$info['pointlog_out']}积分");
				$info_list[0]['noticelog_data']['keyword2'] = array('value'=>pe_date($info['pointlog_atime']));
				$info_list[0]['noticelog_data']['keyword3'] = array('value'=>$info['pointlog_text']);
				$info_list[0]['noticelog_url'] = "/page/user/pointlog_list";
				$info_list[0]['noticelog_tpl'] = $notice['user']['notice_tpl'];
				$info_list[0]['noticelog_tplid'] = $notice['user']['notice_tplid'];
				$info_list[0]['user_id'] = $user['user_id'];
				$info_list[0]['user_name'] = $user['user_name'];
				$info_list[0]['user_wx_openid'] = $user['user_wx_openid'];
			}
		break;*/
	}
	if (is_array($sql_set)) $db->pe_insert('noticelog', $sql_set);
}

//替换通知模板标签
function notice_tag_replace($text, $info) {
	$GLOBALS['notice_taginfo'] = $info;
	if (!function_exists('notice_tag_replace_nn')) {
		function notice_tag_replace_nn($match) { 
			return $GLOBALS['notice_taginfo'][$match[1]];	
		}
	}
	return preg_replace_callback('|{([^}]*)}|', "notice_tag_replace_nn", $text);
	/*if (version_compare(PHP_VERSION, '5.3', '<')) {
		return preg_replace_callback('|{([^}]*)}|', create_function('$match', 'return $GLOBALS[\'notice_taginfo\'][$match[1]];'), $text);	
	}
	else {
		return preg_replace_callback('|{([^}]*)}|', function($match) {
			return $GLOBALS['notice_taginfo'][$match[1]];
		}, $text);	
	}*/
}
?>