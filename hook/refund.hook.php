<?php
//退款状态计算
function refund_stateshow($state, $type = '') {
	global $ini;
	$value = $ini['refund_state'][$state] ? $ini['refund_state'][$state] : '';
	if ($state == 'success') {
		$color = "cgreen";
	}
	elseif ($state == 'close') {
		$color = "cbbb";
	}
	else {
		$color = "corg";	
	}
	if ($type == 'html') return "<span class=\"{$color}\">{$value}</span>";
	if ($type == 'arr') return array('style'=>$color, 'val'=>$value);
	return $value;
}

//增加退款单
function add_refund($info, $refund_text = '') {
	global $db;
	if ($info['refund_id'] && $info['refund_state'] == 'success') return false;
	if ($info['refund_id']) refund_close($info['refund_id']);
	$order = $db->pe_select('order', array('order_id'=>$info['order_id']));
	$refund_maxmoney = refund_maxmoney($info['order_id'], $info['orderdata_id']);
	$sql_set['refund_id'] = $refund_id = pe_guid('refund|refund_id');
	$sql_set['refund_type'] = 1;
	$sql_set['refund_money'] = $refund_maxmoney['money'];	
	//$sql_set['refund_point'] = $refund_maxmoney['point'];
	$sql_set['refund_last'] = $refund_maxmoney['wl'] > 0 ? 1 : 0;	
	$sql_set['refund_text'] = $refund_text;
	$sql_set['refund_atime'] = time();
	$sql_set['refund_state'] = 'wcheck';
	$sql_set['order_id'] = $info['order_id'];
	$sql_set['order_payment'] = $order['order_payment'];
	$sql_set['order_payment_name'] = $order['order_payment_name'];
	$sql_set['orderdata_id'] = $info['orderdata_id'];
	$sql_set['product_id'] = $info['product_id'];
	$sql_set['product_guid'] = $info['product_guid'];
	$sql_set['product_name'] = $info['product_name'];
	$sql_set['product_rule'] = $info['product_rule'];
	$sql_set['product_logo'] = $info['product_logo'];
	$sql_set['product_money'] = $info['product_money'];	
	//$sql_set['product_point'] = $info['product_point'];
	$sql_set['product_jjmoney'] = $info['product_jjmoney'];
	$sql_set['product_allmoney'] = $info['product_allmoney'];	
	$sql_set['product_num'] = $info['product_num'];
	$sql_set['user_id'] = $order['user_id'];
	$sql_set['user_name'] = $order['user_name'];	
	if ($db->pe_insert('refund', pe_dbhold($sql_set, array('product_rule')))) {
		$db->pe_update('orderdata', array('orderdata_id'=>$info['orderdata_id']), array('refund_id'=>$refund_id, 'refund_state'=>'wcheck'));
		add_refundlog($refund_id, 'add');
		return $sql_set;
	}
	else {
		return false;
	}
}

//计算最大退款金额
function refund_maxmoney($order_id, $orderdata_id) {
	global $db;
	$order = $db->pe_select('order', array('order_id'=>$order_id), 'order_pstate, order_money, order_wl_money, order_product_money, order_quan_money, order_point_money');	
	if (!$order['order_pstate']) return array('money'=>'0.0', 'wl'=>'0.0');
	$orderdata = $db->pe_select('orderdata', array('orderdata_id'=>$orderdata_id));
	//计算最大退款商品金额
	if ($order['order_product_money'] > 0) {
		$youhui = ($orderdata['product_allmoney'] / $order['order_product_money']) * ($order['order_quan_money'] + $order['order_point_money']);
		$max['money'] = pe_num($orderdata['product_allmoney'] - $youhui, 'round', 2);	
	}
	else {
		$max['money'] = 0;
	}
	//$max['point'] = intval($orderdata['product_point']);
	//计算最大退款运费金额
	$last = $db->pe_select('refund', " and `order_id` = '{$order_id}' and `refund_last` = 1 and `refund_state` != 'close'");
	if ($last['refund_id']) {
		if ($last['orderdata_id'] == $orderdata_id) {
			$max['wl'] = pe_num($order['order_wl_money'], 'round', 2);	
		}		
		else {
			$max['wl'] = 0;
		}
	}
	else {
		$refund = $db->pe_select('refund', " and `order_id` = '{$order_id}' and `orderdata_id` = '{$orderdata_id}' and `refund_state` != 'close'");
		$orderdata_num = $db->pe_num('orderdata', array('order_id'=>$order_id));
		$refund_num = $db->pe_num('refund', " and `order_id` = '{$order_id}' and `refund_state` != 'close'");
		$refund_num = $refund['refund_id'] ? $refund_num : $refund_num + 1;
		if ($refund_num == $orderdata_num) {
			$max['wl'] = pe_num($order['order_wl_money'], 'round', 2);		
		}
		else {
			$max['wl'] = 0;	
		}
	}
	//优惠券金额大于商品金额会出现负数
	if ($max['money'] < 0) $max['money'] = 0;
	if ($max['wl'] < 0) $max['wl'] = 0;
	$max['money'] = $max['money'] + $max['wl'];
	return $max;
}

//退款通过操作
function refund_success($info) {
	global $db;
	//先调用原路退回接口
	if ($info['refund_money'] > 0) {	
		$order = $db->pe_select('order', array('order_id'=>$info['order_id']));
		switch ($order['order_payment']) {
			case 'alipay':
				pe_lead("include/plugin/payment/alipay/func.php");
				$json = alipay_refund($info);
			break;
			case 'wxpay':
				pe_lead('hook/wechat.hook.php');	
				$json = wechat_refund($info);
			break;
			default:
				if ($order['order_payment'] == 'balance') {
					add_moneylog($info['user_id'], 'back', $info['refund_money'], "订单退款返还，单号【{$info['order_id']}】");
				}
				$json = array('code'=>1);
			break;
		}
	}
	if (!$json['code']) pe_apidata($json);
	if ($db->pe_update('refund', array('refund_id'=>$info['refund_id']), array('refund_state'=>'success', 'refund_pstate'=>'success'))) {
		//更新对应子订单状态				
		$db->pe_update('orderdata', array('orderdata_id'=>$info['orderdata_id']), array('refund_id'=>$info['refund_id'], 'refund_state'=>'success', 'refund_money'=>$info['refund_money']));
		//如果子订单已全部退款，主订单关闭
		if (!$db->pe_num('orderdata', " and `order_id` = '{$info['order_id']}' and `refund_state` != 'success'")) {
			order_close_callback($info['order_id'], "订单退款关闭", false);
		}
		add_refundlog($info['refund_id'], 'success');
		return true;
	}
	else {
		return false;
	}
}

//退款关闭操作
function refund_close($id, $type = 'one') {
	global $db;
	$id = pe_dbhold($id);
	if ($type == 'all') {
		$info_list = $db->pe_selectall('refund', array('order_id'=>$id, 'refund_state'=>array('wcheck', 'wsend', 'wget', 'refuse')));
	}
	else {
		$info_list = $db->pe_selectall('refund', array('refund_id'=>$id, 'refund_state'=>array('wcheck', 'wsend', 'wget', 'refuse')));
	}
	foreach ($info_list as $v) {
		$db->pe_update('refund', array('refund_id'=>$v['refund_id']), array('refund_state'=>'close'));
		$db->pe_update('orderdata', array('refund_id'=>$v['refund_id']), array('refund_id'=>0, 'refund_state'=>''));
		add_refundlog($v['refund_id'], 'close');			
	}
	return true;
}

//增加退款操作记录
function add_refundlog($refund_id, $type, $text = '') {
	global $db, $ini;
	$info = $db->pe_select('refund', array('refund_id'=>$refund_id));
	//$info['refund_point'] > 0 && $money_desc[] = "{$info['refund_point']}积分";
	$info['refund_money'] > 0 && $money_desc[] = "{$info['refund_money']}元";
	$money_desc = is_array($money_desc) ? implode('，', $money_desc) : '0元';	
	switch ($type) {
		case 'add':
			$sql_set['refundlog_text'] = "发起了《{$ini['refund_type'][$info['refund_type']]}》申请，退款金额：{$money_desc}，说明：{$info['refund_text']}";
		break;
		case 'edit':
			$sql_set['refundlog_text'] = "修改了《{$ini['refund_type'][$info['refund_type']]}》申请，退款金额：{$money_desc}，说明：{$info['refund_text']}";
		break;
		case 'send':
			$sql_set['refundlog_text'] = "已寄回商品，快递公司：{$info['refund_wl_name']}，快递单号：{$info['refund_wl_id']}";
		break;
		case 'close':
			$sql_set['refundlog_text'] = "取消本次申请，退款关闭";
		break;
		case 'agree':
			$sql_set['refundlog_text'] = "同意本次申请，退款金额：{$money_desc}，退货地址：{$info['refund_tname']}，{$info['refund_phone']}，{$info['refund_address']}";
		break;	
		case 'refuse':
			$sql_set['refundlog_text'] = "拒绝本次申请，说明：{$text}";
		break;
		case 'success':
			$sql_set['refundlog_text'] = $text ? $text : "同意本次申请，已退款金额：{$money_desc}";
		break;
	}
	$sql_set['refundlog_atime'] = time();
	$sql_set['refund_id'] = $refund_id;
	if (basename($_SERVER["SCRIPT_NAME"]) == 'user.php' && in_array($type, array('add', 'edit', 'send', 'close'))) {
		$sql_set['user_id'] = $info['user_id'];
		$sql_set['user_name'] = $info['user_name'];
	}
	$db->pe_insert('refundlog', pe_dbhold($sql_set));
}