<?php
/**
 * @copyright   2008-2014 简好网络 <http://www.phpshe.com>
 * @creatdate   2012-0501 koyshe <koyshe@gmail.com>
 */
switch ($act) {
	//####################// 微信txt文件验证 //####################//
	case 'verify_txt':
		die(pe_dbhold($_g_txt));
	break;
	//####################// 获取sessionid //####################//
	case 'session_id':
		$info['session_id'] = session_id();
		pe_apidata(array('code'=>1, 'data'=>$info));
	break;
	//####################// 检测用户名 //####################//
	case 'reg_name':
		$code = $db->pe_num('user', array('user_name'=>pe_dbhold($_g_value))) > 0 ? 0 : 1;
		pe_apidata(array('code'=>$code));
	break;
	//####################// 检测邮箱 //####################//
	case 'reg_email':
		$code = $db->pe_num('user', array('user_email'=>pe_dbhold($_g_value))) > 0 ? 0 : 1;
		pe_apidata(array('code'=>$code));
	break;
	//####################// 检测手机号 //####################//
	case 'phone':
		$code = $db->pe_num('user', " and `user_phone` = '".pe_dbhold($_g_value)."' and `user_id` != '{$user['user_id']}'") > 0 ? 0 : 1;
		pe_apidata(array('code'=>$code));
	break;
	//####################// 检测邮箱 //####################//
	case 'email':
		$code = $db->pe_num('user', " and `user_email` = '".pe_dbhold($_g_value)."' and `user_id` != '{$user['user_id']}'") > 0 ? 0 : 1;
		pe_apidata(array('code'=>$code));
	break;
	//####################// 检测重复绑定银行帐号 //####################//
	case 'userbank':
		$code = $db->pe_num('userbank', " and `userbank_num` = '".pe_dbhold($_g_value)."' and `userbank_id` != '".intval($_g_id)."'") > 0 ? 0 : 1;
		pe_apidata(array('code'=>$code));
	break;
	//####################// 检测图片验证码 //####################//
	case 'authcode':
		$code = ($_g_value && strtolower($_s_authcode) == strtolower($_g_value)) ? 1 : 0;
		pe_apidata(array('code'=>$code));
	break;
	//####################// 检测支付密码是否设置 //####################//
	case 'paypw_setting':
		$code = $user['user_paypw'] ? 1 : 0;
		pe_apidata(array('code'=>$code));
	break;
	//####################// 检测登录 //####################//
	case 'login':
		$code = pe_login('user') ? 1 : 0;
		pe_apidata(array('code'=>$code));
	break;
	//####################// 检测短信验证码 //####################//
	case 'yzm':
		pe_lead('hook/yzmlog.hook.php');
		$code = check_yzm($_g_phone ? $_g_phone : $_g_email, $_g_value) ? 1 : 0;
		pe_apidata(array('code'=>$code));
	break;
	//####################// 发送短信验证码 //####################//
	case 'send_phone_yzm':
	case 'send_email_yzm':
		pe_lead('hook/yzmlog.hook.php');
		$value = pe_dbhold($_g_value);
		if ($act == 'send_phone_yzm') {
			$name = '手机号码';
			$type = 'phone';
		}
		else {
			$name = '邮箱';
			$type = 'email';
		}
		if (!pe_formcheck($type, $value)) pe_apidata(array('code'=>0, 'msg'=>"{$name}有误"));
		$info = $db->pe_select('user', array("user_{$type}"=>$value), 'user_id');
		//检测号码存在/不存在
		if ($_g_exist) {
			if (!$info['user_id']) pe_apidata(array('code'=>0, 'msg'=>"{$name}不存在"));
		}
		else {
			if ($info['user_id']) pe_apidata(array('code'=>0, 'msg'=>"{$name}已存在"));
		}
		//if (!$_g_authcode or strtolower($_s_authcode) != strtolower($_g_authcode)) pe_apidata(array('code'=>0, 'msg'=>'请填写图片验证码'));
		pe_apidata(send_yzm($type, $value));
	break;
	//####################// 短网址解析 //####################//
	case 'dwz':
		$info = $db->pe_select('dwz', array('dwz_id'=>intval($_g_value)));
		if ($info['dwz_id']) {
			pe_apidata(array('code'=>1, 'data'=>array('url'=>$info['dwz_url'])));
		}
		else {
			pe_apidata(array('code'=>0));
		}
	break;
}
?>