<?php
/**
 * @copyright   2008-2014 简好网络 <http://www.phpshe.com>
 * @creatdate   2015-0320 koyshe <koyshe@gmail.com>
 */
session_write_close();
$nowtime = time();
$nowdate = date('Y-m-d');
$lastdate = date('Y-m-d', strtotime('-1 day'));
switch ($act) {
	//销量助手，频率1秒
	case 'buyer':
		//更新机器人状态
		$db->pe_update('buyer', "and buyer_state != 'wstart' and buyer_stime > '{$nowtime}'", array('buyer_state'=>'wstart'));
		$db->pe_update('buyer', "and buyer_state != 'work' and `buyer_num` > `buyer_sellnum` and buyer_stime <= '{$nowtime}'", array('buyer_state'=>'work'));
		$db->pe_update('buyer', "and buyer_state != 'success' and `buyer_num` <= `buyer_sellnum`", array('buyer_state'=>'success'));
		//if (date('G') >= 0 && date('G') < 8) cron_show("{$act}-----wait time");
		//选10个机器人工作
		$buyer_list = $db->pe_selectall('buyer', " and `buyer_state` = 'work' and `buyer_enable` = 1 order by `buyer_ntime` asc", '*', array(10));
		foreach ($buyer_list as $v) {
			if ($v['buyer_ntime'] > $nowtime) continue;
			if ($v['buyer_num'] - $v['buyer_sellnum'] <= 0) continue;
			$buynum = 1;
			$user = $db->pe_select('vuser', array('order by'=>'rand()'));
			$info = $db->pe_select('prodata', "and product_id = '{$v['product_id']}' order by rand()");
			if (!$info['product_id']) continue;
			if (add_buylog($user, $info['product_guid'], $buynum, $v['buyer_ntime'])) {								
				//更新商品销量
				$db->pe_update('product', array('product_id'=>$info['product_id']), "product_sellnum = product_sellnum + {$buynum}, product_sellnum_virtual = product_sellnum_virtual + {$buynum}");
				//更新机器人信息
				$buyer_ntime = $v['buyer_ntime'] + $v['buyer_pinlv'];
				$db->pe_update('buyer', array('buyer_id'=>$v['buyer_id']), array('buyer_sellnum'=>$v['buyer_sellnum'] + $buynum, 'buyer_ntime'=>$buyer_ntime));
			}
		}
	break;
	//日常监测，频率5秒
	default:
		//记录用户ip
		if (!$db->pe_num('iplog', array('iplog_ip'=>pe_dbhold(pe_ip()), 'iplog_adate'=>date('Y-m-d')))) {
			$db->pe_insert('iplog', array('iplog_ip'=>pe_dbhold(pe_ip()), 'iplog_adate'=>date('Y-m-d'), 'iplog_atime'=>time()));
		}

		//删除10天以上购物车商品
		$db->pe_delete('cart', "and `cart_atime` <= ".($nowtime-864000));

		//24小时未付款订单关闭
		$info = $db->pe_select('order', " and `order_state` = 'wpay' and `order_atime` <= ".($nowtime-86400));
		order_close_callback($info, '订单未付款，超时关闭');

		//24小时未成团拼团订单退款关闭
		$info = $db->pe_select('pintuan', " and `pintuan_state` = 'wtuan' and `pintuan_etime` <= {$nowtime}");
		if ($info['pintuan_id']) {
			$db->pe_update('pintuan', array('pintuan_id'=>$info['pintuan_id']), array('pintuan_state'=>'close'));
			$info_list = $db->pe_selectall('order', array('order_type'=>'pintuan', 'pintuan_id'=>$info['pintuan_id']));
			foreach ($info_list as $v) {
				order_close_callback($v, '拼团失败，订单自动关闭退款');
			}
		}
		
		//10天自动确认收货
		$info_list = $db->pe_selectall('order', " and `order_state` = 'wget' and `order_payment` != 'cod' and `order_stime` <=".($nowtime-86400*10), '*', array(2));
		foreach ($info_list as $v) {
			order_success_callback($v);
		}

		//更新活动中商品
		product_huodong_callback();

		//检测短信/邮件通知/微信模板消息
		pe_lead('hook/qunfa.hook.php');
		pe_lead('hook/wechat.hook.php');
		//$db->query("lock tables `".dbpre."noticelog` write");
		$list = $db->index('noticelog_id')->pe_selectall('noticelog', array('noticelog_state'=>'new', 'order by'=>'noticelog_id asc'), '*', array(5));
		$db->pe_update('noticelog', array('noticelog_id'=>array_keys($list)), array('noticelog_stime'=>time(), 'noticelog_state'=>'send'));
		$db->pe_update('noticelog', "and `noticelog_state` = 'send' and `noticelog_stime` <='".(time()-60)."'", array('noticelog_state'=>'new'));
		//$db->query("unlock tables");
		foreach ($list as $k=>$v) {
			if ($v['noticelog_way'] == 'sms') {
				qunfa_sms($v['noticelog_user'], $v['noticelog_text'], $v['noticelog_id']);		
			}
			elseif ($v['noticelog_way'] == 'email') {
				qunfa_email($v['noticelog_user'], array('name'=>$v['noticelog_name'], 'text'=>$v['noticelog_text']), $v['noticelog_id']);		
			}
			elseif ($v['noticelog_way'] == 'wechat') {
				wechat_notice($v);		
			}
		}
		cron_show("{$act}-----success");
	break;	
}
//命令行显示中文结果
function cron_show($text) {
	echo $text;
	//echo iconv('utf-8', 'gbk//ignore', $text);
}
?>