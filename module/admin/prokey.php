<?php
/**
 * @copyright   2008-2014 简好网络 <http://www.phpshe.com>
 * @creatdate   2012-1116 koyshe <koyshe@gmail.com>
 */
$menumark = 'product';
$ini['prokey_state'] = array('new'=>'待发放', 'success'=>'已发放', 'expire'=>'已过期');
switch ($act) {
	//####################// 添加卡密 //####################//
	case 'add':
		$product_id = intval($_g_id);
		if (isset($_p_pesubmit)) {
			pe_token_match();
			for ($num=1; $num<=10; $num++) {
				if ($_p_prokey_value[$num] or $_p_prokey_edate[$num]) {
					if (!$_p_prokey_value[$num]) pe_apidata(array('code'=>0, 'msg'=>"请填写第{$num}个卡密"));
					if (!$_p_prokey_edate[$num]) pe_apidata(array('code'=>0, 'msg'=>"请填写第{$num}个截至日期"));
					$sql_set[$num]['prokey_value'] = $_p_prokey_value[$num];
					$sql_set[$num]['prokey_edate'] = $_p_prokey_edate[$num];
					$sql_set[$num]['prokey_atime'] = time();					
					$sql_set[$num]['product_id'] = $product_id;
				}
			}
			if (!is_array($sql_set)) pe_apidata(array('code'=>0, 'msg'=>"请填写卡密信息"));
			if ($db->pe_insert('prokey', pe_dbhold($sql_set))) {
				prokey_callback($product_id);
				pe_apidata(array('code'=>1, 'msg'=>'添加成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'添加失败'));
			}
		}	
		$seo = pe_seo($menutitle='添加卡密', '', '', 'admin');
		include(pe_tpl('prokey_add.html'));
	break;
	//####################// 导入卡密 //####################//
	case 'import':
		$product_id = intval($_g_id);	
		if (isset($_p_pesubmit)) {
			pe_token_match();
			if ($_FILES['file']['size']) {
				pe_lead('include/class/upload.class.php');
				$upload = new upload($_FILES['file'], 'data/attachment/import/', array('local'=>1));
				$excel_name = $upload->filepath_full;
			}
			if (!is_file($excel_name)) pe_error('请选择要导入的xls文件...');		
			pe_lead('include/class/excel/reader.php');
			$excel_in = new Spreadsheet_Excel_Reader();
			$excel_in->setOutputEncoding('utf-8');
			$excel_in->read("{$excel_name}");
			$info_list = array();
			for ($num = 2; $num <= $excel_in->sheets[0]['numRows']; $num++) {
				$prokey_value = trim($excel_in->sheets[0]['cells'][$num][1]);
				$prokey_edate = trim($excel_in->sheets[0]['cells'][$num][2]);
				if ($prokey_value && $prokey_edate) {
					$sql_set[$num]['prokey_value'] = $prokey_value;
					$sql_set[$num]['prokey_edate'] = $prokey_edate;
					$sql_set[$num]['prokey_atime'] = time();					
					$sql_set[$num]['product_id'] = $product_id;
				}
			}
			if (!is_array($sql_set)) pe_error('未读取到完整的卡密信息...');
			if ($db->pe_insert('prokey', pe_dbhold($sql_set))) {
				prokey_callback($product_id);
				pe_success('已成功导入 '.count($sql_set).' 条！', '', 'dialog');
			}
			else {
				pe_error('导入失败');
			}
		}	
		$seo = pe_seo($menutitle='导入卡密', '', '', 'admin');
		include(pe_tpl('prokey_import.html'));
	break;
	//####################// 修改卡密 //####################//
	case 'edit':
		$prokey_id = intval($_g_id);
		if (isset($_p_pesubmit)) {
			pe_token_match();
			if (!$_p_info['prokey_value']) pe_apidata(array('code'=>0, 'msg'=>'请填写卡密'));
			if (!$_p_info['prokey_edate']) pe_apidata(array('code'=>0, 'msg'=>'请填写截至日期'));
			if ($db->pe_update('prokey', array('prokey_id'=>$prokey_id), pe_dbhold($_p_info))) {
				pe_apidata(array('code'=>1, 'msg'=>'修改成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'修改失败'));
			}
		}
		$info = $db->pe_select('prokey', array('prokey_id'=>$prokey_id));
		$seo = pe_seo($menutitle='修改卡密', '', '', 'admin');
		include(pe_tpl('prokey_edit.html'));
	break;
	//####################// 删除卡密 //####################//
	case 'del':
		pe_token_match();
		$prokey_id = is_array($_p_prokey_id) ? $_p_prokey_id : intval($_g_id);
		if ($db->pe_delete('prokey', array('prokey_id'=>$prokey_id))) {
			pe_apidata(array('code'=>1, 'msg'=>'删除成功'));
		}
		else {
			pe_apidata(array('code'=>0, 'msg'=>'删除失败'));
		}
	break;
	//####################// 卡密管理 //####################//
	default:
		$product_id = intval($_g_id);
		//检测过期卡密
		prokey_check($product_id);
		$info_list = $db->pe_selectall('prokey', array('product_id'=>$product_id, 'prokey_state'=>pe_dbhold($_g_state), 'order by'=>'prokey_id desc'), '*', array(100, $_g_page));
		$tongji_arr = $db->index('prokey_state')->pe_selectall('prokey', array('product_id'=>$product_id, 'group by'=>'prokey_state'), 'prokey_state, count(1) as `num`');
		foreach ($ini['prokey_state'] as $k=>$v) {
			$tongji[$k] = intval($tongji_arr[$k]['num']);
		}		
		$seo = pe_seo($menutitle='卡密设置', '', '', 'admin');
		include(pe_tpl('prokey_list.html'));
	break;
}

//检测待发货的虚拟订单
function prokey_callback($product_id) {
	global $db;
	$sql = "select a.* from (select * from `".dbpre."order` where `order_virtual` = 1 and order_state = 'wsend') a, `".dbpre."orderdata` b where a.order_id = b.order_id and b.product_id = '{$product_id}' order by a.order_ptime asc";
	$list = $db->sql_selectall($sql);
	foreach ($list as $v) {
		virtual_order_send_callback($v);
	}
}
?>