<?php
/**
 * @copyright   2008-2021 简好网络 <http://www.phpshe.com>
 * @creatdate   2012-0501 koyshe,andrea <koyshe@gmail.com>
 */
$menumark = 'product';
pe_lead('hook/category.hook.php');
$category_treelist = category_treelist();
$cache_brand = cache::get('brand');
$cache_userlevel = cache::get('userlevel');
switch ($act) {
	//####################// 商品添加 //####################//
	case 'add':
		if (isset($_p_pesubmit)) {
			pe_token_match();
			product_check();
			if ($_g_type) $_p_info['product_type'] = $_g_type;
            $_p_info['product_atime'] = time();
			if ($product_id = $db->pe_insert('product', pe_dbhold($_p_info, array('product_text')))) {
				product_callback($product_id);
				pe_apidata(array('code'=>1, 'msg'=>'添加成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'添加失败'));
			}
		}
		$info['product_type'] = $_g_type;
		$info['wlmb_id'] = 1;
		$album_list = array();
		$product_zhekou = array();
		$product_rule = array();
		$prodata_list = array();
		$wlmb_list = $db->pe_selectall('wlmb', array('order by'=>'`wlmb_id` asc'));
		$seo = pe_seo($menutitle="添加商品", '', '', 'admin');
		include(pe_tpl('product_add.html'));
	break;
	//####################// 商品修改 //####################//
	case 'edit':
		$product_id = intval($_g_id);
		$info = $db->pe_select('product', array('product_id'=>$product_id));
		if (isset($_p_pesubmit)) {
			pe_token_match();
			product_check($info);
			if ($db->pe_update('product', array('product_id'=>$product_id), pe_dbhold($_p_info, array('product_text')))) {
				product_callback($product_id);
				pe_apidata(array('code'=>1, 'msg'=>'修改成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'修改失败'));
			}
		}
		$album_list = explode(',', $info['product_album']);
		$product_zhekou = $info['product_zhekou'] ? json_decode($info['product_zhekou'], true) : array();
		$product_rule = $info['product_rule'] ? json_decode($info['product_rule'], true) : array();
		$prodata_list = $db->index('product_ruleid')->pe_selectall('prodata', array('product_id'=>$product_id));
		$wlmb_list = $db->pe_selectall('wlmb', array('order by'=>'`wlmb_id` asc'));
		$seo = pe_seo($menutitle="修改商品", '', '', 'admin');
		include(pe_tpl('product_add.html'));
	break;
	//####################// 商品删除 //####################//
	case 'del':
		pe_token_match();
		$product_id = is_array($_p_product_id) ? pe_dbhold($_p_product_id) : intval($_g_id);
		if ($db->pe_delete('product', array('product_id'=>$product_id))) {
			$db->pe_delete('prodata', array('product_id'=>$product_id));
			$db->pe_delete('prokey', array('product_id'=>$product_id));
			//删除活动中商品
			huodong_product_del($product_id);
			pe_apidata(array('code'=>1, 'msg'=>'删除成功'));
		}
		else {
			pe_apidata(array('code'=>0, 'msg'=>'删除失败'));
		}
	break;
	//####################// 商品排序 //####################//
	case 'order':
		pe_token_match();
		foreach ($_p_product_order as $k=>$v) {
			$result = $db->pe_update('product', array('product_id'=>intval($k)), array('product_order'=>intval($v)));
		}
		if ($result) {
			pe_apidata(array('code'=>1, 'msg'=>'排序成功'));
		}
		else {
			pe_apidata(array('code'=>0, 'msg'=>'排序失败'));
		}
	break;
	//####################// 商品上下架 //####################//
	case 'state':
		pe_token_match();
		$product_id = is_array($_p_product_id) ? pe_dbhold($_p_product_id) : intval($_g_id);
		if ($db->pe_update('product', array('product_id'=>$product_id), array('product_state'=>intval($_g_value)))) {
			if ($_g_value == 0) {
				//删除活动中商品
				huodong_product_del($product_id);			
			}
			pe_apidata(array('code'=>1, 'msg'=>'操作成功'));
		}
		else {
			pe_apidata(array('code'=>0, 'msg'=>'操作失败'));
		}
	break;
	//####################// 设置推荐/新品 //####################//
	case 'tuijian':
	case 'xinpin':
		pe_token_match();
		$product_id = is_array($_p_product_id) ? pe_dbhold($_p_product_id) : intval($_g_id);
		if ($db->pe_update('product', array('product_id'=>$product_id), array("product_{$act}"=>intval($_g_value)))) {
			pe_apidata(array('code'=>1, 'msg'=>'操作成功'));
		}
		else {
			pe_apidata(array('code'=>0, 'msg'=>'排序失败'));
		}
	break;
	//####################// 商品批量转移 //####################//
	case 'move':
		if (isset($_p_pesubmit)) {
			pe_token_match();
			$category_newid = intval($_p_category_newid);
			if (!$category_newid) pe_apidata(array('code'=>0, 'msg'=>'请选择分类'));
			if ($_g_category_id) {
				$result = $db->pe_update('product', array('category_id'=>intval($_p_category_id)), array('category_id'=>$category_newid));
			}
			else {
				$result = $db->pe_update('product', array('product_id'=>pe_dbhold(explode(',', $_g_id))), array('category_id'=>$category_newid));
			}
			if ($result) {
				pe_apidata(array('code'=>1, 'msg'=>'操作成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'排序失败'));
			}
		}
		$seo = pe_seo($menutitle='转移商品', '', '', 'admin');
		include(pe_tpl('product_move.html'));
	break;
	//####################// 快速咨询 //####################//
	case 'ask':
		$product_id = intval($_g_id);
		if (isset($_p_pesubmit)) {
			pe_token_match();
			$info = $db->pe_select('product', array('product_id'=>$product_id), 'product_id, product_name, product_logo');
			$sql_set['ask_text'] = $_p_ask_text;
			$sql_set['ask_atime']= $_p_ask_atime ? strtotime($_p_ask_atime) : time();
			$sql_set['product_id'] = $info['product_id'];
			$sql_set['product_name'] = $info['product_name'];
			$sql_set['product_logo'] = $info['product_logo'];
			$sql_set['user_name'] = $_p_user_name;
			$sql_set['user_ip'] = pe_ip();
			$user = $db->pe_select('user', array('user_name'=>pe_dbhold($_p_user_name)));
			if ($user['user_id']) {
				$sql_set['user_id'] = $user['user_id'];	
			}
			if ($_p_ask_replytext) {
				$sql_set['ask_replytext'] = $_p_ask_replytext;				
				$sql_set['ask_replytime'] = $sql_set['ask_atime'] + rand(300, 600);
				$sql_set['ask_state'] = 1;			
			}
			if ($db->pe_insert('ask', pe_dbhold($sql_set))) {
				product_jsnum($product_id, 'asknum');
				pe_apidata(array('code'=>1, 'msg'=>'添加成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'添加失败'));
			}
		}
		$info = $db->pe_select('product', array('product_id'=>$product_id));
		$seo = pe_seo($menutitle='添加咨询', '', '', 'admin');
		include(pe_tpl('product_ask.html'));
	break;
	//####################// 快速评价 //####################//
	case 'comment':
		$product_id = intval($_g_id);
		$info = $db->pe_select('product', array('product_id'=>$product_id), 'product_id, product_name, product_logo');
		if (isset($_p_pesubmit)) {
			pe_token_match();
			if (!$_p_user_name) pe_apidata(array('code'=>0, 'msg'=>'请填写用户'));
			if (!$_p_comment_text) pe_apidata(array('code'=>0, 'msg'=>'请填写内容'));
			$sql_set['comment_star'] = intval($_p_comment_star);
			$sql_set['comment_text'] = $_p_comment_text;
			$sql_set['comment_logo'] = implode(',', array_filter($_p_comment_logo));		
			$sql_set['comment_atime']= $_p_comment_atime ? strtotime($_p_comment_atime) : time();
			$sql_set['product_id'] = $info['product_id'];
			$sql_set['product_name'] = $info['product_name'];
			$sql_set['product_logo'] = $info['product_logo'];
			$sql_set['user_name'] = $_p_user_name;
			$sql_set['user_logo'] = $_p_user_logo;
			$sql_set['user_ip'] = pe_ip();
			$user = $db->pe_select('user', array('user_name'=>pe_dbhold($sql_set['user_name'])));
			if ($user['user_id']) {
				$sql_set['user_id'] = $user['user_id'];	
			}
			if ($db->pe_insert('comment', pe_dbhold($sql_set))) {
				product_jsnum($product_id, 'commentnum');
				pe_apidata(array('code'=>1, 'msg'=>'评价成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'评价失败'));
			}
		}
		$comment_logo = array();
		$seo = pe_seo($menutitle='添加评价', '', '', 'admin');
		include(pe_tpl('product_comment.html'));
	break;
	//####################// 设置销量 //####################//
	case 'sell':
		$product_id = intval($_g_id);
		if (isset($_p_pesubmit)) {
			pe_token_match();
			if ($db->pe_update('product', array('product_id'=>$product_id), "`product_sellnum` = ".intval($_p_product_sellnum))) {
				pe_apidata(array('code'=>1, 'msg'=>'设置成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'设置失败'));
			}
		}
		$info = $db->pe_select('product', array('product_id'=>$product_id));
		$seo = pe_seo($menutitle='设置销量', '', '', 'admin');
		include(pe_tpl('product_sell.html'));
	break;
	//####################// 商品列表 //####################//
	default:
		$cache_category = cache::get('category');
		$orderby_arr['num|desc'] = '库存量（多到少）';
		$orderby_arr['num|asc'] = '库存量（少到多）';
		$orderby_arr['sellnum|desc'] = '销售量（多到少）';
		$orderby_arr['sellnum|asc'] = '销售量（少到多）';
		//$orderby_arr['asknum|desc'] = '咨询数(多到少)';
		//$orderby_arr['asknum|asc'] = '咨询数(少到多)';
		$orderby_arr['commentnum|desc'] = '评价数（多到少）';
		$orderby_arr['commentnum|asc'] = '评价数（少到多）';
		$filter_arr = array('istuijian|1'=>'推荐商品', 'wlmoney|0'=>'包邮商品', 'num|0'=>'售空商品');
		$_g_name && $sql_where .= " and `product_name` like '%".pe_dbhold($_g_name)."%'";
		$_g_type && $sql_where .= " and `product_type` = '".pe_dbhold($_g_type)."'";
		//$_g_state && $sqlwhere .= " and `product_state` = '{$_g_state}'";
		$_g_category_id && $sql_where .= category_get_product($_g_category_id);
		$_g_brand_id && $sql_where .= " and `brand_id` = '".intval($_g_brand_id)."'";
		switch ($_g_filter) {
			case 'xiajia':
				$sql_where .= " and `product_state` = 0";			
			break;
			case 'quehuo':
				$sql_where .= " and `product_num` = 0";			
			break;
			case 'tuijian':
				$sql_where .= " and `product_tuijian` = 1";			
			break;
			case 'xinpin':
				$sql_where .= " and `product_xinpin` = 1";			
			break;
			default:
				$sql_where .= " and `product_state` = 1";		
			break;
		}
		$sql_where .= ' order by';
		if ($_g_orderby) {
			$orderby = explode('|', $_g_orderby);
			$sql_where .= " `product_{$orderby[0]}` {$orderby[1]},";
		}
		$sql_where .= " `product_order` asc, `product_id` desc";
		$info_list = $db->pe_selectall('product', $sql_where, '*', array(100, $_g_page));

		$tongji['all'] = $db->pe_num('product', array('product_state'=>1));
		$tongji['xiajia'] = $db->pe_num('product', array('product_state'=>0));
		$tongji['quehuo'] = $db->pe_num('product', array('product_num'=>0));
		$tongji['tuijian'] = $db->pe_num('product', array('product_tuijian'=>1));
		$tongji['xinpin'] = $db->pe_num('product', array('product_xinpin'=>1));
		$seo = pe_seo($menutitle='商品列表', '', '', 'admin');
		include(pe_tpl('product_list.html'));
	break;
}

function product_check($info = array()) {
	global $db;
	if (!is_array($_POST['product_ruleid'])) pe_apidata(array('code'=>0, 'msg'=>'价格库存有误'));
	//如果有参与活动的商品，是禁止修改商品规格的
	$product_rule = is_array($_POST['product_rule']) ? json_encode($_POST['product_rule']) : ''; 
	if ($info['product_rule'] == $product_rule) return true;
	if ($db->pe_num('huodong_product', " and `product_id` = '{$info['product_id']}' and `huodong_etime` >='".time()."'")) pe_apidata(array('code'=>0, 'msg'=>'活动中商品不可修改规格'));
}

function product_callback($product_id) {
	global $db;
	foreach ($_POST['product_ruleid'] as $k=>$v) {
		$product_ruleid = pe_dbhold($_POST['product_ruleid'][$k]);
		$product_rulename = pe_dbhold($_POST['product_rulename'][$k]);
		$sql_set[$k]['product_id'] = $product_id;
		$sql_set[$k]['product_guid'] = $product_rulename ? md5("{$product_id}|{$product_rulename}") : md5($product_id);
		$sql_set[$k]['product_ruleid'] = $product_ruleid;
		$sql_set[$k]['product_rulename'] = $product_rulename;
		$sql_set[$k]['product_money'] = pe_num($_POST['product_money'][$k], 'round', 2);
		$sql_set[$k]['product_ymoney'] = pe_num($_POST['product_ymoney'][$k], 'round', 2);
		$sql_set[$k]['product_point'] = intval($_POST['product_point'][$k]);
		$sql_set[$k]['product_sn'] = pe_dbhold($_POST['product_sn'][$k]);
		$sql_set[$k]['product_num'] = intval($_POST['product_num'][$k]);
		$sql_set[$k]['product_order'] = $k+1;
		$product_num += $sql_set[$k]['product_num'];
	}
	//更新prodata表
	$db->pe_delete('prodata', array('product_id'=>$product_id));
	$db->pe_insert('prodata', $sql_set);
	//更新product表
	$sqlset_product['product_virtual'] = intval($_POST['product_virtual']);
	$prodata = $db->pe_select('prodata', array('product_id'=>$product_id, 'order by'=>'product_money asc'));
	$sqlset_product['product_guid'] = $prodata['product_guid'];
	$sqlset_product['product_money'] = $prodata['product_money'];
	$sqlset_product['product_ymoney'] = $prodata['product_ymoney'];
	$sqlset_product['product_num'] = $product_num;
	$sqlset_product['product_rule'] = is_array($_POST['product_rule']) ? json_encode($_POST['product_rule']) : '';
	//更新相册和logo
	$product_album = array_values(array_filter($_POST['product_album']));
	$sqlset_product['product_logo'] = $product_album[0];
	$sqlset_product['product_album'] = implode(',', $product_album);
	//更新会员折扣价
	$sqlset_product['product_zhekou'] = '';
	if (is_array($_POST['product_zhekou'])) {
		foreach ($_POST['product_zhekou'] as $k=>$v) {
			$v > 0 && $product_zhekou[$k] = pe_num($v/100, 'round', 3);
		}
		if (is_array($product_zhekou)) $sqlset_product['product_zhekou'] = json_encode($product_zhekou);
	}
	$db->pe_update('product', array('product_id'=>$product_id), pe_dbhold($sqlset_product, array('product_rule', 'product_zhekou')));
	product_huodong_callback();
}

function huodong_product_del($product_id = 0) {
	global $db;
	$huodong_list = $db->pe_selectall('huodong_product', " and product_id = '{$product_id}' and `huodong_etime` >='".time()."'");
	foreach ($huodong_list as $v) {
		$db->pe_delete('huodong_product', array('product_id'=>$v['product_id'], 'huodong_id'=>$v['huodong_id']));
		$db->pe_delete('huodong_prodata', array('product_id'=>$v['product_id'], 'huodong_id'=>$v['huodong_id']));		
	}
}
?>