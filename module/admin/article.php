<?php
/**
 * @copyright   2008-2021 简好网络 <http://www.phpshe.com>
 * @creatdate   2012-0501 koyshe <koyshe@gmail.com>
 */
$menumark = 'article';
$cache_category = cache::get('article_category');
$cache_category_arr = cache::get('article_category_arr');
switch ($act) {
	//####################// 文章添加 //####################//
	case 'add':
		$info['category_type'] = $_g_type;
		if (isset($_p_pesubmit)) {
			pe_token_match();
			$_p_info['article_atime'] = $_p_info['article_atime'] ? strtotime($_p_info['article_atime']) : time();
			$_p_info['category_type'] = $info['category_type'];		
			if ($db->pe_insert('article', pe_dbhold($_p_info, array('article_text')))) {
				pe_apidata(array('code'=>1, 'msg'=>'添加成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'添加失败'));
			}
		}
		$seo = pe_seo($menutitle='添加文章', '', '', 'admin');
		include(pe_tpl('article_add.html'));
	break;
	//####################// 文章修改 //####################//
	case 'edit':
		$article_id = intval($_g_id);
		if (isset($_p_pesubmit)) {
			pe_token_match();
			$_p_info['article_atime'] = $_p_info['article_atime'] ? strtotime($_p_info['article_atime']) : time();
			if ($db->pe_update('article', array('article_id'=>$article_id), pe_dbhold($_p_info, array('article_text')))) {
				pe_apidata(array('code'=>1, 'msg'=>'修改成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'修改失败'));
			}
		}
		$info = $db->pe_select('article', array('article_id'=>$article_id));
		$seo = pe_seo($menutitle='修改文章', '', '', 'admin');
		include(pe_tpl('article_add.html'));
	break;
	//####################// 文章删除 //####################//
	case 'del':
		pe_token_match();
		$article_id = is_array($_p_article_id) ? $_p_article_id : intval($_g_id);
		if ($db->pe_delete('article', array('article_id'=>$article_id))) {
			pe_apidata(array('code'=>1, 'msg'=>'删除成功'));
		}
		else {
			pe_apidata(array('code'=>0, 'msg'=>'删除失败'));
		}
	break;
	//####################// 文章列表 //####################//
	default:
		$sql_where = " and `category_type` = '{$_g_type}'";
		$_g_name && $sql_where .= " and `article_name` like '%{$_g_name}%'";
		$_g_category_id && $sql_where .= " and `category_id` = '{$_g_category_id}'";
		$sql_where .= " order by `article_id` desc";
		$info_list = $db->pe_selectall('article', $sql_where, '*', array(100, $_g_page));

		$tongji = $db->index('category_type')->pe_selectall('article', array('group by'=>'category_type'), "count(1) as `num`, category_type");
		foreach ($ini['article_category_type'] as $k=>$v){
			$tongji[$k] = intval($tongji[$k]['num']);
			$tongji['all'] += $tongji[$k]; 
		}

		$seo = pe_seo($menutitle='文章列表', '', '', 'admin');
		include(pe_tpl('article_list.html'));
	break;
}
?>