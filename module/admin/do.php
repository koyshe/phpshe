<?php
/**
 * @copyright   2008-2021 简好网络 <http://www.phpshe.com>
 * @creatdate   2012-0501 koyshe <koyshe@gmail.com>
 */
switch ($act) {
	//####################// 管理员退出 //####################//
	case 'logout':
		unset($_SESSION['admin_idtoken'], $_SESSION['admin_id'], $_SESSION['admin_name']);
		pe_apidata(array('code'=>1, 'msg'=>'已退出登录'));
	break;
	//####################// 管理员登录 //####################//
	default:
		if (isset($_p_pesubmit)) {
			$sql_set['admin_name'] = $_p_admin_name;
			$sql_set['admin_pw'] = md5($_p_admin_pw);
			if (!$_p_authcode || strtolower($_s_authcode) != strtolower($_p_authcode)) pe_apidata(array('code'=>0, 'msg'=>'验证码错误'));
			if ($info = $db->pe_select('admin', pe_dbhold($sql_set))) {
				$db->pe_update('admin', array('admin_id'=>$info['admin_id']), array('admin_ltime'=>time()));
				$_SESSION['admin_idtoken'] = md5($info['admin_id'].$pe['host']);
				$_SESSION['admin_id'] = $info['admin_id'];
				$_SESSION['admin_name'] = $info['admin_name'];
				$_SESSION['admin_ltime'] = $info['admin_ltime'];
				$_SESSION['pe_token'] = pe_token_set($_SESSION['admin_idtoken']);
				pe_apidata(array('code'=>1, 'msg'=>'登录成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'帐号或密码错误'));
			}
		}
		$seo = pe_seo('管理员登录', '', '', 'admin');
		include(pe_tpl('do_login.html'));
	break;
}
?>