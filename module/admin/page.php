<?php
/**
 * @copyright   2008-2014 简好网络 <http://www.phpshe.com>
 * @creatdate   2012-0501 koyshe <koyshe@gmail.com>
 */
$menumark = 'page';
switch ($act) {
	//#####################@ 单页添加 @#####################//
	case 'add':
		if (isset($_p_pesubmit)) {
			pe_token_match();
			if (is_numeric($_p_info['page_mark'])) pe_apidata(array('code'=>0, 'msg'=>'标识不能为数字'));
			if ($db->pe_insert('page', pe_dbhold($_p_info, array('page_text')))) {
				pe_apidata(array('code'=>1, 'msg'=>'添加成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'添加失败'));
			}
		}
		$seo = pe_seo($menutitle='添加单页', '', '', 'admin');
		include(pe_tpl('page_add.html'));
	break;
	//#####################@ 单页修改 @#####################//
	case 'edit':
		if ($_g_mark) {
			$sql_where['page_mark'] = pe_dbhold($_g_mark);
		}
		else {
			$page_id = intval($_g_id);
			$sql_where['page_id'] = $page_id;		
		}
		if (isset($_p_pesubmit)) {
			pe_token_match();
            if (is_numeric($_p_info['page_mark'])) pe_apidata(array('code'=>0, 'msg'=>'标识不能为数字'));
			if ($db->pe_update('page', $sql_where, pe_dbhold($_p_info, array('page_text')))) {
				pe_apidata(array('code'=>1, 'msg'=>'修改成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'修改失败'));
			}
		}
		$info = $db->pe_select('page', $sql_where);
		$seo = pe_seo($menutitle='修改单页', '', '', 'admin');
		include(pe_tpl('page_add.html'));
	break;
	//#####################@ 单页删除 @#####################//
	case 'del':
		pe_token_match();
		$page_id = is_array($_p_page_id) ? $_p_page_id : intval($_g_id);
		if ($db->pe_delete('page', array('page_id'=>$page_id))) {
			pe_apidata(array('code'=>1, 'msg'=>'删除成功'));
		}
		else {
			pe_apidata(array('code'=>0, 'msg'=>'删除失败'));
		}
	break;
	//#####################@ 单页列表 @#####################//
	default :
		$info_list = $db->pe_selectall('page', array('order by'=>'`page_id` desc'), '*', array(100, $_g_page));
		$seo = pe_seo($menutitle='单页列表', '', '', 'admin');
		include(pe_tpl('page_list.html'));
	break;
}
?>