<?php
/**
 * @copyright   2008-2014 简好网络 <http://www.phpshe.com>
 * @creatdate   2012-0501 koyshe <koyshe@gmail.com>
 */
$menumark = 'user';
$cache_userlevel = cache::get('userlevel');
$cache_userlevel_arr = cache::get('userlevel_arr');
switch ($act) {
	//####################// 会员添加 //####################//
	case 'add':
		if (isset($_p_pesubmit)) {
			pe_token_match();
			if (!$_p_user_name) pe_apidata(array('code'=>0, 'msg'=>'请填写用户帐号'));
			if ($db->pe_num('user', array('user_name'=>$_p_user_name))) pe_apidata(array('code'=>0, 'msg'=>'用户名已存在'));
			if (!$_p_user_pw) pe_apidata(array('code'=>0, 'msg'=>'请填写密码'));
			//if (!$_p_user_phone) pe_apidata(array('code'=>0, 'msg'=>'请填写手机号'));
			if ($_p_keyword && !$_p_tguser_id) pe_apidata(array('code'=>0, 'msg'=>'请先验证用户'));
			$sql_set['user_name'] = $_p_user_name;
			$sql_set['user_pw'] = md5($_p_user_pw);
			$sql_set['user_phone'] = $_p_user_phone;
			$sql_set['user_tname'] = $_p_user_tname;
			$sql_set['user_email'] = $_p_user_email;
			$sql_set['user_ip'] = pe_ip();
			$sql_set['user_atime'] = $sql_set['user_ltime'] = time();
			//$sql_set['user_adate'] = $sql_set['user_ldate'] = date('Y-m-d');
			//记录推荐人
			if ($_p_tguser_id) {
				$tguser = $db->pe_select('user', array('user_id'=>intval($_p_tguser_id)), 'user_id, user_name');
				if ($tguser['user_id']) {
					$sql_set['tguser_id'] = $tguser['user_id'];
					$sql_set['tguser_name'] = $tguser['user_name'];
				}
			}
			if ($user_id = $db->pe_insert('user', pe_dbhold($sql_set))) {
				add_pointlog($user_id, 'add', $cache_setting['point_reg'], '新用户注册奖励');
				userinfo_callback($user_id);
				if ($tguser['user_id']) {
					add_pointlog($tguser['user_id'], 'add', $cache_setting['point_tg'], "邀请用户《{$_p_user_name}》注册奖励");
					userinfo_callback($tguser['user_id']);
				}
				pe_apidata(array('code'=>1, 'msg'=>'添加成功', 'id'=>$user_id));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'添加失败'));
			}
		}
		$seo = pe_seo($menutitle='添加会员', '', '', 'admin');
		include(pe_tpl('user_add.html'));
	break;
	//####################// 会员修改 //####################//
	case 'edit':
		$user_id = intval($_g_id);
		$info = $db->pe_select('user', array('user_id'=>$user_id));
		if (isset($_p_pesubmit)) {
			pe_token_match();
			if ($db->pe_update('user', array('user_id'=>$user_id), pe_dbhold($_p_info))) {
				userinfo_callback($user_id);
				pe_apidata(array('code'=>1, 'msg'=>'修改成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'修改失败'));
			}
		}
		$info['user_atime'] = pe_date($info['user_atime']);
		$info['user_ltime'] = pe_date($info['user_ltime']);
		$userlevel_default_id = get_userlevel($info, 'userlevel_id', true);
		foreach ($cache_userlevel_arr['auto'] as $v) {
			if ($v['userlevel_id'] == $userlevel_default_id) {
				$userlevel_list[] = array('id'=>$v['userlevel_id'], 'name'=>"【默认】{$v['userlevel_name']}（".($v['userlevel_zhe']*100)."%）");
			}
		}
		foreach ($cache_userlevel_arr['hand'] as $v) {
			if ($v['userlevel_id'] == $userlevel_default_id) {
				$userlevel_list[] = array('id'=>$v['userlevel_id'], 'name'=>"【指定】{$v['userlevel_name']}（".($v['userlevel_zhe']*100)."%）");
			}
		}			
		$info['userlevel_list'] = $userlevel_list;
		$seo = pe_seo($menutitle='修改会员', '', '', 'admin');
		include(pe_tpl('user_edit.html'));
	break;
	//####################// 会员删除 //####################//
	case 'del':
		pe_token_match();
		$user_id = is_array($_p_user_id) ? $_p_user_id : intval($_g_id);
		if ($db->pe_delete('user', array('user_id'=>$user_id))) {
			$db->pe_delete('user_openid', array('user_id'=>$user_id));
			pe_apidata(array('code'=>1, 'msg'=>'删除成功'));
		}
		else {
			pe_apidata(array('code'=>0, 'msg'=>'删除失败'));
		}
	break;
	//####################// 充值(扣除)金额 //####################//
	case 'addmoney':
	case 'delmoney':
		$user_id = intval($_g_id);
		$info = $db->pe_select('user', array('user_id'=>$user_id));
		if (isset($_p_pesubmit)) {
			pe_token_match();
			$type = $_p_type;
			$value = pe_num($_p_value, 'floor', 2);
			if (!$user_id) pe_apidata(array('code'=>0, 'msg'=>'用户不存在'));
			if (!$type) pe_apidata(array('code'=>0, 'msg'=>'请选择操作类型'));
			if ($value < 0.01) pe_apidata(array('code'=>0, 'msg'=>'请填写金额'));			
			//if ($type == 'del' && $value > $info['user_money']) pe_apidata(array('code'=>0, 'msg'=>'余额不足'));
			if (!$_p_text) pe_apidata(array('code'=>0, 'msg'=>'请填写说明'));
			if (add_moneylog($user_id, $type, $value, $_p_text)) {
				pe_apidata(array('code'=>1, 'msg'=>'操作成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'操作失败'));
			}
		}
		$seo = pe_seo($menutitle='充值(扣除)金额', '', '', 'admin');
		include(pe_tpl('user_addmoney.html'));
	break;
	//####################// 充值(扣除)积分 //####################//
	case 'addpoint':
	case 'delpoint':
		$user_id = intval($_g_id);
		$info = $db->pe_select('user', array('user_id'=>$user_id));
		if (isset($_p_pesubmit)) {
			pe_token_match();
			$type = $_p_type;
			$value = intval($_p_value);
			if (!$user_id) pe_apidata(array('code'=>0, 'msg'=>'用户不存在'));
			if (!$type) pe_apidata(array('code'=>0, 'msg'=>'请选择操作类型'));
			if ($value < 1) pe_apidata(array('code'=>0, 'msg'=>'请填写积分'));
			//if ($type == 'del' && $value > $info['user_point']) pe_apidata(array('code'=>0, 'msg'=>'余额不足'));			
			if (!$_p_text) pe_apidata(array('code'=>0, 'msg'=>'请填写说明'));
			if (add_pointlog($user_id, $type, $value, $_p_text)) {
				pe_apidata(array('code'=>1, 'msg'=>'操作成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'操作失败'));
			}
		}
		$seo = pe_seo($menutitle='增加(扣除)积分', '', '', 'admin');
		include(pe_tpl('user_addpoint.html'));
	break;
	//####################// 修改帐号 //####################//
	case 'setname':
		$user_id = intval($_g_id);
		$info = $db->pe_select('user', array('user_id'=>$user_id));
		if (isset($_p_pesubmit)) {
			pe_token_match();
			if (mb_strlen($_p_value, 'utf8') < 2 or mb_strlen($_p_value, 'utf8') > 15) pe_apidata(array('code'=>0, 'msg'=>'帐号为2-15位字符'));
			if (!pe_formcheck('uname', $_p_value)) pe_apidata(array('code'=>0, 'msg'=>'帐号有特殊字符'));
			if ($db->pe_num('user', array('user_name'=>pe_dbhold($_p_value)))) pe_apidata(array('code'=>0, 'msg'=>'帐号已存在'));
			$sql_set['user_name'] = $_p_value;
			if ($db->pe_update('user', array('user_id'=>$user_id), pe_dbhold($sql_set))) {
				$sqlset_usernamelog['usernamelog_atime'] = time();
				$sqlset_usernamelog['user_id'] = $info['user_id'];
				$sqlset_usernamelog['user_oldname'] = $info['user_name'];
				$sqlset_usernamelog['user_newname'] = $_p_value;
				$db->pe_insert('usernamelog', pe_dbhold($sqlset_usernamelog));
				//更新user字段
				pe_syn_filed_value('user_id', $info['user_id'], 'user_name', $_p_value);
				//更新tguser字段
				pe_syn_filed_value('tguser_id', $info['user_id'], 'tguser_name', $_p_value);
				pe_apidata(array('code'=>1, 'msg'=>'修改成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'修改失败'));
			}
		}
		$seo = pe_seo($menutitle='修改帐号', '', '', 'admin');
		include(pe_tpl('user_setname.html'));
	break;
	//####################// 修改密码 //####################//
	case 'setpw':
	case 'setpaypw':
		$user_id = intval($_g_id);
		$info = $db->pe_select('user', array('user_id'=>$user_id));
		if (isset($_p_pesubmit)) {
			pe_token_match();
			if (!$_p_value) pe_apidata(array('code'=>0, 'msg'=>'请填写密码'));
			if ($act == 'setpw') {
				$sql_set['user_pw'] = md5($_p_value);
			}
			else {
				$sql_set['user_paypw'] = md5($_p_value);	
			}
			if ($db->pe_update('user', array('user_id'=>$user_id), $sql_set)) {
				pe_apidata(array('code'=>1, 'msg'=>'修改成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'修改失败'));
			}
		}	
		$seo = pe_seo($menutitle='修改密码', '', '', 'admin');
		include(pe_tpl('user_setpw.html'));
	break;
	//####################// 变更推荐人 //####################//
	case 'tguser':
		$user_id = intval($_g_id);
		$info = $db->pe_select('user', array('user_id'=>$user_id));
		if (isset($_p_pesubmit)) {
			pe_token_match();
			if ($_p_keyword) {
				if (!$_p_tguser_id) pe_apidata(array('code'=>0, 'msg'=>'请先验证用户'));
				$tguser = $db->pe_select('user', array('user_id'=>$_p_tguser_id));			
				if (!$tguser['user_id']) pe_apidata(array('code'=>0, 'msg'=>'用户不存在'));
				if ($tguser['user_id'] == $user_id) pe_apidata(array('code'=>0, 'msg'=>'推荐人不能为本人'));			
				if ($tguser['tguser_id'] == $user_id) pe_apidata(array('code'=>0, 'msg'=>'不能互相推荐'));
			}
			else {
				$tguser['user_id'] = 0;
				$tguser['user_name'] = '';
			}
			$sql_set['tguser_id'] = $tguser['user_id'];
			$sql_set['tguser_name'] = $tguser['user_name'];			
			if ($db->pe_update('user', array('user_id'=>$user_id), pe_dbhold($sql_set))) {
				userinfo_callback($info['tguser_id']);
				userinfo_callback($tguser['user_id']);
				pe_apidata(array('code'=>1, 'msg'=>'修改成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'修改失败'));
			}
		}
		$seo = pe_seo($menutitle='变更推荐人', '', '', 'admin');
		include(pe_tpl('user_tguser.html'));
	break;
	//####################// 邀请结构 //####################//
	case 'tglist':
	case 'tglist_json':
		$user_id = intval($_g_id);
		if ($act == 'tglist_json') {
			$level = strlen($_g_level) ? (intval($_g_level) + 2) : 1;
			$user_pid = $_g_pid ? intval($_g_pid) : $user_id;
			$user_list = $db->index('user_id')->pe_selectall('user', array('tguser_id'=>$user_pid, 'order by'=>'user_id asc'));	
			$user_idarr = array_keys($user_list);
			if (count($user_idarr)) {
				$tguser_list = $db->index('tguser_id')->pe_selectall('user', array('tguser_id'=>$user_idarr));		
			}
			else {
				$tguser_list = array();
			}
			foreach ($user_list as $v) {
				$isparent = $tguser_list[$v['user_id']]['user_id'] ? true : false; 
				$user_arr[] = array('id'=>$v['user_id'], 'name'=>"{$level}级好友 - {$v['user_name']}", 'isParent'=>$isparent);
			}
			pe_apidata($user_arr);
		}
		/*$user_arr = array();
		$user_list = user_tglist($user_id);
		foreach ($user_list as $v) {
			$user_arr[] = array('id'=>$v['user_id'], 'pId'=>0, 'name'=>$v['user_name']);
			$user_list2 = user_tglist($v['user_id']);
			foreach ($user_list2 as $vv) {
				$user_arr[] = array('id'=>$vv['user_id'], 'pId'=>$vv['tguser_id'], 'name'=>$vv['user_name']);
				$user_list3 = user_tglist($vv['user_id']);
				foreach ($user_list3 as $vvv) {
					$user_arr[] = array('id'=>$vvv['user_id'], 'pId'=>$vvv['tguser_id'], 'name'=>$vvv['user_name']);
				}
			}
		}
		$json = json_encode($user_arr);*/
		$seo = pe_seo($menutitle='邀请结构', '', '', 'admin');
		include(pe_tpl('user_tglist.html'));
	break;
	//####################// 选择用户 //####################//
	case 'select':
		$_g_name && $sql_where .= " and `user_name` like '%{$_g_name}%'";
		$_g_phone && $sql_where .= " and `user_phone` like '%{$_g_phone}%'";
		$sql_where .= " order by `user_id` desc";
		$info_list = $db->pe_selectall('user', $sql_where, '*', array(100, $_g_page));
		$seo = pe_seo($menutitle='选择用户', '', '', 'admin');
		include(pe_tpl('user_select.html'));
	break;
	//####################// 发送短信 //####################//
	case 'send_sms':
		if (isset($_p_pesubmit)) {
			pe_token_match();
			if (!$_p_user_name) pe_apidata(array('code'=>0, 'msg'=>'请选择收件人'));
			if (!$_p_sms_text) pe_apidata(array('code'=>0, 'msg'=>'请填写短信内容'));
			$info_list = $db->pe_selectall('user', array('user_name'=>explode(',', pe_dbhold($_p_user_name))));
			foreach ($info_list as $k=>$v) {
				if (!$v['user_phone']) continue;
				$noticelog_list[$k]['noticelog_user'] = pe_dbhold($v['user_phone']);
				$noticelog_list[$k]['noticelog_name'] = '';
				$noticelog_list[$k]['noticelog_text'] = $_p_sms_text;
				$noticelog_list[$k]['noticelog_atime'] = time();			
			}
			if (!is_array($noticelog_list)) pe_apidata(array('code'=>0, 'msg'=>'没有有效的收件人'));
			if ($db->pe_insert('noticelog', $noticelog_list)) {
				pe_apidata(array('code'=>1, 'msg'=>'提交成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'提交失败'));
			}
		}
		$seo = pe_seo($menutitle='发送短信', '', '', 'admin');
		include(pe_tpl('user_send_sms.html'));
	break;	
	//####################// 发送邮件 //####################//
	case 'email':
		if (isset($_p_pesubmit)) {
			pe_token_match();
			!$_p_email_user && pe_error('收件人必须填写...');
			!$_p_email_name && pe_error('邮件标题必须填写...');
			!$_p_email_text && pe_error('邮件内容必须填写...');
			$email_user = explode(',', $_p_email_user);
			foreach ($email_user as $k=>$v) {
				if (!$v) continue;
				$noticelog_list[$k]['noticelog_user'] = pe_dbhold($v);
				$noticelog_list[$k]['noticelog_name'] = pe_dbhold($_p_email_name);
				$noticelog_list[$k]['noticelog_text'] = $_p_email_text;
				$noticelog_list[$k]['noticelog_atime'] = time();			
			}
			if ($db->pe_insert('noticelog', $noticelog_list)) {
				pe_success('发送成功!', '', 'dialog');
			}
			else {
				pe_error('发送失败...');
			}
		}
		$info_list = $db->pe_selectall('user', array('user_id'=>explode(',', $_g_id)));
		$email_user = array();
		foreach ($info_list as $v) {
			$v['user_email'] && $email_user[] = $v['user_email'];
		}
		$seo = pe_seo($menutitle='发送邮件', '', '', 'admin');
		include(pe_tpl('user_email.html'));
	break;
	//####################// 会员登录 //####################//
	case 'login':
		pe_token_match();
		$info = $db->pe_select('user', array('user_id'=>intval($_g_id)));
		user_login_callback('login', $info);
		pe_apidata(array('code'=>1, 'msg'=>'登录成功'));
	break;
	//####################// 获取信息 //####################//
	case 'getinfo':
		if (!$_g_keyword) pe_apidata(array('code'=>0, 'msg'=>'请填写用户信息'));		
		$sql_where = " and (`user_name` = '{$_g_keyword}' or `user_phone` = '{$_g_keyword}')";
		$info = $db->pe_select('user', $sql_where);
		if ($info['user_id']) {
			pe_apidata(array('code'=>1, 'msg'=>'验证通过', 'data'=>$info));
		}
		else {
			pe_apidata(array('code'=>0, 'msg'=>'用户不存在'));	
		}
	break;
	//####################// 会员状态 //####################//
	case 'state':
		pe_token_match();
		$user_id = intval($_g_id);
		if ($db->pe_update('user', array('user_id'=>$user_id), array('user_state'=>intval($_g_value)))) {
			pe_apidata(array('code'=>1, 'msg'=>'操作成功'));
		}
		else {
			pe_apidata(array('code'=>0, 'msg'=>'操作失败'));
		}
	break;
	//####################// 会员列表 //####################//
	default:
		$_g_id && $sql_where .= " and `user_id` = ".intval($_g_id);
		$_g_name && $sql_where .= " and `user_name` like '%".pe_dbhold($_g_name)."%'";
		$_g_tname && $sql_where .= " and `user_tname` = '".pe_dbhold($_g_tname)."'";
		$_g_tguser_name && $sql_where .= " and `tguser_name` = '".pe_dbhold($_g_tguser_name)."'";
		$_g_phone && $sql_where .= " and `user_phone` like '%".pe_dbhold($_g_phone)."%'";
		$_g_email && $sql_where .= " and `user_email` like '%".pe_dbhold($_g_email)."%'";
		$_g_type && $sql_where .= " and find_in_set('".pe_dbhold($_g_type)."', `user_type`)";
		$_g_userlevel_id && $sql_where .= " and `userlevel_id` = '{$_g_userlevel_id}'";
		if (in_array($_g_orderby, array('ltime|desc', 'money|desc', 'point|desc', 'ordernum|desc'))) {
			$orderby = explode('|', $_g_orderby);
			$sql_where .= " order by `user_{$orderby[0]}` {$orderby[1]}";
		}
		else {
			$sql_where .= " order by `user_id` desc";
		}
		$info_list = $db->pe_selectall('user', $sql_where, '*', array(100, $_g_page));

		$tongji['all'] = $db->pe_num('user');
		$tongji_arr = $db->index('userlevel_id')->pe_selectall('user', array('group by'=>'userlevel_id'), 'userlevel_id, count(1) as `num`');
		foreach ($cache_userlevel as $k=>$v) {
			$tongji[$k] = intval($tongji_arr[$k]['num']);
		}
		$seo = pe_seo($menutitle='会员列表', '', '', 'admin');
		include(pe_tpl('user_list.html'));		
	break;
}

//获取上三级级用户
function tguser_getlist($user, $level=1, $tguser_list = array()) {
	global $user_arr;
	if ($level > 3) return $tguser_list;
	$tguser = $user_arr[$user['tguser_id']];
	if ($tguser['user_id']) {
		$tguser_list[$level] = $tguser;
		return tguser_getlist($tguser, $level+1, $tguser_list);
	}
	else {
		return $tguser_list;
	}
}
?>