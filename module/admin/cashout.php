<?php
$menumark = 'cashout';
switch ($act) {
	//####################// 提现开关 //####################//
	case 'open':
		if ($db->pe_update('setting', array('setting_key'=>'cashout_isopen'), array('setting_value'=>intval($_g_open)))) {
			pe_lead('hook/cache.hook.php');
			cache_write('setting');
			pe_apidata(array('code'=>1, 'msg'=>'操作成功'));
		}
		else {
			pe_apidata(array('code'=>0, 'msg'=>'操作失败'));
		}
	break;
	//####################// 审核通过 //####################//
	case 'success':
		$cashout_id = is_array($_p_cashout_id) ? $_p_cashout_id : intval($_g_id);
		$cashout_list = $db->pe_selectall('cashout', array('cashout_id'=>$cashout_id));
		foreach ($cashout_list as $v) {
			$info = $db->pe_select('cashout', array('cashout_id'=>$v['cashout_id']));
			if ($info['cashout_state']) continue;
			if ($info['cashout_banktype'] == 'wechat') {
				pe_lead('hook/wechat.hook.php');
				pe_apidata(wechat_transfer($info['cashout_id']));
			}
			else {
				$db->pe_update('cashout', array('cashout_id'=>$info['cashout_id']), array('cashout_state'=>1, 'cashout_ptime'=>time()));
			}
		}
		pe_apidata(array('code'=>1, 'msg'=>'操作成功'));
	break;
	//####################// 审核取消 //####################//
	case 'refuse':
		$cashout_id = is_array($_p_cashout_id) ? $_p_cashout_id : intval($_g_id);
		$closetext = $_g_closetext;
		if (!$closetext) pe_apidata(array('code'=>0, 'msg'=>'请填写取消原因'));
		$cashout_list = $db->pe_selectall('cashout', array('cashout_id'=>$cashout_id));
		foreach ($cashout_list as $v) {
			$info = $db->pe_select('cashout', array('cashout_id'=>$v['cashout_id']));
			if ($info['cashout_state']) continue;
			$db->pe_update('cashout', array('cashout_id'=>$info['cashout_id']), array('cashout_state'=>2, 'cashout_closetext'=>$closetext, 'cashout_ptime'=>time()));
			add_moneylog($info['user_id'], 'back', $info['cashout_money'], "提现未通过，退回{$info['cashout_money']}元，原因：{$closetext}");
		}
		pe_apidata(array('code'=>1, 'msg'=>'操作成功'));
	break;
	//####################// 提现列表 //####################//
	default:
		$sql_where = " and `cashout_state` = ".intval($_g_state);
		$_g_name && $sql_where .= " and `user_name` = '{$_g_name}'";
		$_g_banknum && $sql_where .= " and `cashout_banknum` = '{$_g_banknum}'";
		$_g_banktname && $sql_where .= " and `cashout_banktname` = '{$_g_banktname}'";
		$_g_bankname && $sql_where .= " and `cashout_bankname` = '{$_g_bankname}'";
		$_g_user_type && $sql_where .= " and `user_type` = '{$_g_user_type}'";
		$_g_date1 && $sql_where .= " and `cashout_ptime` >= '".strtotime($_g_date1)."'";
		$_g_date2 && $sql_where .= " and `cashout_ptime` < '".(strtotime($_g_date2) + 86400)."'";
		if ($_g_state) {
			$sql_where .= " order by cashout_ptime desc, cashout_id desc";		
		}
		else {		
			$sql_where .= " order by cashout_id desc";
		}	
		$tongji_arr = $db->pe_select('cashout', $sql_where, "count(1) as `num`, sum(cashout_jsmoney) as `money`");
		$tongji['allnum'] = intval($tongji_arr['num']);
		$tongji['allmoney'] = pe_num($tongji_arr['money'], 'round', 2);
		if ($act == 'export') {
			$info_list = $db->pe_selectall('cashout', $sql_where);
			pe_lead('include/class/excel_out.class.php');
			include("{$pe['path']}include/plugin/phpexcel/PHPExcel.php");
			$xls_data[] = array('申请日期', '用户帐号', '结款金额', '银行名称', '开户行', '银行卡号', '收款人姓名', '审核日期');
			foreach($info_list as $k=>$v) {
				$cashout_atime = pe_date($v['cashout_atime'], 'Y/m/d H:i:s');
				$cashout_ptime = $v['cashout_ptime'] ? pe_date($v['cashout_ptime'], 'Y/m/d H:i:s') : '待审核';					
				$xls_data[] = array($cashout_atime, $v['user_name'], $v['cashout_jsmoney'], $v['cashout_bankname'], $v['cashout_bankaddress'], $v['cashout_banknum'], $v['cashout_banktname'], $cashout_ptime);
				$allnum++;
				$allmoney += $v['cashout_jsmoney']; 
			}
			$xls_data[] = array('', '', "总计：{$tongji['allmoney']}元", "共：{$tongji['allnum']}笔", '', '', '', '', '');
			$xls_name = date('d')."号提现导出";
			$xls = new excel('UTF-8', false, $xls_name);  
			$xls->addArray($xls_data);  
			$xls->generateXML($xls_name);
		}
		else {
			$info_list = $db->pe_selectall('cashout', $sql_where, '*', array(50, $_g_page));

			$tongji_arr = $db->index('cashout_state')->pe_selectall('cashout', array('group by'=>'cashout_state'), 'count(1) as num, `cashout_state`');
			$tongji[0] = intval($tongji_arr[0]['num']);
			$tongji[1] = intval($tongji_arr[1]['num']);
			$tongji[2] = intval($tongji_arr[2]['num']);
			$seo = pe_seo($menutitle='提现管理');
			include(pe_tpl('cashout_list.html'));
		}
	break;
}
?>