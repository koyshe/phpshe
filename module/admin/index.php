<?php
/**
 * @copyright   2008-2014 简好网络 <http://www.phpshe.com>
 * @creatdate   2012-0501 koyshe <koyshe@gmail.com>
 */
$menumark = 'index';
switch ($act) {
	//####################// phpinfo信息 //####################//
	case 'phpinfo':
		phpinfo();
	break;
	//####################// 修改管理资料 //####################//
	case 'setadmin':
		if (isset($_p_pesubmit)) {
			pe_token_match();
			if (!$_p_info['admin_name']) pe_apidata(array('code'=>0, 'msg'=>'请填写账号'));
			if ($db->pe_update('admin', array('admin_id'=>$admin['admin_id']), pe_dbhold($_p_info))) {
				pe_apidata(array('code'=>1, 'msg'=>'修改成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'修改失败'));
			}
		}
		$info = $admin;
		$seo = pe_seo($menutitle='修改管理资料', '', '', 'admin');
		include(pe_tpl('index_setadmin.html'));
	break;
	//####################// 修改管理密码 //####################//
	case 'setpw':
		if (isset($_p_pesubmit)) {
			pe_token_match();
			if (!$_p_admin_pw) pe_apidata(array('code'=>0, 'msg'=>'请填写密码'));
			$sql_set['admin_pw'] = md5($_p_admin_pw);
			if ($db->pe_update('admin', array('admin_id'=>$admin['admin_id']), $sql_set)) {
				pe_apidata(array('code'=>1, 'msg'=>'修改成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'修改失败'));
			}
		}
		$info = $admin;
		$seo = pe_seo($menutitle='修改管理密码', '', '', 'admin');
		include(pe_tpl('index_setpw.html'));
	break;
	//####################// 后台首页 //####################//
	default:
		$time1 = strtotime(date('Y-m-d'));
		$time2 = $time1 - 86400;
		
		//待处理项
		$tongji['order_wpay'] = $db->pe_num('order', array('order_state'=>'wpay'));
		$tongji['order_wsend'] = $db->pe_num('order', array('order_state'=>'wsend'));
		//$tongji['order_wget'] = $db->pe_num('order', array('order_state'=>'wget'));
		$tongji['refund_wcheck'] = $db->pe_num('refund', array('refund_state'=>'wcheck'));
		$tongji['cashout_js'] = $db->pe_num('cashout', array('cashout_state'=>0));
		
		//数据统计
		$tongji['product_num'] = $db->pe_num('product');
		$tongji['category_num'] = $db->pe_num('category');
		$tongji['brand_num'] = $db->pe_num('brand');
		$tongji['comment_num'] = $db->pe_num('comment');	
		$tongji['order_num'] = $db->pe_num('order'); 
		$tongji['refund_num'] = $db->pe_num('refund'); 
		$tongji['iplog_num'] = $db->pe_num('iplog');
		$tongji['user_num'] = $db->pe_num('user'); 
		$tongji_arr = $db->pe_select('user', '', 'sum(user_money) as `money`, sum(user_point) as `point`'); 
		$tongji['user_money'] = $tongji_arr['money'];
		$tongji['user_point'] = $tongji_arr['point'];
		$tongji['pay_num'] = $db->pe_num('pay', array('order_state'=>'success'));
		$tongji['cashout_num'] = $db->pe_num('cashout');

		//交易统计
		$tongji['pay_today'] = $db->pe_num('pay', " and `order_state` = 'success' and `order_ptime` >= '{$time1}'");
		$tongji['pay_lastday'] = $db->pe_num('pay', " and `order_state` = 'success' and `order_ptime` >= '{$time2}' and `order_ptime` < '{$time1}'");
		$tongji['order_today'] = $db->pe_num('order', " and `order_atime` >= '{$time1}'");
		$tongji['order_lastday'] = $db->pe_num('order', " and `order_atime` >= '{$time2}' and `order_atime` < '{$time1}'");
		$tongji['refund_today'] = $db->pe_num('refund', " and `refund_atime` >= '{$time1}'");
		$tongji['refund_lastday'] = $db->pe_num('refund', " and `refund_atime` >= '{$time2}' and `refund_atime` < '{$time1}'");
		//流量统计
		$tongji['iplog_today'] = $db->pe_num('iplog', " and `iplog_atime` >= '{$time1}'");
		$tongji['iplog_lastday'] = $db->pe_num('iplog', " and `iplog_atime` >= '{$time2}' and `iplog_atime` < '{$time1}'");
		$tongji['user_today'] = $db->pe_num('user', " and `user_atime` >= '{$time1}'");
		$tongji['user_lastday'] = $db->pe_num('user', " and `user_atime` >= '{$time2}' and `user_atime` < '{$time1}'");
		$tongji['sign_today'] = $db->pe_num('signlog', " and `signlog_atime` >= '{$time1}'");
		$tongji['sign_lastday'] = $db->pe_num('signlog', " and `signlog_atime` >= '{$time2}' and `signlog_atime` < '{$time1}'");

		//报表数据
		$date1 = date('Y-m-d', strtotime("-10 day"));
		$date2 = date('Y-m-d');
		$time1 = strtotime($date1);
		$time2 = strtotime($date2);
		//收款统计数据
		$pay_list = $db->index('pay_date')->pe_selectall('pay', " and order_state = 'success' group by `pay_date` having `pay_date` >= '{$date1}' and `pay_date` <= '{$date2}'", "from_unixtime(order_ptime, '%Y-%m-%d') as pay_date, sum(order_money) as `money`");
		for ($i=$time1; $i<=$time2; $i=$i+86400) {
			$pay_x[] = pe_date($i, 'm-d');
			$pay_y[] = pe_num($pay_list[pe_date($i, 'Y-m-d')]['money'], 'floor', 2);
		}	
		//流量统计数据
		$iplog_list = $db->index('iplog_adate')->pe_selectall('iplog', " and `iplog_adate` >= '{$date1}' and `iplog_adate` <= '{$date2}' group by `iplog_adate`", "iplog_adate, sum(1) as `num`");
		for ($i=$time1; $i<=$time2; $i=$i+86400) {
			$iplog_x[] = pe_date($i, 'm-d');
			$iplog_y[] = intval($iplog_list[pe_date($i, 'Y-m-d')]['num']);
		}
		
		//系统环境信息
		$php_os = PHP_OS;
		$php_version = 'PHP'.PHP_VERSION;
		$mysql_version = pe_cut($db->version(), 13);
		if (stripos($_SERVER["SERVER_SOFTWARE"], 'iis') !== false) {
			$iis_arr = explode('/', $_SERVER["SERVER_SOFTWARE"]);
			$php_apache = "IIS/{$iis_arr[1]}";
		}
		else {
			$apache_arr = explode(' ', $_SERVER["SERVER_SOFTWARE"]);
			$php_apache = $apache_arr[0];	
		}
		$seo = pe_seo($menutitle='网站概况', '', '', 'admin');
		include(pe_tpl('index.html'));
	break;
}
?>