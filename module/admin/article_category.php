<?php
/**
 * @copyright   2008-2021 简好网络 <http://www.phpshe.com>
 * @creatdate   2012-0501 koyshe <koyshe@gmail.com>
 */
$menumark = 'article_category';
pe_lead('hook/cache.hook.php');
switch ($act) {
	//####################// 分类添加 //####################//
	case 'add':
		$category_id = intval($_g_id);
		$info['category_type'] = $_g_type;
		if (isset($_p_pesubmit)) {
			pe_token_match();
			$_p_info['category_type'] = $info['category_type'];
			if ($db->pe_insert('article_category', pe_dbhold($_p_info))) {
				cache_write('article_category');
				pe_apidata(array('code'=>1, 'msg'=>'添加成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'添加失败'));
			}
		}
		$seo = pe_seo($menutitle='添加分类', '', '', 'admin');
		include(pe_tpl('article_category_add.html'));
	break;
	//####################// 分类修改 //####################//
	case 'edit':
		$category_id = intval($_g_id);
		if (isset($_p_pesubmit)) {
			pe_token_match();
			if ($db->pe_update('article_category', array('category_id'=>$category_id), pe_dbhold($_p_info))) {
				cache_write('article_category');
				pe_apidata(array('code'=>1, 'msg'=>'修改成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'修改失败'));
			}
		}
		$info = $db->pe_select('article_category', array('category_id'=>$category_id));
		$seo = pe_seo($menutitle='修改分类', '', '', 'admin');
		include(pe_tpl('article_category_add.html'));
	break;
	//####################// 分类删除 //####################//
	case 'del':
		pe_token_match();
		$category_id = intval($_g_id);
		$category_id == 1 && pe_apidata(array('code'=>0, 'msg'=>'默认分类不能删除'));
		if ($db->pe_delete('article_category', array('category_id'=>$category_id))) {
			cache_write('article_category');
			pe_apidata(array('code'=>1, 'msg'=>'删除成功'));
		}
		else {
			pe_apidata(array('code'=>0, 'msg'=>'删除失败'));
		}
	break;
	//####################// 分类排序 //####################//
	case 'order':
		pe_token_match();
		foreach ($_p_category_order as $k=>$v) {
			$result = $db->pe_update('article_category', array('category_id'=>$k), array('category_order'=>$v));
		}
		if ($result) {
			cache_write('article_category');
			pe_apidata(array('code'=>1, 'msg'=>'排序成功'));
		}
		else {
			pe_apidata(array('code'=>0, 'msg'=>'排序失败'));
		}
	break;
	//####################// 分类列表 //####################//
	default:
		$info_list = $db->pe_selectall('article_category', array('category_type'=>$_g_type, 'order by'=>'`category_order` asc, `category_id` asc'));
		$seo = pe_seo($menutitle='文章分类', '', '', 'admin');
		include(pe_tpl('article_category_list.html'));
	break;
}
?>