<?php
/**
 * @copyright   2008-2014 简好网络 <http://www.phpshe.com>
 * @creatdate   2012-0501 koyshe <koyshe@gmail.com>
 */
$menumark = 'adminlevel';
switch ($act) {
	//####################// 管理权限添加 //####################//
	case 'add':
		if (isset($_p_pesubmit)) {
			pe_token_match();	
			if (is_array($_p_modact)) {
				foreach ($_p_modact as $v) {
					$modact_arr[] = $v;
					if ($adminmenu_menumark_list[$v]) $menumark_arr[] = $adminmenu_menumark_list[$v];
				}
			}
			$_p_info['adminlevel_modact'] = is_array($modact_arr) ? serialize($modact_arr) : '';			
			$_p_info['adminlevel_menumark'] = is_array($menumark_arr) ? serialize($menumark_arr) : '';			
			if ($db->pe_insert('adminlevel', $_p_info)) {
				cache::write('adminlevel');
				pe_apidata(array('code'=>1, 'msg'=>'添加成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'添加失败'));
			}
		}
		$info['adminlevel_modact'] = array();
		$seo = pe_seo($menutitle='添加权限', '', '', 'admin');
		include(pe_tpl('adminlevel_add.html'));
	break;
	//####################// 管理权限修改 //####################//
	case 'edit':
		$adminlevel_id = intval($_g_id);
		$_g_id == 1 && pe_apidata(array('code'=>0, 'msg'=>'总管理权限不能修改'));
		if (isset($_p_pesubmit)) {
			pe_token_match();
			if (is_array($_p_modact)) {
				foreach ($_p_modact as $v) {
					$modact_arr[] = $v;
					if ($adminmenu_menumark_list[$v]) $menumark_arr[] = $adminmenu_menumark_list[$v];
				}
			}
			$_p_info['adminlevel_modact'] = is_array($modact_arr) ? serialize($modact_arr) : '';			
			$_p_info['adminlevel_menumark'] = is_array($menumark_arr) ? serialize($menumark_arr) : '';
			if ($db->pe_update('adminlevel', array('adminlevel_id'=>$adminlevel_id), $_p_info)) {
				cache::write('adminlevel');
				pe_apidata(array('code'=>1, 'msg'=>'修改成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'修改失败'));
			}
		}
		$info = $db->pe_select('adminlevel', array('adminlevel_id'=>$adminlevel_id));
		$info['adminlevel_modact'] = $info['adminlevel_modact'] ? unserialize($info['adminlevel_modact']) : array();
		$seo = pe_seo($menutitle='修改权限', '', '', 'admin');
		include(pe_tpl('adminlevel_add.html'));
	break;
	//####################// 管理权限删除 //####################//
	case 'del':
		pe_token_match();
		$adminlevel_id = intval($_g_id);
		$adminlevel_id == 1 && pe_apidata(array('code'=>0, 'msg'=>'总管理权限不能删除'));
		if ($db->pe_delete('adminlevel', array('adminlevel_id'=>$adminlevel_id))) {
			cache::write('adminlevel');
			pe_apidata(array('code'=>1, 'msg'=>'删除成功'));
		}
		else {
			pe_apidata(array('code'=>0, 'msg'=>'删除失败'));
		}
	break;
	//####################// 管理权限列表 //####################//
	default:
		$info_list = $db->pe_selectall('adminlevel', array('order by'=>'adminlevel_id asc'), '*', array(100, $_g_page));
		$tongji['all'] = $db->pe_num('adminlevel');
		$seo = pe_seo($menutitle='管理权限', '', '', 'admin');
		include(pe_tpl('adminlevel_list.html'));
	break;
}
?>