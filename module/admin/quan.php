<?php
/**
 * @copyright   2008-2014 简好网络 <http://www.phpshe.com>
 * @creatdate   2012-1116 koyshe <koyshe@gmail.com>
 */
$menumark = 'quan';
$cache_category = cache::get('category');
switch ($act) {
	//####################// 优惠券添加 //####################//
	case 'add':
		if (isset($_p_pesubmit)) {
			pe_token_match();
			$_p_info['quan_atime'] = time();
			if ($_p_info['quan_datetype'] == 'relative') {
				$_p_info['quan_sdate'] = '1970-01-01';
				$_p_info['quan_edate'] = '1970-01-01';
			}
			else {
				$_p_info['quan_day'] = 0;			
			}
			$_p_info['product_id'] = is_array($_p_product_id) ? implode(',', $_p_product_id) : '';
			if ($quan_id = $db->pe_insert('quan', pe_dbhold($_p_info))) {
				quan_callback($quan_id);
				pe_apidata(array('code'=>1, 'msg'=>'添加成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'添加失败'));
			}
		}
		$product_list = array();
		$info['quan_type'] = 'online';
		$info['quan_sdate'] = date('Y-m-d');
		$info['quan_edate'] = date('Y-m-d', strtotime('+1 month'));		
		$seo = pe_seo($menutitle='添加优惠券', '', '', 'admin');
		include(pe_tpl('quan_add.html'));
	break;
	//####################// 优惠券修改(优惠券) //####################//
	case 'edit':
		$quan_id = intval($_g_id);
		$info = $db->pe_select('quan', array('quan_id'=>$quan_id));
		if (isset($_p_pesubmit)) {
			pe_token_match();
			if ($_p_info['quan_num'] < $info['quan_num_get']) pe_apidata(array('code'=>0, 'msg'=>'发放量应大于已领取数'));
			$_p_info['product_id'] = is_array($_p_product_id) ? implode(',', $_p_product_id) : '';
			if ($db->pe_update('quan', array('quan_id'=>$quan_id), pe_dbhold($_p_info))) {
				quan_callback($quan_id);
				/*$sql_set['quan_name'] = $_p_info['quan_name'];
				//$sql_set['quan_limit'] = $_p_info['quan_limit'];	
				if ($info['quan_datetype'] == 'absolute') {
					$sql_set['quan_sdate'] = $_p_info['quan_sdate'];	
					$sql_set['quan_edate'] = $_p_info['quan_edate'];				
				}*/
				$sql_set['product_id'] = $_p_info['product_id'];	
				$db->pe_update('quanlog', array('quan_id'=>$quan_id), pe_dbhold($sql_set));
				pe_apidata(array('code'=>1, 'msg'=>'修改成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'修改失败'));
			}
		}
		$product_ids = $info['product_id'] ? explode(',', $info['product_id']) : array();
		$product_list = $db->pe_selectall('product', array('product_id'=>$product_ids));
		$disabled = 'disabled="disabled"';
		$seo = pe_seo($menutitle='修改优惠券', '', '', 'admin');
		include(pe_tpl('quan_add.html'));
	break;
	//####################// 优惠券删除 //####################//
	case 'del':
		pe_token_match();
		$quan_id = intval($_g_id);
		if ($db->pe_delete('quan', array('quan_id'=>$quan_id))) {
			$db->pe_delete('quanlog', array('quan_id'=>$quan_id, 'user_id'=>0));
			pe_apidata(array('code'=>1, 'msg'=>'删除成功'));
		}
		else {
			pe_apidata(array('code'=>0, 'msg'=>'删除失败'));
		}
	break;
	//####################// 优惠券结束 //####################//
	case 'cancel':
		pe_token_match();
		$quan_id = intval($_g_id);
		if ($db->pe_update('quan', array('quan_id'=>$quan_id), array('quan_state'=>0))) {
			pe_apidata(array('code'=>1, 'msg'=>'操作成功'));
		}
		else {
			pe_apidata(array('code'=>0, 'msg'=>'操作失败'));
		}
	break;
	//####################// 选择商品 //####################//
	case 'product_list':
		pe_lead('hook/category.hook.php');
		$category_treelist = category_treelist();
		$cache_brand = cache::get('brand');
		$_g_name && $sql_where .= " and `product_name` like '%".pe_dbhold($_g_name)."%'";
		$_g_category_id && $sql_where .= category_get_product($_g_category_id);
		$_g_brand_id && $sql_where .= " and `brand_id` = '".intval($_g_brand_id)."'";
		$sql_where .= " order by `product_id` desc";
		$info_list = $db->pe_selectall('product', $sql_where, '*', array(50, $_g_page));

		$seo = pe_seo($menutitle='选择商品', '', '', 'admin');
		include(pe_tpl('quan_product_list.html'));
	break;
	//####################// 添加商品 //####################//
	case 'product_add':
		$info = $db->pe_select('product', array('product_id'=>$_g_id));
		$product_logo = pe_thumb($info['product_logo'], 100, 100);
		$product_url = pe_url('product-'.$info['product_id']);
$html = <<<html
	<tr class="js_product" id="{$info['product_id']}">
		<td>{$info['product_id']}<input type="hidden" name="product_id[]" value="{$info['product_id']}" /></td>
		<td><img src="{$product_logo}" width="40" height="40" class="imgbg" /></td>
		<td class="left"><a href="{$product_url}" target="_blank" class="cblue">{$info['product_name']}</a></td>
		<td><span class="corg">{$info['product_money']}</span></td>
		<td><a href="javascript:;" class="admin_btn">删除</a></td>
	</tr>
html;
		echo json_encode(array('html'=>$html));
	break;
	//####################// 使用记录 //####################//
	case 'mylog':
		user_quancheck();
		$quan_id = intval($_g_id);	
		$info = $db->pe_select('quan', array('quan_id'=>$quan_id));
		$info_list = $db->pe_selectall('quanlog', array('quan_id'=>$quan_id, 'order by'=>'quanlog_id desc'), '*', array(100, $_g_page));
		$seo = pe_seo($menutitle='使用记录', '', '', 'admin');
		include(pe_tpl('quan_mylog.html'));
	break;
	//####################// 优惠券列表 //####################//
	default:
		user_quancheck();
		strlen($_g_state) && $sql_where['quan_state'] = intval($_g_state);
		$sql_where['order by'] = 'quan_id desc';
		$info_list = $db->pe_selectall('quan', $sql_where, '*', array(100, $_g_page));

		$tongji_arr = $db->index('quan_state')->pe_selectall('quan', array('group by'=>'quan_state'), 'quan_state, count(1) as `num`');
		foreach ($ini['quan_state'] as $k=>$v) {
		    $tongji[$k] = intval($tongji_arr[$k]['num']);
		    $tongji['all'] += $tongji[$k];
        }
		$seo = pe_seo($menutitle='优惠券列表', '', '', 'admin');
		include(pe_tpl('quan_list.html'));
	break;
}

function quan_callback($quan_id) {
	global $db;
	if (is_array($_POST['product_id'])) {
		$info = $db->pe_select('product', array('product_id'=>current($_POST['product_id'])), 'product_logo');
	}
	else {
		$info = $db->pe_select('product', array('order by'=>'product_sellnum desc'), 'product_logo');
	}
	$db->pe_update('quan', array('quan_id'=>$quan_id), array('product_logo'=>$info['product_logo']));
}
?>