<?php
/**
 * @copyright   2008-2021 简好网络 <http://www.phpshe.com>
 * @creatdate   2012-0501 koyshe <koyshe@gmail.com>
 */
$menumark = 'pay';
switch ($act) {
	//####################// 收款记录 //####################//
	default:
		$_g_state && $sql_where .= " and `order_state` = '{$_g_state}'";
		$_g_id && $sql_where .= " and `pay_id` = '{$_g_id}'";
        $_g_order_id && $sql_where .= " and `order_id` = '{$_g_order_id}'";
        $_g_order_name && $sql_where .= " and `order_name` like '%{$_g_order_name}%'";
		$_g_user_name && $sql_where .= " and `user_name` like '%{$_g_user_name}%'";
		$sql_where .= ' order by `pay_id` desc';

		$info_list = $db->pe_selectall('pay', $sql_where, '*', array(100, $_g_page));
		$tongji_arr = $db->index('order_state')->pe_selectall('pay', array('group by'=>'order_state'), 'count(1) as `num`, `order_state`');
		foreach (array('wpay', 'success') as $v) {
			$tongji[$v] = intval($tongji_arr[$v]['num']);
		}
		$seo = pe_seo($menutitle='收款记录');
		include(pe_tpl('pay_list.html'));
	break;
}
?>