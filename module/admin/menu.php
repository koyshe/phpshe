<?php
/**
 * @copyright   2008-2021 简好网络 <http://www.phpshe.com>
 * @creatdate   2012-0501 koyshe <koyshe@gmail.com>
 */
$menumark = 'menu';
pe_lead('hook/cache.hook.php');
switch ($act) {
	//####################// 导航添加 //####################//
	case 'add':
		$menu_id = intval($_g_id);
		$info['menu_client'] = $_g_client;
		if (isset($_p_pesubmit)) {
			pe_token_match();
			$_p_info['menu_client'] = $info['menu_client'];
			$_p_info['menu_state'] = intval($_p_menu_state);
			if ($db->pe_insert('menu', pe_dbhold($_p_info))) {
				cache_write('menu');
				pe_apidata(array('code'=>1, 'msg'=>'添加成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'添加失败'));
			}
		}
		$info['menu_state'] = 1;
		$menu_urllist = menu_urllist();
		$seo = pe_seo($menutitle='添加导航', '', '', 'admin');
		include(pe_tpl('menu_add.html'));
	break;
	//####################// 导航修改 //####################//
	case 'edit':
		$menu_id = intval($_g_id);
		$info = $db->pe_select('menu', array('menu_id'=>$menu_id));
		if (isset($_p_pesubmit)) {
			pe_token_match();
			$_p_info['menu_state'] = intval($_p_menu_state);
			if ($db->pe_update('menu', array('menu_id'=>$menu_id), pe_dbhold($_p_info))) {
				cache_write('menu');
				pe_apidata(array('code'=>1, 'msg'=>'修改成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'修改失败'));
			}
		}
		$menu_urllist = menu_urllist();
		$seo = pe_seo($menutitle='修改导航', '', '', 'admin');
		include(pe_tpl('menu_add.html'));
	break;
	//####################// 导航删除 //####################//
	case 'del':
		pe_token_match();
		$menu_id = is_array($_p_menu_id) ? $_p_menu_id : intval($_g_id);
		if ($db->pe_delete('menu', array('menu_id'=>$menu_id))) {
			cache_write('menu');
			pe_apidata(array('code'=>1, 'msg'=>'删除成功'));
		}
		else {
			pe_apidata(array('code'=>0, 'msg'=>'删除失败'));
		}
	break;
	//####################// 导航排序 //####################//
	case 'order':
		pe_token_match();
		foreach ($_p_menu_order as $k=>$v) {
			$result = $db->pe_update('menu', array('menu_id'=>$k), array('menu_order'=>$v));
		}
		if ($result) {
			cache_write('menu');
			pe_apidata(array('code'=>1, 'msg'=>'排序成功'));
		}
		else {
			pe_apidata(array('code'=>0, 'msg'=>'排序失败'));
		}
	break;
	//####################// 导航状态 //####################//
	case 'state':
		pe_token_match();
		$menu_id = is_array($_p_menu_id) ? $_p_menu_id : intval($_g_id);
		if ($db->pe_update('menu', array('menu_id'=>$menu_id), array('menu_state'=>intval($_g_value)))) {
			cache_write('menu');
			pe_apidata(array('code'=>1));
		}
		else {
			pe_apidata(array('code'=>0, 'msg'=>'操作失败'));
		}
	break;
	//####################// 导航列表 //####################//
	default :
		!$_g_client && $_g_client = 'pc';
		$info_list = $db->pe_selectall('menu', array('menu_client'=>pe_dbhold($_g_client), 'order by'=>'`menu_order` asc, `menu_id` asc'));

		$tongji = $db->index('menu_client')->pe_selectall('menu', array('group by'=>'menu_client'), 'count(1) as num, menu_client');
		foreach ($ini['menu_client'] as $k=>$v){
			$tongji[$k] = intval($tongji[$k]['num']);
			$tongji['all'] += $tongji[$k]; 
		}
		$seo = pe_seo($menutitle='导航设置', '', '', 'admin');
		include(pe_tpl('menu_list.html'));
	break;
}
function menu_urllist() {
	global $pe;
	$arr['全部商品'] = array("url"=>pe_url("product-list"), 'minapp_url'=>'/page/index/product_list/product_list');	
	$arr['领券中心'] = array("url"=>pe_url("quan-list"), 'minapp_url'=>'/page/index/quan_list/quan_list');
	$arr['资讯中心'] = array("url"=>pe_url("article-news"), 'minapp_url'=>'/page/index/article_list/article_list');
	$arr['帮助中心'] = array("url"=>pe_url("article-help"), 'minapp_url'=>'/page/index/article_list/article_list');
	$arr['品牌专区'] = array("url"=>pe_url("brand-list"), 'minapp_url'=>'/page/index/brand_list/brand_list');
	$arr['每日秒杀'] = array("url"=>pe_url("huodong-miaosha"), 'minapp_url'=>'/page/index/huodong_miaosha/huodong_miaosha');
	$arr['限时拼团'] = array("url"=>pe_url("huodong-pintuan"), 'minapp_url'=>'/page/index/huodong_pintuan/huodong_pintuan');
	$arr['签到活动'] = array("url"=>"{$pe['host']}user.php?mod=sign", 'minapp_url'=>'/page/user/sign/sign');
	$arr['购物车'] = array("url"=>pe_url("cart"), 'minapp_url'=>'/page/index/cart/cart');
	$arr['晒单'] = array("url"=>pe_url("comment-share"), 'minapp_url'=>'/page/index/comment_share/comment_share');	
	return $arr;
}
?>