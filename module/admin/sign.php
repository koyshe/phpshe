<?php
/**
 * @copyright   2008-2021 简好网络 <http://www.phpshe.com>
 * @creatdate   2012-0501 koyshe <koyshe@gmail.com>
 */
$menumark = 'sign';
$tongji['all'] = $db->pe_num('signlog');
switch ($act) {
	//####################// 签到记录 //####################//
	case 'list':
		$_g_user_name && $sql_where['user_name'] = $_g_user_name;
		$sql_where['order by'] = 'signlog_id desc';
		$info_list = $db->pe_selectall('signlog', pe_dbhold($sql_where), '*', array(100, $_g_page));

		$seo = pe_seo($menutitle='签到记录', '', '', 'admin');
		include(pe_tpl('sign_list.html'));
	break;
	//####################// 签到设置 //####################//
	default :
		if (isset($_p_pesubmit)) {
			pe_token_match();
			if (is_array($_p_lx_day)) {
				foreach ($_p_lx_day as $k=>$v) {
					if (!$_p_lx_day[$k] or !$_p_lx_point[$k]) continue;
					//$lianxu_arr[$k]['day'] = $_p_lx_day[$k];
					//$lianxu_arr[$k]['point'] = $_p_lx_point[$k];
					$lianxu_arr[] = array('day'=>$_p_lx_day[$k], 'point'=>$_p_lx_point[$k]);
				}
			}
			if (is_array($_p_lj_day)) {
				foreach ($_p_lj_day as $k=>$v) {
					if (!$_p_lj_day[$k] or !$_p_lj_point[$k]) continue;
					//$leiji_arr[$k]['day'] = $_p_lj_day[$k];
					//$leiji_arr[$k]['point'] = $_p_lj_point[$k];
					$leiji_arr[] = array('day'=>$_p_lj_day[$k], 'point'=>$_p_lj_point[$k]);
				}
			}
			$sign_point_lianxu = is_array($lianxu_arr) ? serialize($lianxu_arr) : '';
			$sign_point_leiji = is_array($leiji_arr) ? serialize($leiji_arr) : '';
			$sql = "update `".dbpre."sign` set `value` = case `key`
				when 'sign_state' then '".intval($_p_sign_state)."'
				when 'sign_text' then '".pe_dbhold($_p_sign_text, 'all')."'
				when 'sign_point' then '".intval($_p_sign_point)."'
				when 'sign_point_shouci' then '".intval($_p_sign_point_shouci)."'
				when 'sign_point_lianxu' then '{$sign_point_lianxu}'
				when 'sign_point_leiji' then '{$sign_point_leiji}' else `value` end";
			if ($db->sql_update($sql)) {
				pe_apidata(array('code'=>1, 'msg'=>'设置成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'设置失败'));
			}
		}
		$info = $db->index('key')->pe_selectall('sign');
		$lianxu_list = $info['sign_point_lianxu']['value'] ? unserialize($info['sign_point_lianxu']['value']) : array();
		$leiji_list = $info['sign_point_leiji']['value'] ? unserialize($info['sign_point_leiji']['value']) : array();		
		
		$seo = pe_seo($menutitle='签到设置', '', '', 'admin');
		include(pe_tpl('sign_setting.html'));
	break;
}
?>