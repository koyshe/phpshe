<?php
/**
 * @copyright   2008-2021 简好网络 <http://www.phpshe.com>
 * @creatdate   2012-0501 koyshe <koyshe@gmail.com>
 */
$menumark = 'setting';
pe_lead('hook/cache.hook.php');
switch ($act) {
	//####################// 修改模板 //####################//
	case 'edit':
		$notice_id = intval($_g_id);
		if (isset($_p_pesubmit)) {
			pe_token_match();
			if ($db->pe_update('notice', array('notice_id'=>$notice_id), pe_dbhold($_p_info))) {
				cache_write('notice');
				pe_apidata(array('code'=>1, 'msg'=>'修改成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'修改失败'));
			}
		}
		$info = $db->pe_select('notice', array('notice_id'=>$notice_id));
		$seo = pe_seo($menutitle='修改模板', '', '', 'admin');
		include(pe_tpl('notice_add.html'));
	break;
	//####################// 开启状态 //####################//
	case 'state':
		pe_token_match();
		$notice_id = is_array($_p_notice_id) ? $_p_notice_id : $_g_id;
		if ($db->pe_update('notice', array('notice_id'=>$notice_id), array('notice_state'=>intval($_g_value)))) {
			cache_write('notice');
			pe_apidata(array('code'=>1));
		}
		else {
			pe_apidata(array('code'=>0, 'msg'=>'操作失败'));
		}
	break;
}
?>