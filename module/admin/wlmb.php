<?php
$menumark = 'wlmb';
$city_list = json_encode(city_list());
switch($act) {
	//####################// 添加模板 //####################//
	case 'add':
		if (isset($_p_pesubmit)) {
			pe_token_match();
			if (!$_p_wlmb_name) pe_apidata(array('code'=>0, 'msg'=>'请填写名称'));
			if (!$_p_wlmb_type) pe_apidata(array('code'=>0, 'msg'=>'请选择类型'));
			if (is_array($_p_city)) {
				foreach ($_p_city as $k=>$v) {
					if ($_p_city[$k] or $_p_value[$k] or $_p_money[$k]) {
						$num = $k + 1;
						if (!$_p_city[$k]) pe_apidata(array('code'=>0, 'msg'=>"请填写第{$num}个配送区域"));
						if (!$_p_value[$k]) pe_apidata(array('code'=>0, 'msg'=>"请填写第{$num}个首重/首件"));
						if (!$_p_money[$k]) pe_apidata(array('code'=>0, 'msg'=>"请填写第{$num}个运费"));	
					}
					$wlmb_data[] = pe_dbhold(array('city'=>$_p_city[$k],'value'=>$_p_value[$k],'money'=>$_p_money[$k],'value2'=>$_p_value2[$k],'money2'=>$_p_money2[$k]));
				}
			}
			if (!is_array($wlmb_data)) pe_apidata(array('code'=>0, 'msg'=>"请填写运费设置"));
			$sql_set['wlmb_name'] = $_p_wlmb_name;
			$sql_set['wlmb_type'] = $_p_wlmb_type;			
			$sql_set['wlmb_data'] = json_encode($wlmb_data);
			$sql_set['wlmb_atime'] = time();
			if ($db->pe_insert('wlmb', pe_dbhold($sql_set, array('wlmb_data')))) {
				pe_apidata(array('code'=>1, 'msg'=>'添加成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'添加失败'));
			}
		}
		$info['wlmb_type'] = 'num';
		$wlmb_data = array();
		$seo = pe_seo($menutitle='新增模板');
		include(pe_tpl('wlmb_add.html'));
	break;
	//####################// 修改模板 //####################//
	case 'edit':
		$wlmb_id = intval($_g_id);
		$info = $db->pe_select('wlmb', array('wlmb_id'=>$wlmb_id));
		if (isset($_p_pesubmit)) {
			pe_token_match();
			if (!$_p_wlmb_name) pe_apidata(array('code'=>0, 'msg'=>'请填写名称'));
			if (!$_p_wlmb_type) pe_apidata(array('code'=>0, 'msg'=>'请选择类型'));
			if (is_array($_p_city)) {
				foreach ($_p_city as $k=>$v) {
					if ($_p_city[$k] or $_p_value[$k] or $_p_money[$k]) {
						$num = $k + 1;
						if (!$_p_city[$k]) pe_apidata(array('code'=>0, 'msg'=>"请填写第{$num}个配送区域"));
						if (!$_p_value[$k]) pe_apidata(array('code'=>0, 'msg'=>"请填写第{$num}个首重/首件"));
						if (!$_p_money[$k]) pe_apidata(array('code'=>0, 'msg'=>"请填写第{$num}个运费"));	
					}
					$wlmb_data[] = pe_dbhold(array('city'=>$_p_city[$k],'value'=>$_p_value[$k],'money'=>$_p_money[$k],'value2'=>$_p_value2[$k],'money2'=>$_p_money2[$k]));
				}
			}
			if (!is_array($wlmb_data)) pe_apidata(array('code'=>0, 'msg'=>"请填写运费设置"));
			$sql_set['wlmb_name'] = $_p_wlmb_name;
			$sql_set['wlmb_type'] = $_p_wlmb_type;			
			$sql_set['wlmb_data'] = json_encode($wlmb_data);				
			if ($db->pe_update('wlmb', array('wlmb_id'=>$wlmb_id), pe_dbhold($sql_set, array('wlmb_data')))) {
				pe_apidata(array('code'=>1, 'msg'=>'修改成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'修改失败'));
			}
		}
		$wlmb_data = $info['wlmb_data'] ? json_decode($info['wlmb_data'], true) : array();
		$seo = pe_seo($menutitle='修改模板');
		include(pe_tpl('wlmb_add.html'));
	break;
	//####################// 模板删除 //####################//
	case 'del':
		pe_token_match();
		$wlmb_id = intval($_g_id);
		$wlmb_id == 1 && pe_apidata(array('code'=>0, 'msg'=>'默认模板不能删除'));
		if ($db->pe_delete('wlmb', array('wlmb_id'=>$wlmb_id))) {
			pe_apidata(array('code'=>1, 'msg'=>'删除成功'));
		}
		else {
			pe_apidata(array('code'=>0, 'msg'=>'删除失败'));
		}
	break;
	//####################// 选择城市 //####################//
	case 'city':
		$seo = pe_seo($menutitle='选择城市');
		include(pe_tpl('wlmb_city.html'));		
	break;
	//####################// 模板列表 //####################//
	default:
		$info_list = $db->pe_selectall('wlmb', array('order by'=>'`wlmb_id` asc'));
		$tongji['all'] = $db->pe_num('wlmb');
		$seo = pe_seo($menutitle='运费模板');
		include(pe_tpl('wlmb_list.html'));
	break;
}
function city_list() {
	global $db;
	$city_list = $db->pe_selectall('city', array('city_pid'=>0));
	foreach ($city_list as $v) {
		$info_list[] = $v['city_name'];
	}
	return $info_list;
}
?>