<?php
/**
 * @copyright   2008-2021 简好网络 <http://www.phpshe.com>
 * @creatdate   2012-0501 koyshe <koyshe@gmail.com>
 */
$menumark = 'order';
pe_lead('hook/refund.hook.php');
$cache_payment = cache::get('payment');
switch ($act) {
	//####################// 订单详情 //####################//
	case 'view':
		$order_id = pe_dbhold($_g_id);	
		$info = $db->pe_select('order', array('order_id'=>$order_id));
		$product_list = $db->pe_selectall('orderdata', array('order_id'=>$info['order_id']));
		$prokey_list = $db->pe_selectall('prokey', array('order_id'=>$info['order_id'], 'order by'=>'prokey_id asc'));

		$seo = pe_seo($menutitle='订单详情', '', '', 'admin');
		include(pe_tpl('order_view.html'));
	break;
	//####################// 订单修改 //####################//
	case 'edit':
		$order_id = pe_dbhold($_g_id);
		if (isset($_p_pesubmit)) {
			pe_token_match();			
			if ($db->pe_update('order', array('order_id'=>$order_id), pe_dbhold($_p_info))) {
				pe_apidata(array('code'=>1, 'msg'=>'修改成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'修改失败'));
			}
		}
		$info = $db->pe_select('order', array('order_id'=>$order_id));
		$wlname_list = $cache_setting['web_wlname'] ? explode(',', $cache_setting['web_wlname']) : array();
		$seo = pe_seo($menutitle='订单修改', '', '', 'admin');
		include(pe_tpl('order_add.html'));
	break;
	//####################// 订单删除 //####################//
	case 'del':
		pe_token_match();
		$order_id = pe_dbhold($_g_id);
		$info = $db->pe_select('order', array('order_id'=>$order_id));
		if (!in_array($info['order_state'], array('success', 'close'))) pe_apidata(array('code'=>0, 'msg'=>'交易完成或关闭的订单才可以删除'));
		if ($db->pe_delete('order', array('order_id'=>$order_id))) {
			$db->pe_delete('orderdata', array('order_id'=>$order_id));
			$refund_list = $db->pe_selectall('refund', array('order_id'=>$order_id));
			foreach ($refund_list as $v) {
				$db->pe_delete('refund', array('refund_id'=>$v['refund_id']));
				$db->pe_delete('refundlog', array('refund_id'=>$v['refund_id']));
			}
			pe_apidata(array('code'=>1, 'msg'=>'删除成功'));
		}
		else {
			pe_apidata(array('code'=>0, 'msg'=>'删除失败'));
		}
	break;
	//####################// 订单付款 //####################//
	case 'pay':
		$order_id = pe_dbhold($_g_id);
		$info = $db->pe_select('order', array('order_id'=>$order_id));
		if (isset($_p_pesubmit)) {
			pe_token_match();
			if ($info['order_state'] != 'wpay') pe_apidata(array('code'=>0, 'msg'=>'请勿重复付款'));
			//生成付款单
			$sql_set['pay_id'] = $pay_id = pe_guid('pay|pay_id', 'pay');
			$sql_set['pay_type'] = 'order';
			$sql_set['order_id'] = $order_id;
			$sql_set['order_name'] = $info['order_name'];
			$sql_set['order_money'] = $info['order_money'];
			$sql_set['order_atime'] = time();
			$sql_set['order_payment'] = 'adminpay';
			$sql_set['order_payment_name'] = $cache_payment['adminpay']['payment_name'];
			$sql_set['user_id'] = $info['user_id'];
			$sql_set['user_name'] = $info['user_name'];		
			if (!$db->pe_insert('pay', pe_dbhold($sql_set))) pe_apidata(array('code'=>0, 'msg'=>'系统异常，请重试'));
			if (pay_success($pay_id, 'adminpay')) {
				pe_apidata(array('code'=>1, 'msg'=>'付款成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'付款失败'));
			}
		}
		include(pe_tpl('order_pay.html'));
	break;
	//####################// 订单发货 //####################//
	case 'send':
		$order_id = pe_dbhold($_g_id);
		$info = $db->pe_select('order', array('order_id'=>$order_id));
		if (isset($_p_pesubmit)) {
			pe_token_match();
			if ($info['order_state'] != 'wsend') pe_apidata(array('code'=>0, 'msg'=>'请勿重复发货'));
			if ($info['order_virtual']) {
				$result = virtual_order_send_callback($info);
			}
			else {
				$result = order_send_callback($info, $_p_order_wl_id, $_p_order_wl_name);
			}
			if ($result) {
				pe_apidata(array('code'=>1, 'msg'=>'操作成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'操作失败'));
			}
		}
		$wlname_list = $cache_setting['web_wlname'] ? explode(',', $cache_setting['web_wlname']) : array();
		//检测是否有退款子订单
		$refund_list = $db->pe_selectall('orderdata', array('order_id'=>$order_id, 'refund_state'=>array('wcheck', 'wsend', 'wget', 'refuse')));
		include(pe_tpl('order_send.html'));
	break;
	//####################// 审核通过 //####################//
	case 'check':
		pe_token_match();
		$order_id = pe_dbhold($_g_id);
		$info = $db->pe_select('order', array('order_id'=>$order_id));
		if ($info['order_state'] != 'wcheck') pe_apidata(array('code'=>0, 'msg'=>'参数错误'));
		if ($db->pe_update('order', array('order_id'=>$order_id), array('order_state'=>'wsend'))) {
			pe_apidata(array('code'=>1, 'msg'=>'操作成功'));
		}
		else {
			pe_apidata(array('code'=>0, 'msg'=>'操作失败'));
		}
	break;
	//####################// 订单核销 //####################//
	case 'hexiao':
		pe_token_match();
		$order_id = pe_dbhold($_g_id);
		$info = $db->pe_select('order', array('order_id'=>$order_id));
		if ($info['order_state'] != 'whx') pe_apidata(array('code'=>0, 'msg'=>'参数错误'));
		order_send_callback($info);
		order_success_callback($info);
		pe_apidata(array('code'=>1, 'msg'=>'核销成功'));
		/*if (isset($_p_pesubmit)) {
			$order_code = pe_dbhold($_p_order_code);
			if (!$order_code) pe_apidata(array('code'=>0, 'msg'=>'券码无效'));
			$info = $db->pe_select('order', array('order_code'=>$order_code));
			if (!$info['order_id']) pe_apidata(array('code'=>0, 'msg'=>'订单不存在'));	
			if ($info['order_state'] != 'whx') pe_apidata(array('code'=>0, 'msg'=>'订单无效或已核销'));
			order_send_callback($info);
			order_success_callback($info);
			pe_apidata(array('code'=>1, 'msg'=>'核销成功'));
		}
		include(pe_tpl('order_hexiao.html'));*/
	break;
	//####################// 订单确认收货 //####################//
	case 'success':
		pe_token_match();
		$order_id = pe_dbhold($_g_id);
		$info = $db->pe_select('order', array('order_id'=>$order_id));
		if ($info['order_state'] != 'wget') pe_apidata(array('code'=>0, 'msg'=>'参数错误'));
		if (order_success_callback($info)) {
			pe_apidata(array('code'=>1, 'msg'=>'操作成功'));
		}
		else {
			pe_apidata(array('code'=>0, 'msg'=>'操作失败'));
		}
	break;
	//####################// 订单关闭 //####################//
	case 'close':
		$order_id = pe_dbhold($_g_id);
		$info = $db->pe_select('order', array('order_id'=>$order_id));
		if (isset($_p_pesubmit)) {
			pe_token_match();
			if ($info['order_state'] == 'close') pe_apidata(array('code'=>0, 'msg'=>'请勿重复关闭'));
			if ($info['order_state'] == 'success') pe_apidata(array('code'=>0, 'msg'=>'交易完成订单不可关闭'));
			if ($_p_order_closetext == '') pe_apidata(array('code'=>0, 'msg'=>'请填写关闭原因'));
			if (order_close_callback($info, $_p_order_closetext)) {
				pe_apidata(array('code'=>1, 'msg'=>'操作成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'操作失败'));
			}
		}
		include(pe_tpl('order_close.html'));
	break;
	//####################// 订单改价格 //####################//
	case 'money':		
		$order_id = pe_dbhold($_g_id);
		$info = $db->pe_select('order', array('order_id'=>$order_id));
		$product_list = $db->pe_selectall('orderdata', array('order_id'=>$order_id));
		if (isset($_p_pesubmit)) {
			pe_token_match();
			foreach ($product_list as $v) {
				$sql_set['product_jjmoney'] = pe_num($_p_product_jjmoney[$v['orderdata_id']], 'round', 2);
				$sql_set['product_allmoney'] = pe_num($v['product_money'] * $v['product_num'] + $sql_set['product_jjmoney'], 'round', 2);			
				$db->pe_update('orderdata', array('orderdata_id'=>$v['orderdata_id']), pe_dbhold($sql_set));
				$order_product_money += $sql_set['product_allmoney'];
			}
			$sql_order['order_product_money'] = $order_product_money;
			$sql_order['order_wl_money'] = $_p_order_wl_money;
			$sql_order['order_money'] = $order_product_money + $_p_order_wl_money - $info['order_quan_money'] - $info['order_point_money'];
			if ($db->pe_update('order', array('order_id'=>$order_id), pe_dbhold($sql_order))) {
				//更新收款单金额
				$pay_list = $db->pe_selectall('pay', " and find_in_set('{$order_id}', order_id) and order_state = 'wpay'");
				foreach ($pay_list as $v) {
					$v['order_id'] = $v['order_id'] ? explode(',', $v['order_id']) : array();
					$tongji = $db->pe_select('order', array('order_id'=>$v['order_id']), 'sum(order_money) as `money`');
					$db->pe_update('pay', array('pay_id'=>$v['pay_id']), array('order_money'=>$tongji['money']));
				}
				pe_apidata(array('code'=>1, 'msg'=>'修改成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'修改失败'));
			}
		}
		include(pe_tpl('order_money.html'));
	break;
	//####################// 打印购物单 //####################//
	case 'print_product1':
		$order_id = explode(',', pe_dbhold($_g_id));
		$info_list = $db->pe_selectall('order', array('order_id'=>$order_id));
		$seo = pe_seo($menutitle='打印发货单', '', '', 'admin');
		include(pe_tpl('order_print_product.html'));
	break;
	//####################// 打印配送单 //####################//
	case 'print_product':
		pe_lead('hook/feie.hook.php');
		$order_id = pe_dbhold($_g_id);
		$info = $db->pe_select('order', array('order_id'=>$order_id));
		return pe_apidata(feie_print($info));
	break;
	//####################// 打印快递单 //####################//
	case 'print_express':
		$order_id = pe_dbhold($_g_id);
		$express_id = intval($_g_express_id);
		$order = $db->pe_select('order', array('order_id'=>$order_id));

		$express_list = $db->pe_selectall('express', array('order by'=>'`express_order` asc, `express_id` desc'));
		!$express_id && $express_id = $express_list[0]['express_id'];
		$info = $db->pe_select('express', array('express_id`'=>$express_id));
		$tag_list = $info['express_tag'] ? unserialize($info['express_tag']) : array();

		$seo = pe_seo($menutitle='打印快递单', '', '', 'admin');
		include(pe_tpl('order_print_express.html'));
	break;
	//####################// 查看快递 //####################//
	case 'kuaidi':
		$info_list = order_kuaidi($_g_id, $_g_code);
		$seo = pe_seo($menutitle='查看快递', '', '', 'admin');
		include(pe_tpl('order_kuaidi.html'));
	break;
	//####################// 订单列表 //####################//
	default:
		$_g_state && $sql_where .= " and `order_state` = '".pe_dbhold($_g_state)."'";	
		$_g_id && $sql_where .= " and `order_id` = '".pe_dbhold($_g_id)."'";
		$_g_code && $sql_where .= " and `order_code` = '".pe_dbhold($_g_code)."'";
		$_g_pintuan_id && $sql_where .= " and `pintuan_id` = '".pe_dbhold($_g_pintuan_id)."'";
		$_g_user_id && $sql_where .= " and `user_id` = '".intval($_g_user_id)."'";
		$_g_user_name && $sql_where .= " and `user_name` = '".pe_dbhold($_g_user_name)."'";
		$_g_user_tname && $sql_where .= " and `user_tname` = '".pe_dbhold($_g_user_tname)."'";
		$_g_user_phone && $sql_where .= " and `user_phone` = '".pe_dbhold($_g_user_phone)."'";
		$_g_date1 && $sql_where .= " and `order_atime` >= '".strtotime($_g_date1)."'";
		$_g_date2 && $sql_where .= " and `order_atime` < '".(strtotime($_g_date2) + 86400)."'";
		if ($_g_state == 'wsend' or $_g_state == 'wtuan') {
			$sql_where .= " order by `order_ptime` desc";	
		}
		elseif ($_g_state == 'wget') {
			$sql_where .= " order by `order_stime` desc";
		}
		elseif (in_array($_g_state, array('success', 'close'))) {
			$sql_where .= " order by `order_ftime` desc";
		}
		else {
			$sql_where .= " order by `order_id` desc";		
		}
		if ($act == 'export') {
			$info_list = $db->pe_selectall('order', $sql_where);
			foreach ($info_list as $k => $v) {
				$info_list[$k]['product_list'] = $db->pe_selectall('orderdata', array('order_id'=>$v['order_id']));
			}
			pe_lead('include/class/excel_out.class.php');	 	 	 	 	 	 	 	 	
			$xls_data[] = array('订单编号', '订单状态', '下单时间', '实付款', '支付单号', '支付时间', '商品信息', '商品数量', '物流公司', '货运单号', '收货人姓名', '收货人地址', '收货人手机号', '订单留言');
			foreach($info_list as $k=>$v) {
				$product_name = $product_num = array();
				foreach ($v['product_list'] as $kk=>$vv) {
					$product_name[] = $vv['product_name'];
					$product_num[] = $vv['product_num'];				
				}
				$product_name = implode(' | ', $product_name);
				$product_num = implode(' | ', $product_num);
				$xls_data[] = array($v['order_id'], order_stateshow($v['order_state']), pe_date($v['order_atime'], 'Y/m/d H:i:s'), $v['order_money'], $v['order_payid'], pe_date($v['order_ptime'], 'Y/m/d H:i:s'), $product_name, $product_num, $v['order_wl_name'], $v['order_wl_id'], $v['user_tname'], $v['user_address'], $v['user_phone'], $v['order_text']);
			}
			$xls = new excel('UTF-8', false, '订单导出');  
			$xls->addArray($xls_data);  
			$xls->generateXML('订单导出');
		}
		else {
			$info_list = $db->pe_selectall('order', $sql_where, '*', array(100, $_g_page));
			foreach ($info_list as $k=>$v) {
				$info_list[$k]['product_list'] = $db->pe_selectall('orderdata', array('order_id'=>$v['order_id']));
			}
			//统计订单数量
			$tongji_arr = $db->index('order_state')->pe_selectall('order', array('group by'=>'order_state'), '`order_state`, count(1) as `num`');
			foreach ($ini['order_state'] as $k=>$v) {
				$tongji[$k] = intval($tongji_arr[$k]['num']);
				$tongji['all'] += intval($tongji[$k]);
			}
			$seo = pe_seo($menutitle='订单列表', '', '', 'admin');
			include(pe_tpl('order_list.html'));
		}
	break;
}
?>