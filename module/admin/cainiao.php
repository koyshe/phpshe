<?php
/**
 * @copyright   2008-2021 简好网络 <http://www.phpshe.com>
 * @creatdate   2012-0501 koyshe <koyshe@gmail.com>
 */
$menumark = 'cainiao';
switch ($act) {	
	//####################// 导出待发货 //####################//
	case 'export':
		$order_id = pe_dbhold($_p_order_id);
		$info_list = $db->pe_selectall('order', array('order_id'=>$order_id));
		foreach ($info_list as $k => $v) {
			$info_list[$k]['product_list'] = $db->pe_selectall('orderdata', array('order_id'=>$v['order_id']));
		}
		include("{$pe['path']}include/plugin/phpexcel/PHPExcel.php");
		$objExcel = new PHPExcel();  
		$objExcel->setActiveSheetIndex(0);   
		$objActSheet = $objExcel->getActiveSheet();   
		//设置当前活动sheet的名称
		$objActSheet->setTitle('sheet1');
		$objActSheet->setCellValue('A1', '');
		$objActSheet->setCellValue('B1', "收件信息");
		$objActSheet->mergeCells('B1:D1');
		$objActSheet->setCellValue('E1', "物品信息");
		$objActSheet->mergeCells('E1:F1');
		$objActSheet->setCellValue('G1', '备注');

		$objActSheet->setCellValue('A2', '编号');
		$objActSheet->setCellValue('B2', '收件人姓名');		
		$objActSheet->setCellValue('C2', '收件人联系方式');		
		$objActSheet->setCellValue('D2', '收件地址');	
		$objActSheet->setCellValue('E2', '物品类');	
		$objActSheet->setCellValue('F2', '物品详情');		
		$objActSheet->setCellValue('G2', '备注信息');
		$index = 2;
		foreach($info_list as $k=>$v) {
			$index++;
			$product_name = array();
			foreach ($v['product_list'] as $kk=>$vv) {
				$product_name[] = ($vv['product_jname'] ? $vv['product_jname'] : $vv['product_name'])."({$vv['product_num']}件)";
			}
			$objActSheet->setCellValue("A{$index}", $v['order_id']);
			$objActSheet->setCellValue("B{$index}", $v['user_tname']);
			$objActSheet->setCellValue("C{$index}", $v['user_phone']);		
			$objActSheet->setCellValue("D{$index}", $v['user_address']);
			$objActSheet->setCellValue("E{$index}", '日用品');
			$objActSheet->setCellValue("F{$index}", implode("; ", $product_name));
			$objActSheet->setCellValue("G{$index}", $v['order_text']);
			//$objActSheet->getStyle("I{$index}")->getAlignment()->setWrapText(true);
		}
		$order_id1 = $info_list[0]['order_id'];
		$order_id2 = $info_list[count($info_list) - 1]['order_id'];
		$db->pe_update('order', array('order_id'=>$order_id), array('order_print'=>1));
		$xls_name = "PHPSHE商城订单导出{$order_id1}_{$order_id2}.xlsx";
		$objWriter = PHPExcel_IOFactory::createWriter($objExcel, 'Excel2007');			
		header("Pragma: public");  
		header("Expires: 0");  
		header("Cache-Control:must-revalidate, post-check=0, pre-check=0");  
		header("Content-Type:application/force-download");  
		header("Content-Type:application/vnd.ms-execl");  
		header("Content-Type:application/octet-stream");  
		header("Content-Type:application/download");;  
		header('Content-Disposition:attachment;filename='.$xls_name.'');  
		header("Content-Transfer-Encoding:binary");  
		$objWriter->save('php://output');
	break;
	//####################// 确认导出/打印 //####################//
	case 'confirm_print':
		$order_id = pe_dbhold($_p_order_id);
		if ($db->pe_update('order', array('order_id'=>$order_id), array('order_print'=>1))) {
			pe_apidata(array('code'=>1, 'msg'=>'操作成功'));
		}
		else {
			pe_apidata(array('code'=>0, 'msg'=>'操作失败'));		
		}
	break;
	//####################// 导入发货 //####################//
	case 'import':
		if (isset($_p_pesubmit)) {
			if ($_FILES['file']['size']) {
				pe_lead('include/class/upload.class.php');
				$upload = new upload($_FILES['file'], 'data/attachment/import/', array('local'=>1));
				$file = $upload->filepath_full;
			}
			if (!is_file($file)) pe_error('请选择要导入的xls文件...');
			include("{$pe['path']}include/plugin/phpexcel/PHPExcel.php");
			$objReader = new PHPExcel_Reader_Excel2007();
			if (!$objReader->canRead($file)) {
			    $objReader = new PHPExcel_Reader_Excel5();
				if (!$objReader->canRead($file)) {
					pe_error('无法识别的xls文件...');
				}
			}
			$objPHPExcel = $objReader->load($file);
			$sheet = $objPHPExcel->getSheet(0);//获取第一个工作表
			$row_num = $sheet->getHighestRow();//取得总行数
			$col_num = $sheet->getHighestColumn(); //取得总列数
			$order_wl_id = $order_wl_name = array();
			if ($objPHPExcel->getActiveSheet()->getCell("O1")->getValue() != '包裹号') pe_error('导入模板错误');
			for ($i = 2; $i <= $row_num; $i++) {
				$order_id = cainiao_format($objPHPExcel->getActiveSheet()->getCell("A{$i}")->getValue());
				$order_wl_id = cainiao_format($objPHPExcel->getActiveSheet()->getCell("O{$i}")->getValue());
				$order_wl_name = cainiao_format($objPHPExcel->getActiveSheet()->getCell("N{$i}")->getValue());
				if (!$order_id or !$order_wl_id or !$order_wl_name) continue;
				$order_idarr[] = $order_id;
				$order_wl_id_arr[$order_id] = $order_wl_id;
				$order_wl_name_arr[$order_id] = $order_wl_name;
			}
			$num = 0;
			$info_list = $db->index('order_id')->pe_selectall('order', array('order_id'=>$order_idarr), 'order_id, order_state, order_wl_id, order_wl_name');
			foreach ($info_list as $info) {		
				if (!$info['order_id']) continue;
				//已经发货没更新上快递单号的在这里重新补上
				if ($info['order_state'] == 'wget' && ($info['order_wl_id'] == '' or $info['order_wl_name'] == '')) {
					$sqlset_order['order_wl_id'] = $order_wl_id_arr[$info['order_id']];
					$sqlset_order['order_wl_name'] = $order_wl_name_arr[$info['order_id']];				
					$db->pe_update('order', array('order_id'=>$info['order_id']), pe_dbhold($sqlset_order));
					$num++;
				}
				if ($info['order_state'] != 'wsend') continue;
				if ($info['order_virtual']) {
					virtual_order_send_callback($info);
				}
				else {
					order_send_callback($info, $order_wl_id_arr[$info['order_id']], $order_wl_name_arr[$info['order_id']]);
				}
				$num++;
			}
			pe_success("发货成功，共计：{$num}条");
		}
		$seo = pe_seo($menutitle='导入并发货', '', '', 'admin');
		include(pe_tpl('cainiao_import.html'));
	break;
	//####################// 订单列表 //####################//
	default:
		if (in_array($_g_state, array('wsend_0', 'wsend_1'))) {
			$state = explode('_', $_g_state);
			$sql_where = " and `order_state` = '{$state[0]}' and `order_print` = '{$state[1]}' order by `order_ptime` desc";
			$pagenum = 10000;
		}
		elseif ($_g_state == 'wget') {
			$sql_where = " and `order_state` = 'wget' order by `order_stime` desc";
			$pagenum = 200;
		}
		elseif ($_g_state == 'success') {
			$sql_where = " and `order_state` = 'success' order by `order_ftime` desc";
			$pagenum = 3;
		}
		$info_list = $db->pe_selectall('order', $sql_where, '*', array($pagenum, $_g_page));
		foreach ($info_list as $k => $v) {
			$info_list[$k]['product_list'] = $db->pe_selectall('orderdata', array('order_id'=>$v['order_id']));
		}
		
		$tongji['wsend_0'] = $db->pe_num('order', array('order_state'=>'wsend', 'order_print'=>0));
		$tongji['wsend_1'] = $db->pe_num('order', array('order_state'=>'wsend', 'order_print'=>1));
		$tongji['wget'] = $db->pe_num('order', array('order_state'=>'wget'));
		$tongji['success'] = $db->pe_num('order', array('order_state'=>'success'));		
		
		$seo = pe_seo($menutitle='批量发货打印', '', '', 'admin');
		include(pe_tpl('cainiao_list.html'));
	break;
}

//过滤菜鸟导出的特殊字符
function cainiao_format($str) {
	return $str;
	return trim(str_ireplace(array('"', '=', "'"), '', $str));
}
?>