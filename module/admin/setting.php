<?php
/**
 * @copyright   2008-2014 简好网络 <http://www.phpshe.com>
 * @creatdate   2012-0501 koyshe <koyshe@gmail.com>
 */
$menumark = 'setting';
pe_lead('hook/cache.hook.php');
pe_lead('hook/wechat.hook.php');
switch ($act) {
	//####################// 会员设置 //####################//
	case 'user':
		if (isset($_p_pesubmit)) {
			pe_token_match();
			if ($_p_info['cashout_min'] <= 0) $_p_info['cashout_min'] = 0.1;
			$sql = "update `".dbpre."setting` set `setting_value` = case `setting_key` {$sqlset}
				when 'web_guestbuy' then '".intval($_p_info['web_guestbuy'])."'
				when 'web_checkphone' then '".intval($_p_info['web_checkphone'])."'
				when 'web_checkemail' then '".intval($_p_info['web_checkemail'])."'
				when 'cashout_min' then '".round($_p_info['cashout_min'], 1)."'
				when 'cashout_fee' then '".(round($_p_info['cashout_fee'], 2)/100)."'
				when 'cashout_date' then '".pe_dbhold($_p_info['cashout_date'])."'
				when 'cashout_desc' then '".pe_dbhold($_p_info['cashout_desc'])."'
				when 'point_bili' then '".intval($_p_info['point_bili'])."'
				when 'point_maxlimit' then '".(round($_p_info['point_maxlimit'], 2)/100)."'
				when 'point_reg' then '".intval($_p_info['point_reg'])."'
				when 'point_tg' then '".intval($_p_info['point_tg'])."'
				when 'point_order' then '".(round($_p_info['point_order'], 2)/100)."'
				when 'point_comment' then '".intval($_p_info['point_comment'])."'
				else `setting_value` end";					
			if ($db->sql_update($sql)) {
				cache_write('setting');
				pe_apidata(array('code'=>1, 'msg'=>'设置成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'设置失败'));
			}
		}
		$info = $db->index('setting_key')->pe_selectall('setting');
		$seo = pe_seo($menutitle='会员设置', '', '', 'admin');
		include(pe_tpl('setting_user.html'));
	break;
	//####################// 分销设置 //####################//
	case 'tg':
		if (isset($_p_pesubmit)) {
			pe_token_match();
			$sql = "update `".dbpre."setting` set `setting_value` = case `setting_key`
				when 'tg_state' then '".intval($_p_tg_state)."'
				when 'tg_fc1' then '".(round($_p_tg_fc1, 2)/100)."'
				when 'tg_fc2' then '".(round($_p_tg_fc2, 2)/100)."'
				when 'tg_fc3' then '".(round($_p_tg_fc3, 2)/100)."'
				when 'tg_desc' then '".pe_dbhold($_p_tg_desc, 'all')."'
				else `setting_value` end";				
			if ($db->sql_update($sql)) {
				cache_write('setting');
				pe_apidata(array('code'=>1, 'msg'=>'设置成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'设置失败'));
			}
		}
		$info = $db->index('setting_key')->pe_selectall('setting');		
		$seo = pe_seo($menutitle='分销设置', '', '', 'admin');
		include(pe_tpl('setting_tg.html'));		
	break;
	//####################// 短信设置 //####################//
	case 'sms':
		if (isset($_p_pesubmit)) {
			pe_token_match();
			$sql = "update `".dbpre."setting` set `setting_value` = case `setting_key`
				when 'sms_key' then '".pe_dbhold($_p_sms_key)."'
				when 'sms_sign' then '".pe_dbhold($_p_sms_sign)."'
				when 'sms_admin' then '".pe_dbhold($_p_sms_admin)."'
				else `setting_value` end";				
			if ($db->sql_update($sql)) {
				cache_write('setting');
				pe_apidata(array('code'=>1, 'msg'=>'设置成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'设置失败'));
			}
		}
		$info = $db->index('setting_key')->pe_selectall('setting');	
		$notice_list = $db->index('notice_user|notice_type')->pe_selectall('notice', array('notice_way'=>'sms'));
		$seo = pe_seo($menutitle='短信设置', '', '', 'admin');
		include(pe_tpl('setting_sms.html'));
	break;
	//####################// 短信测试 //####################//
	case 'sms_test':
		pe_token_match();
		if (!$_g_user) pe_error('管理员手机号未填写...');
		foreach (explode(',', $_g_user) as $k=>$v) {
			if (!$v) continue;
			$sql_set[$k]['noticelog_way'] = 'sms';
			$sql_set[$k]['noticelog_user'] = pe_dbhold($v);
			$sql_set[$k]['noticelog_name'] = '';
			$sql_set[$k]['noticelog_text'] = "【简好网络】尊敬的用户：您好，欢迎使用简好网络旗下软件 - PHPSHE商城系统，官网：http://www.phpshe.com";
			$sql_set[$k]['noticelog_atime'] = time();			
		}
		if ($db->pe_insert('noticelog', $sql_set)) {
			pe_apidata(array('code'=>1, 'msg'=>'发送中，请在统计记录查看发送结果'));
		}
		else {
			pe_apidata(array('code'=>0, 'msg'=>'设置失败'));
		}
	break;
	//####################// 邮箱设置 //####################//
	case 'email':
		if (isset($_p_pesubmit)) {
			pe_token_match();
			$sql = "update `".dbpre."setting` set `setting_value` = case `setting_key`
				when 'email_smtp' then '".pe_dbhold($_p_email_smtp)."'
				when 'email_ssl' then '".pe_dbhold($_p_email_ssl)."'
				when 'email_port' then '".pe_dbhold($_p_email_port)."'
				when 'email_name' then '".pe_dbhold($_p_email_name)."'
				when 'email_pw' then '".pe_dbhold($_p_email_pw)."'
				when 'email_nname' then '".pe_dbhold($_p_email_nname)."'
				when 'email_admin' then '".pe_dbhold($_p_email_admin)."'
				else `setting_value` end";
			if ($db->sql_update($sql)) {
				cache_write('setting');
				pe_apidata(array('code'=>1, 'msg'=>'设置成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'设置失败'));
			}
		}
		$info = $db->index('setting_key')->pe_selectall('setting');
		$notice_list = $db->index('notice_user|notice_type')->pe_selectall('notice', array('notice_way'=>'email'));			
		$seo = pe_seo($menutitle='邮箱设置', '', '', 'admin');
		include(pe_tpl('setting_email.html'));		
	break;
	//####################// 邮件测试 //####################//
	case 'email_test':
		pe_token_match();
		if (!$_g_user) pe_error('管理员邮箱未填写...');
		foreach (explode(',', $_g_user) as $k=>$v) {
			if (!$v) continue;
			$sql_set[$k]['noticelog_way'] = 'email';
			$sql_set[$k]['noticelog_user'] = pe_dbhold($v);
			$sql_set[$k]['noticelog_name'] = 'PHPSHE商城系统测试邮件';
			$sql_set[$k]['noticelog_text'] = '尊敬的用户：您好，欢迎使用简好网络旗下软件 - PHPSHE商城系统<br/><br/>简好网络官网：http://www.phpshe.com<br/><br/>邮件发送日期：'.pe_date(time());
			$sql_set[$k]['noticelog_atime'] = time();			
		}
		if ($db->pe_insert('noticelog', $sql_set)) {
			pe_apidata(array('code'=>1, 'msg'=>'发送中，请在统计记录查看发送结果'));
		}
		else {
			pe_apidata(array('code'=>0, 'msg'=>'设置失败'));
		}	
	break;
	//####################// 自提点设置 //####################//
	case 'pickup':
		if (isset($_p_pesubmit)) {
			pe_token_match();
			$pickup_lbs = $_p_pickup_lbs ? explode(',', $_p_pickup_lbs) : array();
			$sql = "update `".dbpre."setting` set `setting_value` = case `setting_key`
				when 'pickup_name' then '".pe_dbhold($_p_info['pickup_name'])."'
				when 'pickup_address' then '".pe_dbhold($_p_info['pickup_address'])."'
				when 'pickup_lat' then '".pe_dbhold($pickup_lbs[0])."'
				when 'pickup_lng' then '".pe_dbhold($pickup_lbs[1])."'
				when 'pickup_phone' then '".pe_dbhold($_p_info['pickup_phone'])."'
				else `setting_value` end";				
			if ($db->sql_update($sql)) {
				cache_write('setting');
				pe_success('设置成功');
			}
			else {
				pe_error('设置失败...');
			}
		}
		$info = $db->index('setting_key')->pe_selectall('setting');
		$seo = pe_seo($menutitle='自提点设置', '', '', 'admin');
		include(pe_tpl('setting_pickup.html'));		
	break;
	//####################// 快递设置 //####################//
	case 'kuaidi':
		if (isset($_p_pesubmit)) {
			pe_token_match();
			$sql = "update `".dbpre."setting` set `setting_value` = case `setting_key`
				when 'web_wlname' then '".pe_dbhold($_p_info['web_wlname'])."'
				when 'web_wlkey' then '".pe_dbhold($_p_info['web_wlkey'])."'
				else `setting_value` end";
			if ($db->sql_update($sql)) {
				cache_write('setting');
				pe_apidata(array('code'=>1, 'msg'=>'设置成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'设置失败'));
			}
		}
		$info = $db->index('setting_key')->pe_selectall('setting');
		$seo = pe_seo($menutitle='快递设置', '', '', 'admin');
		include(pe_tpl('setting_kuaidi.html'));		
	break;
	//####################// 上传设置 //####################//
	case 'upload':
		if (isset($_p_pesubmit)) {
			pe_token_match();
			$sql = "update `".dbpre."setting` set `setting_value` = case `setting_key`
				when 'upload_server' then '".pe_dbhold($_p_upload_server)."'
				when 'upload_aliyun_domain' then '".pe_dbhold($_p_upload_aliyun_domain)."'
				when 'upload_aliyun_accesskey_id' then '".pe_dbhold($_p_upload_aliyun_accesskey_id)."'
				when 'upload_aliyun_accesskey_secret' then '".pe_dbhold($_p_upload_aliyun_accesskey_secret)."'
				else `setting_value` end";
			if ($db->sql_update($sql)) {
				cache_write('setting');
				pe_apidata(array('code'=>1, 'msg'=>'设置成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'设置失败'));
			}
		}
		$info = $db->index('setting_key')->pe_selectall('setting');
		$ini['upload_server'] = array('local'=>'本地服务器', 'aliyun'=>'阿里云');
		$seo = pe_seo($menutitle='上传设置', '', '', 'admin');
		include(pe_tpl('setting_upload.html'));		
	break;
	//####################// 登录设置 //####################//
	case 'login':
		if (isset($_p_pesubmit)) {
			pe_token_match();
			$sql = "update `".dbpre."setting` set `setting_value` = case `setting_key`
				when 'login_qq_state' then '".intval($_p_login_qq_state)."'
				when 'login_qq_appid' then '".pe_dbhold($_p_login_qq_appid)."'
				when 'login_qq_appkey' then '".pe_dbhold($_p_login_qq_appkey)."'
				when 'login_wx_state' then '".intval($_p_login_wx_state)."'
				when 'login_wx_appid' then '".pe_dbhold($_p_login_wx_appid)."'
				when 'login_wx_appsecret' then '".pe_dbhold($_p_login_wx_appsecret)."'
				else `setting_value` end";
			if ($db->sql_update($sql)) {
				cache_write('setting');
				pe_apidata(array('code'=>1, 'msg'=>'设置成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'设置失败'));
			}
		}
		$info = $db->index('setting_key')->pe_selectall('setting');
		$seo = pe_seo($menutitle='登录设置', '', '', 'admin');
		include(pe_tpl('setting_login.html'));		
	break;
	//####################// 网站设置 //####################//
	default:
		if (isset($_p_pesubmit)) {
			pe_token_match();
			$sql = "update `".dbpre."setting` set `setting_value` = case `setting_key`
				when 'web_title' then '".pe_dbhold($_p_info['web_title'])."'
				when 'web_keywords' then '".pe_dbhold($_p_info['web_keywords'])."'
				when 'web_description' then '".pe_dbhold($_p_info['web_description'])."'
				when 'web_copyright' then '".pe_dbhold($_p_info['web_copyright'])."'
				when 'web_icp' then '".pe_dbhold($_p_info['web_icp'])."'
				when 'web_logo' then '".pe_dbhold($_p_info['web_logo'])."'
				when 'h5_logo' then '".pe_dbhold($_p_info['h5_logo'])."'
				when 'web_qrcode' then '".pe_dbhold($_p_info['web_qrcode'])."'
				when 'web_wechat_qrcode' then '".pe_dbhold($_p_info['web_wechat_qrcode'])."'
				when 'kefu_wechat' then '".pe_dbhold($_p_info['kefu_wechat'])."'
				when 'kefu_phone' then '".pe_dbhold($_p_info['kefu_phone'])."'
				when 'kefu_qq' then '".pe_dbhold($_p_info['kefu_qq'])."'
				when 'web_hotword' then '".pe_dbhold($_p_info['web_hotword'])."'
				when 'web_state' then '".intval($_p_info['web_state'])."'
				when 'web_tongji' then '".pe_dbhold($_p_info['web_tongji'], 'all')."'
				else `setting_value` end";
			if ($db->sql_update($sql)) {
				cache_write('setting');
				pe_apidata(array('code'=>1, 'msg'=>'设置成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'设置失败'));
			}
		}	
		$info = $db->index('setting_key')->pe_selectall('setting');
		$seo = pe_seo($menutitle='网站设置', '', '', 'admin');
		include(pe_tpl('setting_base.html'));
	break;
}
?>