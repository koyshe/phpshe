<?php
$menumark = 'index';
pe_lead('hook/refund.hook.php');
switch($act) {
	//####################// 个人中心 //####################//
	default:
		//用户基本信息
		$info['user_name'] = $user['user_name'];
		$info['user_logo'] = pe_thumb($user['user_logo'], 100, 100, 'avatar');
		$info['userlevel_name'] = $cache_userlevel[$user['userlevel_id']]['userlevel_name'];
		$info['user_money'] = $user['user_money'];		
		$info['user_point'] = $user['user_point'];
		//统计订单数量
		$info['wpay_num'] = $db->pe_num('order', array('user_id'=>$user['user_id'], 'order_state'=>'wpay'));
		$info['wsend_num'] = $db->pe_num('order', array('user_id'=>$user['user_id'], 'order_state'=>'wsend'));	
		//最新订单列表
		$list = $db->pe_selectall('order', array('user_id'=>$user['user_id'], 'order by'=>'order_id desc'), '*', array(5));
		foreach ($list as $k => $v) {
			$list[$k]['order_atime'] = pe_date($v['order_atime']);
			$list[$k]['order_stateshow'] = order_stateshow($v['order_state'], 'html');
			$product_list = $db->pe_selectall('orderdata', array('order_id'=>$v['order_id']));
			foreach ($product_list as $kk=>$vv) {
				$product_list[$kk]['product_logo'] = pe_thumb($vv['product_logo'], 400, 400);
				$product_list[$kk]['product_rule'] = order_skushow($vv['product_rule']);
				$product_list[$kk]['refund_stateshow'] = refund_stateshow($vv['refund_state'], 'html');
			}
			$list[$k]['product_list'] = $product_list;
		}
		pe_fixurl(pe_url("/page/user/index", 'app'));
		$seo = pe_seo($menutitle='个人中心');
		include(pe_tpl('index.html'));
	break;
}
?>