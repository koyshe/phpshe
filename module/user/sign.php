<?php
/**
 * @copyright   2006-**** 简好网络 <http://www.phpshe.com>
 * @creatdate   2012-0501 koyshe <koyshe@gmail.com>
 */
$menumark = 'sign';
$zhou_arr = array('Mon'=>'周一', 'Tue'=>'周二', 'Wed'=>'周三', 'Thu'=>'周四', 'Fri'=>'周五', 'Sat'=>'周六', 'Sun'=>'周日');
$daynum = date('t');
switch ($act) {
	//####################// 每日签到 //####################//
	default:
		$nowdate = date('Y-m-d');
		$lastdate = date('Y-m-d', strtotime('-1 day'));
		$nowmonth = date('Y-m');
		$config = $db->index('key')->pe_selectall('sign');
		$today = $db->pe_select('signlog', array('user_id'=>$user['user_id'], 'signlog_adate'=>$nowdate));
		//计算本月连续签到和累计签到
		if ($today['signlog_id']) {
			$lianxu = $db->pe_select('signlog', array('user_id'=>$user['user_id'], 'signlog_adate'=>$nowdate), 'signlog_lx_day as `day`');		
		}
		else {
			$lianxu = $db->pe_select('signlog', array('user_id'=>$user['user_id'], 'signlog_adate'=>$lastdate), 'signlog_lx_day as `day`');
			if (date('j') == 1) $lianxu['day'] = 0;	
		}
		$sign_lx_day = intval($lianxu['day']);
		$sign_lj_day = $db->pe_num('signlog', array('user_id'=>$user['user_id'], 'signlog_month'=>$nowmonth));
		if (isset($_p_pesubmit)) {
			if (!$config['sign_state']['value']) pe_apidata(array('code'=>0, 'msg'=>'活动未开启'));
			if ($today['signlog_id']) pe_apidata(array('code'=>0, 'msg'=>'请勿重复签到'));
			$sql_set['signlog_point'] = $config['sign_point']['value'];
			$sql_set['signlog_atime'] = time();
			$sql_set['signlog_adate'] = $nowdate;
			$sql_set['signlog_month'] = $nowmonth;
			$sql_set['signlog_lx_day'] = $sign_lx_day + 1;		
			$sql_set['signlog_lj_day'] = $sign_lj_day + 1;
			$sql_set['signlog_ip'] = pe_ip();
			$sql_set['user_id'] = $user['user_id'];
			$sql_set['user_name'] = $user['user_name'];
			$sql_set['user_logo'] = $user['user_logo'];
			if ($signlog_id = $db->pe_insert('signlog', pe_dbhold($sql_set))) {
				$point = sign_callback($signlog_id, $sql_set['signlog_lx_day'], $sql_set['signlog_lj_day']);
				pe_apidata(array('code'=>1, 'msg'=>"已获得{$point}积分"));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'签到失败'));
			}
		}
		//本月签到记录
		$info_list = $db->index('day')->pe_selectall('signlog', array('user_id'=>$user['user_id'], 'signlog_month'=>$nowmonth), "from_unixtime(signlog_atime, '%e') as day");
		for ($i=1; $i<=$daynum; $i++) {
			$date_list[$i]['sign'] = is_array($info_list[$i]) ? true : false;	
			$date_list[$i]['today'] = ($i == date('j')) ? true : false;	
		}
		$info['date_list'] = $date_list;
		$info['nowmonth'] = $nowmonth;
		$info['sign'] = $today['signlog_id'] ? true : false;
		$info['sign_lx_day'] = $sign_lx_day;
		$info['sign_lj_day'] = $sign_lj_day;
		$info['sign_text'] = $config['sign_text']['value'];
		$info['user_point'] = $user['user_point'];
		pe_fixurl(pe_url("/page/user/sign", 'app'));
		$seo = pe_seo($menutitle='签到有礼');
		include(pe_tpl('sign.html'));
	break;
}

function sign_callback($signlog_id, $lx_day, $lj_day) {
	global $db, $config, $user;
	$sign_allpoint = 0;
	$sign_point = intval($config['sign_point']['value']);
	$sign_point_shouci = intval($config['sign_point_shouci']['value']);
	$lianxu_arr = $config['sign_point_lianxu']['value'] ? unserialize($config['sign_point_lianxu']['value']) : array();
	$leiji_arr = $config['sign_point_leiji']['value'] ? unserialize($config['sign_point_leiji']['value']) : array();
	//日常签到奖励
	if ($sign_point) {
		add_pointlog($user['user_id'], 'add', $sign_point, "【".date('Y-m-d')."】每日签到奖励");
		$sign_allpoint += $sign_point;
	}
	//首次签到奖励
	if ($sign_point_shouci && check_shouci() == 1) {
		add_pointlog($user['user_id'], 'add', $sign_point_shouci, "【".date('Y-m')."】首次签到奖励");	
		$sign_allpoint += $sign_point_shouci;
	}
	//连续签到奖励
	foreach ($lianxu_arr as $v) {
		if ($lx_day == $v['day']) {
			add_pointlog($user['user_id'], 'add', $v['point'], "【".date('Y-m')."】连续签到{$v['day']}天奖励");
			$sign_allpoint += $v['point'];
		}
	}
	//累计签到奖励
	foreach ($leiji_arr as $v) {
		if ($lj_day == $v['day']) {
			add_pointlog($user['user_id'], 'add', $v['point'], "【".date('Y-m')."】累计签到{$v['day']}天奖励");
			$sign_allpoint += $v['point'];
		}
	}
	$db->pe_update('signlog', array('signlog_id'=>$signlog_id), array('signlog_point'=>$sign_allpoint));
	return intval($sign_allpoint);
}

function check_shouci() {
    global $db, $user;
    return $db->pe_num('signlog', array('user_id'=>$user['user_id']));
}
?>