<?php
$menumark = 'order';
pe_lead('hook/refund.hook.php');
switch($act) {
	//####################// 订单详情 //####################//
	case 'view':
		$order_id = pe_dbhold($_g_id);
		$info = $db->pe_select('order', array('order_id'=>$order_id, 'user_id'=>$user['user_id']));
		if (!$info['order_id']) pe_error('参数错误...');
		$info['order_stateshow'] = order_stateshow($info['order_state'], 'html');
		$info['order_adate'] = pe_date($info['order_atime']);
		$info['order_pdate'] = pe_date($info['order_ptime']);		
		$info['order_sdate'] = pe_date($info['order_stime']);		
		$info['order_fdate'] = pe_date($info['order_ftime']);
		$product_list = $db->pe_selectall('orderdata', array('order_id'=>$order_id));
		foreach ($product_list as $kk=>$vv) {
			$product_list[$kk]['product_logo'] = pe_thumb($vv['product_logo'], 400, 400);
			$product_list[$kk]['product_rule'] = order_skushow($vv['product_rule']);
			$product_list[$kk]['refund_stateshow'] = refund_stateshow($vv['refund_state'], 'html');
		}
		$info['product_list'] = $product_list;
		$prokey_list = $db->pe_selectall('prokey', array('order_id'=>$order_id, 'order by'=>'prokey_id asc'));
		$info['prokey_list'] = $prokey_list;
		pe_fixurl(pe_url("/page/user/order?id={$order_id}", 'app'));
		$seo = pe_seo($menutitle='订单详情');
		include(pe_tpl('order.html'));
	break;
	//####################// 订单支付 //####################//
	case 'pay':
		$order_id = pe_dbhold($_g_id);
		$info = $db->pe_select('order', array('order_id'=>$order_id, 'user_id'=>$user['user_id']));
		//生成付款单
		$order['pay_type'] = 'order';	
		$order['order_id'] = $info['order_id'];
		$order['order_name'] = $info['order_name'];
		$order['order_money'] = $info['order_money'];
		$order['order_payment'] = $info['order_payment'];
		$pay_id = pay_add($user, $order);
		if (!$pay_id) pe_apidata(array('code'=>0, 'msg'=>'系统异常，请重试'));	
		pe_apidata(array('code'=>1, 'data'=>array('id'=>$pay_id)));
	break;
	//####################// 订单取消 //####################//
	case 'close':
		$order_id = pe_dbhold($_g_id);
		$info = $db->pe_select('order', array('order_id'=>$order_id, 'user_id'=>$user['user_id']));
		if (isset($_p_pesubmit)) {
			if (!$info['order_id']) pe_apidata(array('code'=>0, 'msg'=>'参数错误'));
			if ($info['order_state'] != 'wpay') pe_apidata(array('code'=>0, 'msg'=>'订单不可取消'));
			if (!$_p_order_closetext) pe_apidata(array('code'=>0, 'msg'=>'请填写取消原因'));
			if (order_close_callback($info, $_p_order_closetext)) {
				pe_apidata(array('code'=>1, 'msg'=>'操作成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'操作失败'));
			}
		}
		if (!$info['order_id']) pe_error('参数错误', '', 'dialog');
		pe_fixurl(pe_url("/page/user/order_close?id={$order_id}", 'app'));
		$seo = pe_seo($menutitle='订单取消');
		include(pe_tpl('order_close.html'));
	break;
	//####################// 订单确认收货 //####################//
	case 'success':
		$order_id = pe_dbhold($_g_id);
		$info = $db->pe_select('order', array('order_id'=>$order_id, 'user_id'=>$user['user_id']));
		if (!$info['order_id']) pe_apidata(array('code'=>0, 'msg'=>'参数错误'));
		if ($info['order_state'] != 'wget') pe_apidata(array('code'=>0, 'msg'=>'未发货订单不能确认'));
		if (in_array($info['order_payment'], array('alipay_db', 'cod'))) pe_apidata(array('code'=>0, 'msg'=>'参数错误'));
		if (order_success_callback($info)) {
			pe_apidata(array('code'=>1, 'msg'=>'操作成功'));
		}
		else {
			pe_apidata(array('code'=>0, 'msg'=>'操作失败'));
		}
	break;
	//####################// 订单评价 //####################//
	case 'comment':
		$order_id = pe_dbhold($_g_id);
		$info = $db->pe_select('order', array('order_id'=>$order_id, 'user_id'=>$user['user_id']));
		$list = $db->pe_selectall('orderdata', array('order_id'=>$order_id));
		if (isset($_p_pesubmit)) {
			//pe_token_match();
			if (!$info['order_id']) pe_apidata(array('code'=>0, 'msg'=>'参数错误'));
			if ($info['order_state'] != 'success') pe_apidata(array('code'=>0, 'msg'=>'订单未完成'));
			if ($info['order_comment']) pe_apidata(array('code'=>0, 'msg'=>'请勿重复评价'));
			foreach ($list as $k=>$v) {
				$sql_set[$k]['comment_star'] = intval($_p_comment_star[$v['orderdata_id']]);
				$sql_set[$k]['comment_text'] = $_p_comment_text[$v['orderdata_id']];
				$sql_set[$k]['comment_logo'] = is_array($_p_comment_logo[$v['orderdata_id']]) ? implode(',', array_filter($_p_comment_logo[$v['orderdata_id']])) : '';	
				$sql_set[$k]['comment_atime']= time();
				$sql_set[$k]['product_id'] = $v['product_id'];
				$sql_set[$k]['product_name'] = $v['product_name'];
				$sql_set[$k]['product_logo'] = $v['product_logo'];
				$sql_set[$k]['order_id'] = $order_id;
				$sql_set[$k]['user_id'] = $user['user_id'];
				$sql_set[$k]['user_name'] = $user['user_name'];
				$sql_set[$k]['user_logo'] = $user['user_logo'];
				$sql_set[$k]['user_ip'] = pe_ip();
				if (!$sql_set[$k]['comment_star']) pe_apidata(array('code'=>0, 'msg'=>'请选择评分'));
				if (!$sql_set[$k]['comment_text']) pe_apidata(array('code'=>0, 'msg'=>'请填写评价内容'));
			}
			if ($db->pe_insert('comment', pe_dbhold($sql_set))) {
				order_comment_callback($info);
				pe_apidata(array('code'=>1, 'msg'=>'评价成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'评价失败'));
			}
		}
		foreach ($list as $k=>$v) {
			$list[$k]['product_logo'] = pe_thumb($v['product_logo'], 400, 400);
			$list[$k]['product_rule'] = order_skushow($v['product_rule']);
			$list[$k]['comment_star'] = 0;
			$list[$k]['comment_logo'] = array();
		}
		pe_fixurl(pe_url("/page/user/order_comment?id={$order_id}", 'app'));
		$seo = pe_seo($menutitle='订单评价', '', '', 'user');
		include(pe_tpl('order_comment.html'));
	break;
	//####################// 查看快递 //####################//
	case 'kuaidi':
		$order_id = pe_dbhold($_g_id);
		$sql_field = "order_id,order_wl_id,order_wl_name";
		$info = $db->pe_select('order', array('order_id'=>$order_id), $sql_field);
		$info['list'] = order_kuaidi($info['order_wl_id'], $info['order_wl_name']);
		pe_fixurl(pe_url("/page/user/order_kuaidi?id={$order_id}", 'app'));
		$seo = pe_seo($menutitle='快递信息', '', '', 'user');
		include(pe_tpl('order_kuaidi.html'));
	break;
	//####################// 订单列表 //####################//
	default:
		if (in_array($_g_state, array('wpay', 'wsend', 'wget', 'success'))) {
			$sql_where .= " and `order_state` = '".pe_dbhold($_g_state)."'";	
		}
		elseif ($_g_state == 'wpj') {
			$sql_where .= " and `order_state` = 'success' and `order_comment` = 0";			
		}
		$sql_where .= " and `user_id` = '{$user['user_id']}' order by `order_id` desc";
		$list = $db->pe_selectall('order', $sql_where, '*', array(20, $_g_page));
		foreach ($list as $k=>$v) {
			$list[$k]['order_atime'] = pe_date($v['order_atime']);
			$list[$k]['order_stateshow'] = order_stateshow($v['order_state'], 'html');
			$product_list = $db->pe_selectall('orderdata', array('order_id'=>$v['order_id']));
			foreach ($product_list as $kk=>$vv) {
				$product_list[$kk]['product_logo'] = pe_thumb($vv['product_logo'], 400, 400);
				$product_list[$kk]['product_rule'] = order_skushow($vv['product_rule']);
				$product_list[$kk]['refund_stateshow'] = refund_stateshow($vv['refund_state'], 'html');
			}
			$list[$k]['product_list'] = $product_list;
		}
		//统计订单数量
		$tongji_arr = $db->index('order_state')->pe_selectall('order', array('user_id'=>$user['user_id'], 'group by'=>'order_state'), '`order_state`, count(1) as `num`');
		foreach ($ini['order_state'] as $k=>$v) {
			$tongji[$k] = intval($tongji_arr[$k]['num']);
			$tongji['all'] += intval($tongji[$k]);
		}
		$tongji['wpj'] = $db->pe_num('order', array('user_id'=>$user['user_id'], 'order_state'=>'success', 'order_comment'=>0));
		pe_fixurl(pe_url("/page/user/order_list?state={$_g_state}", 'app'));
		$seo = pe_seo($menutitle='我的订单', '', '', 'user');
		include(pe_tpl('order_list.html'));
	break;
}
?>