<?php
/**
 * @copyright   2008-2014 简好网络 <http://www.phpshe.com>
 * @creatdate   2016-0101 koyshe <koyshe@gmail.com>
 */
$menumark = 'userbank';
switch ($act) {
	//####################// 账户增加 //####################//
	case 'add':
		$userbank_typelist[] = array('value'=>'', 'name'=>'请选择');
		foreach ($ini['userbank_type'] as $k=>$v) {
			if ($k == 'wechat') continue;
			$userbank_typelist[] = array('value'=>$k, 'name'=>$v);
		}
		if (isset($_p_pesubmit)) {
			pe_token_match();
			$userbank_index = intval($_p_userbank_index);
			//if (!in_array($userbank_type, array('alipay', 'wechat')) && !$_p_userbank_address) pe_apidata(array('code'=>0, 'msg'=>'请填写开户行'));
			if (!$userbank_typelist[$userbank_index]['value']) pe_apidata(array('code'=>0, 'msg'=>'请选择收款账户'));
			if (!$_p_userbank_num) pe_apidata(array('code'=>0, 'msg'=>'请填写帐号'));
			if (!$_p_userbank_tname) pe_apidata(array('code'=>0, 'msg'=>'请填写收款人'));
			$sql_set['userbank_type'] = $userbank_typelist[$userbank_index]['value'];
			$sql_set['userbank_name'] = $userbank_typelist[$userbank_index]['name'];
			$sql_set['userbank_address'] = $_p_userbank_address;
			$sql_set['userbank_tname'] = $_p_userbank_tname;
			$sql_set['userbank_num'] = $_p_userbank_num;
			$sql_set['userbank_atime'] = time();
			$sql_set['user_id'] = $user['user_id'];
			$sql_set['user_name'] = $user['user_name'];
			if ($db->pe_insert('userbank', pe_dbhold($sql_set))) {
				pe_apidata(array('code'=>1, 'msg'=>'添加成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'添加失败'));
			}
		}
		$info['userbank_typelist'] = $userbank_typelist;
		$info['userbank_index'] = 0;
		pe_fixurl(pe_url("/page/user/userbank_add", 'app'));
		$seo = pe_seo($menutitle='新增账户');
		include(pe_tpl('userbank_add.html'));
	break;
	//####################// 账户删除 //####################//
	case 'del':
		pe_token_match();
		$userbank_id = intval($_g_id);
		if ($db->pe_delete('userbank', array('userbank_id'=>$userbank_id, 'user_id'=>$user['user_id']))) {
			pe_apidata(array('code'=>1, 'msg'=>'删除成功'));
		}
		else {
			pe_apidata(array('code'=>0, 'msg'=>'删除失败'));
		}
	break;
	//####################// 账户列表 //####################//
	default:
		$list = $db->pe_selectall('userbank', array('user_id'=>$user['user_id'], 'order by'=>'userbank_id desc'));
		foreach ($list as $k=>$v) {
			$list[$k]['userbank_num'] = userbank_num($v['userbank_num']);
		}
		$tongji['all'] = $db->pe_num('userbank', array('user_id'=>$user['user_id']));
		pe_fixurl(pe_url("/page/user/userbank_list", 'app'));
		$seo = pe_seo($menutitle='收款账户');
		include(pe_tpl('userbank_list.html'));
	break;
}
?>