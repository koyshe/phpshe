<?php
switch($act) {
	//####################// 修改头像 //####################//
	case 'logo':
		$menumark = 'setting_logo';
		if (isset($_p_pesubmit)) {
			pe_token_match();
			if ($db->pe_update('user', array('user_id'=>$user['user_id']), array('user_logo'=>pe_dbhold($_p_user_logo)))) {
				pe_apidata(array('code'=>1, 'msg'=>'修改成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'修改失败'));
			}
		}
		$info['user_logo'] = $user['user_logo'] ? pe_thumb($user['user_logo'], 100, 100, 'avatar') : '';
		pe_fixurl(pe_url("/page/user/setting_base", 'app'));
		$seo = pe_seo($menutitle='修改头像');
		include(pe_tpl('setting_logo.html'));
	break;
	//####################// 登录密码修改 //####################//
	case 'pw':
		$menumark = 'setting_pw';
		if (isset($_p_pesubmit)) {
			pe_token_match();
			if ($user['user_pw'] && !$db->pe_num('user', array('user_id'=>$user['user_id'], 'user_pw'=>md5($_p_user_oldpw)))) pe_apidata(array('code'=>0, 'msg'=>'当前密码错误'));
			if (strlen($_p_user_pw) < 6 or strlen($_p_user_pw) > 20) pe_apidata(array('code'=>0, 'msg'=>'新密码为6-20位字符'));
			if ($_p_user_pw != $_p_user_pw2) pe_apidata(array('code'=>0, 'msg'=>'新密码与确认密码不一致'));
			if ($db->pe_update('user', array('user_id'=>$user['user_id']), array('user_pw'=>md5($_p_user_pw)))) {
				pe_apidata(array('code'=>1, 'msg'=>'修改成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'修改失败'));
			}
		}
		$info['user_pw'] = $user['user_pw'] ? true : false;
		pe_fixurl(pe_url("/page/user/setting_pw", 'app'));
		$seo = pe_seo($menutitle='修改登录密码');
		include(pe_tpl('setting_pw.html'));
	break;
	//####################// 支付密码修改 //####################//
	case 'paypw':
		$menumark = 'setting_pw';
		if (isset($_p_pesubmit)) {
			pe_token_match();
			if ($user['user_paypw'] && !$db->pe_num('user', array('user_id'=>$user['user_id'], 'user_paypw'=>md5($_p_user_oldpw)))) pe_apidata(array('code'=>0, 'msg'=>'当前密码错误'));
			if (strlen($_p_user_pw) < 6 or strlen($_p_user_pw) > 20) pe_apidata(array('code'=>0, 'msg'=>'新密码为6-20位字符'));
			if ($_p_user_pw != $_p_user_pw1) pe_apidata(array('code'=>0, 'msg'=>'新密码与确认密码不一致'));
			if ($db->pe_update('user', array('user_id'=>$user['user_id']), array('user_paypw'=>md5($_p_user_pw)))) {
				pe_apidata(array('code'=>1, 'msg'=>'修改成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'修改失败'));
			}
		}
		$info['user_paypw'] = $user['user_paypw'] ? true : false;
		pe_fixurl(pe_url("/page/user/setting_paypw", 'app'));
		$seo = pe_seo($menutitle='修改支付密码');
		include(pe_tpl('setting_paypw.html'));
	break;
	//####################// 个人信息 //####################//
	default:
		$menumark = 'setting_base';
		if (isset($_p_pesubmit)) {
			pe_token_match();
			if ($_p_user_phone) {
				if (!pe_formcheck('phone', $_p_user_phone)) pe_apidata(array('code'=>0, 'msg'=>'请填写正确的手机号'));
				if ($db->pe_num('user', " and `user_phone` = '".pe_dbhold($_p_user_phone)."' and `user_id` != '{$user['user_id']}'")) pe_apidata(array('code'=>0, 'msg'=>'手机号已存在'));
			}
			if ($_p_user_email) {
				if (!pe_formcheck('email', $_p_user_email)) pe_apidata(array('code'=>0, 'msg'=>'请填写正确的邮箱'));
				if ($db->pe_num('user', " and `user_email` = '".pe_dbhold($_p_user_email)."' and `user_id` != '{$user['user_id']}'")) pe_apidata(array('code'=>0, 'msg'=>'邮箱已存在'));
			}
			$sql_set['user_tname'] = $_p_user_tname;
			$sql_set['user_phone'] = $_p_user_phone;
			$sql_set['user_email'] = $_p_user_email;
			if ($db->pe_update('user', array('user_id'=>$user['user_id']), pe_dbhold($sql_set))) {
				pe_apidata(array('code'=>1, 'msg'=>'修改成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'修改失败'));
			}
		}
		$info['user_name'] = $user['user_name'];
		$info['user_logo'] = pe_thumb($user['user_logo'], 100, 100, 'avatar');
		$info['userlevel_name'] = $cache_userlevel[$user['userlevel_id']]['userlevel_name'];
		$info['user_tname'] = $user['user_tname'];	
		$info['user_phone'] = $user['user_phone'];
		$info['user_email'] = $user['user_email'];	
		$info['user_adate'] = pe_date($user['user_atime']);
		pe_fixurl(pe_url("/page/user/setting_base", 'app'));
		$seo = pe_seo($menutitle='帐号设置');
		include(pe_tpl('setting_base.html'));
	break;
}
?>