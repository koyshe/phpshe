<?php
$menumark = 'collect';
switch($act) {
	//####################// 收藏删除 //####################//
	case 'del':
		pe_token_match();
		$collect_id = intval($_g_id);
		$info = $db->pe_select('collect', array('collect_id'=>$collect_id, 'user_id'=>$user['user_id']));
		if ($db->pe_delete('collect', array('collect_id'=>$info['collect_id']))) {
			product_jsnum($info['product_id'], 'collectnum');
			pe_apidata(array('code'=>1, 'msg'=>'删除成功'));
		}
		else {
			pe_apidata(array('code'=>0, 'msg'=>'删除失败'));
		}
	break;
	//####################// 收藏列表 //####################//
	default:
		$list = $db->pe_selectall('collect', array('user_id'=>$user['user_id'], 'order by'=>'collect_id desc'), '*', array(20, $_g_page));
		foreach ($list as $k=>$v) {
			$list[$k]['collect_atime'] = pe_date($v['collect_atime'], 'Y-m-d');
			$list[$k]['product_logo'] = pe_thumb($v['product_logo'], 400, 400);
		}
		$tongji['all'] = $db->pe_num('collect', array('user_id'=>$user['user_id']));
		pe_fixurl(pe_url("/page/user/collect_list", 'app'));
		$seo = pe_seo($menutitle='我的收藏');
		include(pe_tpl('collect_list.html'));
	break;
}
?>