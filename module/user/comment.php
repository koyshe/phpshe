<?php
$menumark = 'comment';
switch($act) {
	//####################// 评价列表 //####################//
	default:
		$list = $db->pe_selectall('comment', array('user_id'=>$user['user_id'], 'order by'=>'comment_id desc'), '*', array(20, $_g_page));
		foreach ($list as $k=>$v) {
			$list[$k]['comment_star'] = pe_comment($v['comment_star']);
			$list[$k]['comment_atime'] = pe_date($v['comment_atime']);
			$list[$k]['comment_reply_time'] = pe_date($v['comment_reply_time']);
			$list[$k]['product_logo'] = pe_thumb($v['product_logo'], 100, 100);
			$list[$k]['user_logo'] = pe_thumb($v['user_logo'], 100, 100, 'avatar');
			//评价晒图
			$comment_logo = $v['comment_logo'] ? explode(',', $v['comment_logo']) : array();
			foreach ($comment_logo as $kk=>$vv) {
				$comment_logo[$kk] = pe_thumb($vv);
			}
			$list[$k]['comment_logo'] = $comment_logo;
		}
		$tongji['all'] = $db->pe_num('comment', array('user_id'=>$user['user_id']));	
		pe_fixurl(pe_url("/page/user/comment_list", 'app'));
		$seo = pe_seo($menutitle='我的评价');
		include(pe_tpl('comment_list.html'));
	break;
}
?>