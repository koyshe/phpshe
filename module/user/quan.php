<?php
/**
 * @copyright   2008-2014 简好网络 <http://www.phpshe.com>
 * @creatdate   2012-0501 koyshe <koyshe@gmail.com>
 */
$menumark = 'quan';
switch($act) {
	//####################// 优惠券兑换 //####################//
	case 'duihuan':
		//pe_token_match();
		$quan_code = pe_dbhold($_g_value);
		if (!$quan_code) pe_apidata(array('code'=>0, 'msg'=>'请输入口令'));		
		$info = $db->pe_select('quan', array('quan_type'=>'offline', 'quan_code'=>$quan_code));
		if (!$info['quan_id']) pe_apidata(array('code'=>0, 'msg'=>'口令无效'));
		if (!$info['quan_state']) pe_apidata(array('code'=>0, 'msg'=>'口令已过期'));
		$quanlog_num = $db->pe_num('quanlog', array('quan_id'=>$info['quan_id'], 'user_id'=>$user['user_id']));
		if ($info['quan_limitnum'] && $info['quan_limitnum'] <= $quanlog_num) pe_apidata(array('code'=>0, 'msg'=>'请勿重复兑换'));
		if (add_quanlog($user, $info['quan_id'])) {
			pe_apidata(array('code'=>1, 'msg'=>'兑换成功'));
		}
		else {
			pe_apidata(array('code'=>0, 'msg'=>'兑换失败'));	
		}
	break;
	//####################// 优惠券列表 //####################//
	default:
		user_quancheck();
		$list = $db->pe_selectall('quanlog', array('user_id'=>$user['user_id'], 'quanlog_state'=>intval($_g_state), 'order by'=>'quanlog_id desc'), '*', array(20, $_g_page));
		
		$tongji_arr = $db->index('quanlog_state')->pe_selectall('quanlog', array('user_id'=>$user['user_id'], 'group by'=>'quanlog_state'), '`quanlog_state`, count(1) as `num`');
		foreach ($ini['quanlog_state'] as $k=>$v) {
			$tongji[$k] = intval($tongji_arr[$k]['num']);
			$tongji['all'] += intval($tongji[$k]);
		}
		pe_fixurl(pe_url("/page/user/quan_list", 'app'));
		$seo = pe_seo($menutitle='我的优惠券');
		include(pe_tpl('quan_list.html'));
	break;
}
?>