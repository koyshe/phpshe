<?php
switch($act) {
	//####################// 用户登录 //####################//
	case 'login':
		if (isset($_p_pesubmit)) {
			$user_name = pe_dbhold($_p_user_name);
			$user_pw = md5($_p_user_pw);
			if ($info = $db->pe_select('user', " and (user_name = '{$user_name}' or user_phone = '{$user_name}' or user_email = '{$user_name}') and user_pw = '{$user_pw}'")) {
				user_login_callback('login', $info);
				pe_apidata(array('code'=>1, 'msg'=>'登录成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'帐号或密码错误'));
			}
		}
		pe_fixurl(pe_url("/page/user/do_login", 'app'));
		$seo = pe_seo($menutitle='用户登录');
 		include(pe_tpl('do_login.html'));
	break;
	//####################// 第三方登录返回 //####################//
	case 'login3':
		$login3 = pe_get_session('login3');
		if ($login3['from'] == 'qq') {
			$sqlset_openid['user_qq_unionid'] = $user_qq_unionid = pe_dbhold($login3['user_qq_unionid']);
			$sqlset_openid['user_qq_openid'] = $user_qq_openid = pe_dbhold($login3['user_qq_openid']);			
			$info = $db->pe_select('user_openid', array('user_qq_unionid'=>$user_qq_unionid));	
		}
		elseif ($login3['from'] == 'wx') {
			$sqlset_openid['user_wx_unionid'] = $user_wx_unionid = pe_dbhold($login3['user_wx_unionid']);
			$sqlset_openid['user_wx_openid'] = $user_wx_openid = pe_dbhold($login3['user_wx_openid']);			
			$info = $db->pe_select('user_openid', array('user_wx_unionid'=>$user_wx_unionid));		
		}
		if (isset($_p_pesubmit)) {
			if (!is_array($login3)) pe_apidata(array('code'=>0, 'msg'=>'参数错误'));
			if ($_p_bind) {
				//绑定并登录
				$user_name = pe_dbhold($_p_user_name);
				$user_pw = md5($_p_user_pw);
				$user = $db->pe_select('user', " and (user_name = '{$user_name}' or user_phone = '{$user_name}' or user_email = '{$user_name}')");
				if (!$user['user_id']) pe_apidata(array('code'=>0, 'msg'=>'帐号不存在'));
				$user = $db->pe_select('user', " and (user_name = '{$user_name}' or user_phone = '{$user_name}' or user_email = '{$user_name}') and user_pw = '{$user_pw}'");
				if (!$user['user_id']) pe_apidata(array('code'=>0, 'msg'=>'密码错误'));
				user_login_callback('login', $user);
			}
			else {
				//直接注册账号
				$sqlset_user['user_name'] = user_jsname($login3['user_name']);
				$sqlset_user['user_logo'] = $login3['user_logo'];
				$sqlset_user['user_phone'] = $login3['user_phone'];
				$sqlset_user['user_ip'] = pe_ip();
				$sqlset_user['user_atime'] = time();
				$user_id = $db->pe_insert('user', pe_dbhold($sqlset_user, array('user_logo')));
				$user = $db->pe_select('user', array('user_id'=>$user_id));
				if (!$user['user_id']) pe_apidata(array('code'=>0, 'msg'=>'注册失败，请重试'));
				user_login_callback('reg', $user);			
			}
			update_user_openid();
			pe_del_session('login3');
			pe_apidata(array('code'=>1, 'msg'=>'登录成功', 'url'=>$login3['jump']));
		}
		$user = $db->pe_select('user', array('user_id'=>$info['user_id']));
		if ($user['user_id'] && is_array($login3)) {
			user_login_callback('login', $user);
			update_user_openid();
			pe_del_session('login3');
			pe_success('登录成功！', $login3['jump']);		
		}
		pe_fixurl(pe_url("/page/user/do_login3", 'app'));
		$seo = pe_seo($menutitle='绑定手机号');
	 	include(pe_tpl('do_login3.html'));
	break;
	//####################// 用户退出 //####################//
	case 'logout':
		unset($_SESSION['user_idtoken'], $_SESSION['user_id'], $_SESSION['user_name'], $_SESSION['user_ltime']);
		pe_success('退出成功！', $pe['host']);
	break;
	//####################// 重置密码 //####################//
	case 'getpw':
		if (isset($_p_pesubmit)) {
			$user_name = pe_dbhold($_p_user_name);
			if (!$user_name) pe_apidata(array('code'=>0, 'msg'=>'请填写帐号'));
			if (stripos($user_name, '@') === false) {
				$info = $db->pe_select('user', array('user_phone'=>$user_name), 'user_id');
			}
			else {
				$info = $db->pe_select('user', array('user_email'=>$user_name), 'user_id');
			}
			if (!$info['user_id']) pe_apidata(array('code'=>0, 'msg'=>'帐号不存在'));
			pe_lead('hook/yzmlog.hook.php');
			if (!$_p_yzm or !check_yzm($user_name, $_p_yzm)) pe_apidata(array('code'=>0, 'msg'=>'验证码错误'));
			if (strlen($_p_user_pw) < 6 or strlen($_p_user_pw) > 20) pe_apidata(array('code'=>0, 'msg'=>'新密码为6-20位字符'));
			if ($_p_user_pw != $_p_user_pw2) pe_apidata(array('code'=>0, 'msg'=>'两次密码不一致'));
			if ($db->pe_update('user', array('user_id'=>$info['user_id']), array('user_pw'=>md5($_p_user_pw)))) {
				pe_apidata(array('code'=>1, 'msg'=>'密码重置成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'密码重置失败'));
			}
		}
		pe_fixurl(pe_url("/page/user/do_getpw", 'app'));
		$seo = pe_seo($menutitle='重置密码');
 		include(pe_tpl('do_getpw.html'));
	break;
	//####################// 用户注册 //####################//
	case 'register':
		if (isset($_p_pesubmit)) {
			//if (mb_strlen($_p_user_name, 'utf8') < 5 or mb_strlen($_p_user_name, 'utf8') > 15) pe_apidata(array('code'=>0, 'msg'=>'用户名为5-15位字符'));
			//if (!pe_formcheck('uname', $_p_user_name)) pe_apidata(array('code'=>0, 'msg'=>'用户名有特殊字符'));				
			//if ($db->pe_num('user', array('user_name'=>pe_dbhold($_p_user_name)))) pe_apidata(array('code'=>0, 'msg'=>'用户名已存在'));
			if (!$_p_authcode || strtolower($_s_authcode) != strtolower($_p_authcode)) pe_apidata(array('code'=>0, 'msg'=>'图片验证码错误'));
			if ($_p_reg_type == 'phone') {
				if (!pe_formcheck('phone', $_p_user_phone)) pe_apidata(array('code'=>0, 'msg'=>'请填写正确的手机号'));
				if ($db->pe_num('user', array('user_phone'=>pe_dbhold($_p_user_phone)))) pe_apidata(array('code'=>0, 'msg'=>'手机号已存在'));			
				pe_lead('hook/yzmlog.hook.php');
				if ($cache_setting['web_checkphone'] && !check_yzm($_p_user_phone, $_p_phone_yzm)) pe_apidata(array('code'=>0, 'msg'=>'短信验证码错误'));			
				$sql_set['user_name'] = user_jsname($_p_user_phone);
			}
			else {
				if (!pe_formcheck('email', $_p_user_email)) pe_apidata(array('code'=>0, 'msg'=>'请填写正确的邮箱'));
				if ($db->pe_num('user', array('user_email'=>pe_dbhold($_p_user_email)))) pe_apidata(array('code'=>0, 'msg'=>'邮箱已存在'));			
				pe_lead('hook/yzmlog.hook.php');
				if ($cache_setting['web_checkemail'] && !check_yzm($_p_user_email, $_p_email_yzm)) pe_apidata(array('code'=>0, 'msg'=>'邮箱验证码错误'));
				$sql_set['user_name'] = user_jsname($_p_user_email);
			}
			if (strlen($_p_user_pw) < 6 or strlen($_p_user_pw) > 20) pe_apidata(array('code'=>0, 'msg'=>'密码为6-20位字符'));
			if ($_p_user_pw1 && $_p_user_pw != $_p_user_pw1) pe_apidata(array('code'=>0, 'msg'=>'两次密码不一致'));
			$sql_set['user_pw'] = md5($_p_user_pw);
			$sql_set['user_phone'] = $_p_user_phone;
			$sql_set['user_email'] = $_p_user_email;
			$sql_set['user_ip'] = pe_ip();
			$sql_set['user_atime'] = $sql_set['user_ltime'] = time();
			if ($user_id = $db->pe_insert('user', pe_dbhold($sql_set))) {
				user_login_callback('reg', $user_id);
				if ($_p_reg_type == 'email') {
					update_yzm($_p_user_email, $_p_email_yzm);
				}
				else {
					update_yzm($_p_user_phone, $_p_phone_yzm);				
				}
				pe_apidata(array('code'=>1, 'msg'=>'注册成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'注册失败'));
			}
		}
		pe_fixurl(pe_url("/page/user/do_login", 'app'));
		$seo = pe_seo($menutitle='用户注册');
 		include(pe_tpl('do_register.html'));
	break;
}

function update_user_openid() {
	global $db, $user, $sqlset_openid;
	//取消已绑定三方登录信息
	foreach ($sqlset_openid as $k=>$v) {
		$db->pe_update('user_openid', array($k=>$v), array($k=>''));
	}
	//重新绑定三方登录信息
	$openid = $db->pe_select('user_openid', array('user_id'=>$user['user_id']));
	if ($openid['user_id']) {
		$db->pe_update('user_openid', array('user_id'=>$user['user_id']), pe_dbhold($sqlset_openid));
	}
	else {
		$sqlset_openid['user_id'] = $user['user_id'];
		$db->pe_insert('user_openid', pe_dbhold($sqlset_openid));
	}
}
?>