<?php
$menumark = 'useraddr';
switch($act) {
	//####################// 添加地址 //####################//
	case 'add':
		if (isset($_p_pesubmit)) {
			pe_token_match();
			if (!$_p_user_tname) pe_apidata(array('code'=>0, 'msg'=>'请填写姓名'));
			if (!pe_formcheck('phone', $_p_user_phone)) pe_apidata(array('code'=>0, 'msg'=>'手机号有误'));
			if (!$_p_address_province) pe_apidata(array('code'=>0, 'msg'=>'请选择省份'));
			if (!$_p_address_city) pe_apidata(array('code'=>0, 'msg'=>'请选择城市'));
			if (!$_p_address_area) pe_apidata(array('code'=>0, 'msg'=>'请选择区县'));
			if (!$_p_address_text) pe_apidata(array('code'=>0, 'msg'=>'请填写详细地址'));
			$sql_set['address_province'] = $_p_address_province;
			$sql_set['address_city'] = $_p_address_city;
			$sql_set['address_area'] = $_p_address_area;
			$sql_set['address_text'] = $_p_address_text;
			$sql_set['address_default'] = intval($_p_address_default);
			$sql_set['address_atime'] = time();
			$sql_set['user_id'] = $user['user_id'];
			$sql_set['user_name'] = $user['user_name'];
			$sql_set['user_tname'] = $_p_user_tname;
			$sql_set['user_phone'] = $_p_user_phone;					
			if ($_p_address_default == 1) {
				$db->pe_update('useraddr', array('user_id'=>$user['user_id']), array('address_default'=>0));
			}	
			if ($db->pe_insert('useraddr', pe_dbhold($sql_set))) {
				pe_apidata(array('code'=>1, 'msg'=>'添加成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'添加失败'));
			}
		}
		pe_fixurl(pe_url("/page/user/useraddr_add", 'app'));
		$seo = pe_seo($menutitle='新增地址');
		include(pe_tpl('useraddr_add.html'));
	break;
	//####################// 修改地址 //####################//
	case 'edit':
		$address_id = intval($_g_id);
		$info = $db->pe_select('useraddr', array('address_id'=>$address_id, 'user_id'=>$user['user_id']));
		if (isset($_p_pesubmit)) {
			pe_token_match();
			if (!$info['address_id']) pe_apidata(array('code'=>0, 'msg'=>'参数错误'));
			if (!$_p_user_tname) pe_apidata(array('code'=>0, 'msg'=>'请填写姓名'));
			if (!pe_formcheck('phone', $_p_user_phone)) pe_apidata(array('code'=>0, 'msg'=>'手机号有误'));
			if (!$_p_address_province) pe_apidata(array('code'=>0, 'msg'=>'请选择省份'));
			if (!$_p_address_city) pe_apidata(array('code'=>0, 'msg'=>'请选择城市'));
			if (!$_p_address_area) pe_apidata(array('code'=>0, 'msg'=>'请选择区县'));
			if (!$_p_address_text) pe_apidata(array('code'=>0, 'msg'=>'请填写详细地址'));
			$sql_set['address_province'] = $_p_address_province;
			$sql_set['address_city'] = $_p_address_city;
			$sql_set['address_area'] = $_p_address_area;
			$sql_set['address_text'] = $_p_address_text;
			$sql_set['address_default'] = intval($_p_address_default);
			$sql_set['user_tname'] = $_p_user_tname;
			$sql_set['user_phone'] = $_p_user_phone;
			if ($_p_address_default == 1) {
				$db->pe_update('useraddr', array('user_id'=>$user['user_id']), array('address_default'=>0));
			}			
			if ($db->pe_update('useraddr', array('address_id'=>$address_id), pe_dbhold($sql_set))) {
				pe_apidata(array('code'=>1, 'msg'=>'修改成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'修改失败'));
			}
		}
		pe_fixurl(pe_url("/page/user/useraddr_edit?id={$_g_id}", 'app'));
		$seo = pe_seo($menutitle='修改地址');
		include(pe_tpl('useraddr_add.html'));
	break;
	//####################// 地址删除 //####################//
	case 'del':
		pe_token_match();
		$address_id = intval($_g_id);
		if ($db->pe_delete('useraddr', array('address_id'=>$address_id, 'user_id'=>$user['user_id']))) {
			pe_apidata(array('code'=>1, 'msg'=>'删除成功'));
		}
		else {
			pe_apidata(array('code'=>0, 'msg'=>'删除失败'));
		}
	break;
	//####################// 地址列表 //####################//
	default:
		$list = $db->pe_selectall('useraddr', array('user_id'=>$user['user_id'], 'order by'=>'address_default desc, address_id desc'));

		$tongji['all'] = $db->pe_num('useraddr', array('user_id'=>$user['user_id']));					
		pe_fixurl(pe_url("/page/user/useraddr_list", 'app'));
		$seo = pe_seo($menutitle='收货地址');
		include(pe_tpl('useraddr_list.html'));
	break;
}
?>