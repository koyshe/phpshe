<?php
/**
 * @copyright   2008-2014 简好网络 <http://www.phpshe.com>
 * @creatdate   2016-0101 koyshe <koyshe@gmail.com>
 */
switch ($act) {
	//####################// 提现申请 //####################//
	case 'add':
		$menumark = 'cashout_add';
		$info['user_money'] = $user['user_money'];
		$info['cashout_fee_bili'] = pe_num($cache_setting['cashout_fee'], 'round', 4);
		$cashout_date = $cache_setting['cashout_date'] ? explode(',', $cache_setting['cashout_date']) : array();
		if (count($cashout_date) && !in_array(date('j'), $cashout_date)) pe_apidata(array('code'=>'date_error', 'msg'=>"每月 {$cache_setting['cashout_date']} 号可以提现"));
		$userbank_list = $db->index('userbank_id')->pe_selectall('userbank', array('user_id'=>$user['user_id'], 'order by'=>'userbank_id desc'));
		if ($user['user_wx_openid']) {
			$userbank_list = array(array('userbank_id'=>0, 'userbank_name'=>'微信零钱', 'userbank_type'=>'wechat', 'userbank_num'=>$user['user_name'])) + $userbank_list;		
		}
		if (isset($_p_pesubmit)) {
			//pe_token_match();
			$cashout_money = pe_num($_p_cashout_money, 'floor', 2); 
			$cashout_fee = pe_num($cashout_money * $info['cashout_fee_bili'], 'floor', 2);
			$cashout_jsmoney = pe_num($cashout_money - $cashout_fee, 'floor', 2);
			$userbank = $userbank_list[intval($_p_userbank_id)];
			if (!$userbank['userbank_name']) pe_apidata(array('code'=>0, 'msg'=>'请选择收款账户'));
			if ($cashout_money < $cache_setting['cashout_min']) pe_apidata(array('code'=>0, 'msg'=>"满{$cache_setting['cashout_min']}元可提现"));
			if ($cashout_money > $info['user_money']) pe_apidata(array('code'=>0, 'msg'=>'余额不足'));
			$sql_set['cashout_money'] = $cashout_money;
			$sql_set['cashout_fee'] = $cashout_fee;
			$sql_set['cashout_jsmoney'] = $cashout_jsmoney;
			$sql_set['cashout_atime'] = time();
			$sql_set['cashout_ip'] = pe_ip();
			$sql_set['cashout_bankname'] = $userbank['userbank_name'];
			$sql_set['cashout_banktype'] = $userbank['userbank_type'];
			$sql_set['cashout_banknum'] = $userbank['userbank_num'];
			$sql_set['cashout_banktname'] = $userbank['userbank_tname'];
			$sql_set['cashout_bankaddress'] = $userbank['userbank_address'];	
			$sql_set['user_id'] = $user['user_id'];
			$sql_set['user_name'] = $user['user_name'];
			if ($userbank['userbank_type'] == 'wechat') {
				if (!$user['user_wx_openid']) pe_apidata(array('code'=>0, 'msg'=>'未绑定微信'));
				$sql_set['user_wx_openid'] = $user['user_wx_openid'];
			}
			if ($db->pe_insert('cashout', pe_dbhold($sql_set))) {
				add_moneylog($user['user_id'], 'cashout', $cashout_money, "提现{$cashout_money}元，手续费{$cashout_fee}元，实到账{$cashout_jsmoney}元");
				pe_apidata(array('code'=>1, 'msg'=>'提交成功'));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'提交失败'));
			}
		}
		!count($userbank_list) && pe_error('请先添加收款账户', 'user.php?mod=userbank&act=add');
		foreach ($userbank_list as $k=>$v) {
			$userbank_list[$k]['userbank_num'] = userbank_num($v['userbank_num']);
			$userbank_list[$k]['userbank_nameshow'] = "{$v['userbank_name']}（".userbank_num($v['userbank_num'])."）";
		}
		$info['cashout_desc'] = $cache_setting['cashout_desc'];
		$info['cashout_fee'] = 0;
		$info['cashout_jsmoney'] = 0;
		pe_fixurl(pe_url("/page/user/cashout_add", 'app'));
		$seo = pe_seo($menutitle='申请提现');
		include(pe_tpl('cashout_add.html'));
	break;
	//####################// 取消提现 //####################//
	case 'del':
		//pe_token_match();
		$cashout_id = intval($_g_id);
		$info = $db->pe_select('cashout', array('cashout_id'=>$cashout_id, 'cashout_state'=>0, 'user_id'=>$user['user_id']));
		if (!$info['cashout_id']) pe_apidata(array('code'=>0, 'msg'=>'参数错误'));
		$sql_set['cashout_state'] = 2;
		$sql_set['cashout_ptime'] = time();		
		if ($db->pe_update('cashout', array('cashout_id'=>$info['cashout_id']), $sql_set)) {
			add_moneylog($user['user_id'], 'back', $info['cashout_money'], "用户取消提现，退回{$info['cashout_money']}元");
			pe_apidata(array('code'=>1, 'msg'=>'取消成功'));
		}
		else {
			pe_apidata(array('code'=>0, 'msg'=>'取消失败'));
		}
	break;
	//####################// 提现列表 //####################//
	default:
		$menumark = 'cashout_list';
		$list = $db->pe_selectall('cashout', array('user_id'=>$user['user_id'], 'order by'=>'`cashout_id` desc'), '*', array(20, $_g_page));
		foreach ($list as $k=>$v) {
			$list[$k]['cashout_banknum'] = userbank_num($v['cashout_banknum']);
			$list[$k]['cashout_atime'] = pe_date($v['cashout_atime']);
		}
		pe_fixurl(pe_url("/page/user/cashout_list", 'app'));
		$seo = pe_seo($menutitle='提现记录');
		include(pe_tpl('cashout_list.html'));
	break;
}
?>