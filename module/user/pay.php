<?php
/**
 * @copyright   2008-2014 简好网络 <http://www.phpshe.com>
 * @creatdate   2016-0101 koyshe <koyshe@gmail.com>
 */
$menumark = 'pay';
switch($act) {
	//####################// 在线充值 //####################//
	default:
		if (isset($_p_pesubmit)) {
			$order_money = pe_num($_p_order_money, 'floor', 2);
			if ($order_money < 0.1) pe_apidata(array('code'=>0, 'msg'=>'最低充值0.1元'));
			//生成付款单
			$order['pay_type'] = 'recharge';	
			$order['order_id'] = '';
			$order['order_name'] = "商城用户【{$user['user_name']}】账户充值{$order_money}元";
			$order['order_money'] = $order_money;
			$order['order_payment'] = key(payment_list('recharge'));
			$order_id = pay_add($user, $order);
			if (!$order_id) pe_apidata(array('code'=>0, 'msg'=>'系统异常，请重试'));	
			pe_apidata(array('code'=>1, 'data'=>array('id'=>$order_id)));
		}
		$info['user_money'] = $user['user_money'];
		pe_fixurl(pe_url("/page/user/pay", 'app'));
		$seo = pe_seo($menutitle='余额充值');
		include(pe_tpl('pay.html'));
	break;
}
?>