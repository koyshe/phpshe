<?php
/**
 * @copyright   2008-2014 简好网络 <http://www.phpshe.com>
 * @creatdate   2012-0501 koyshe <koyshe@gmail.com>
 */
switch ($act) {
	//####################// 文章列表 //####################//
	case 'list':
		if (in_array($id, array('news', 'help'))) {
			$category_id = key($cache_article_category_arr[$id]);
		}
		else {
			$category_id = intval($id);
		}
		$info['category_id'] = $category_id;
		$info['category_type'] = $cache_article_category[$category_id]['category_type'];
		$info['category_typename'] = $ini['article_category_type'][$info['category_type']];
		$info['category_list'] = $cache_article_category_arr[$info['category_type']] ? $cache_article_category_arr[$info['category_type']] : array();

		$list = $db->pe_selectall('article', array('category_id'=>$category_id, 'order by'=>'`article_atime` desc'), '*', array(20, $_g_page));
		foreach ($list as $k=>$v) {
			$list[$k]['article_logo'] = pe_thumb($v['article_logo']);
			$list[$k]['article_atime'] = pe_date($v['article_atime'], 'Y年m月d日');
			$list[$k]['category_name'] = $cache_article_category[$v['category_id']]['category_name'];
		}
		$info['list'] = $list;
		pe_fixurl(pe_url("/page/index/article_list?id={$id}", 'app'));
		$seo = pe_seo($menutitle = $info['category_typename']);
		include(pe_tpl('article_list.html'));
	break;
	//####################// 文章内容 //####################//
	default:
		$article_id = intval($act);
		$db->pe_update('article', array('article_id'=>$article_id), '`article_clicknum`=`article_clicknum`+'.rand(2,5));
		$info = $db->pe_select('article', array('article_id'=>$article_id));
		$info['article_atime'] = pe_date($info['article_atime']);
		$info['category_name'] = $cache_article_category[$info['category_id']]['category_name'];
		pe_fixurl(pe_url("/page/index/article?id={$article_id}", 'app'));		
		$seo = pe_seo($info['article_name']);
		include(pe_tpl('article_view.html'));
	break;
}
?>