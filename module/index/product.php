<?php
switch ($act) {
	//####################// 商品收藏 //####################//
	case 'collect':
		$product_id = intval($_g_id);
		if (!$user['user_id']) pe_apidata(array('code'=>'nologin', 'msg'=>'请先登录'));
		$info = $db->pe_select('product', array('product_id'=>$product_id), 'product_id, product_name, product_logo, product_money, product_collectnum');
		if (!$info['product_id']) pe_apidata(array('code'=>0, 'msg'=>'商品无效'));
		$collect = $db->pe_select('collect', array('product_id'=>$info['product_id'], 'user_id'=>$user['user_id']));
		if ($collect['collect_id']) {
			$db->pe_delete('collect', array('collect_id'=>$collect['collect_id']));
			product_jsnum($product_id, 'collectnum');
			$data['collect'] = false;
			$data['num'] = $info['product_collectnum'] - 1;
			pe_apidata(array('code'=>1, 'msg'=>'已取消', 'data'=>$data));
		}
		else {
			$sql_set['collect_atime'] = time();
			$sql_set['product_id'] = $info['product_id'];
			$sql_set['product_name'] = $info['product_name'];
			$sql_set['product_logo'] = $info['product_logo'];
			$sql_set['product_money'] = $info['product_money'];
			$sql_set['user_id'] = $user['user_id'];
			$sql_set['user_name'] = $user['user_name'];
			$db->pe_insert('collect', pe_dbhold($sql_set));
			product_jsnum($product_id, 'collectnum');
			$data['collect'] = true;
			$data['num'] = $info['product_collectnum'] + 1;
			pe_apidata(array('code'=>1, 'msg'=>'收藏成功', 'data'=>$data));
		}
	break;
	//####################// 商品列表 //####################//
	case 'list':
		$category_id = intval($id);
		$category_zk_id = is_array($cache_category_arr[$category_id]) ? $category_id : intval($cache_category[$category_id]['category_pid']);
		$info = $db->pe_select('category', array('category_id'=>$category_id));
		//检索条件
		$sql_where = " and `product_state` = 1";
		$category_id && $sql_where .= category_get_product($category_id);
		$_g_brand && $sql_where .= " and `brand_id` = ".intval($_g_brand);
		$_g_keyword && $sql_where .= " and `product_name` like '%".pe_dbhold($_g_keyword)."%'";
		$orderby_arr = array('sellnum_desc', 'clicknum_desc', 'money_desc', 'money_asc', 'commentnum_desc');
		if (in_array($_g_orderby, $orderby_arr)) {
			$orderby = explode('_', $_g_orderby);
			$sql_where .= " order by `product_{$orderby[0]}` {$orderby[1]}";
		}
		else {
			$sql_where .= " order by `product_order` asc, `product_id` desc";
		}
		$list = $db->pe_selectall('product', $sql_where, '*', array(40, $_g_page));
		foreach ($list as $k=>$v) {
			$list[$k]['product_logo'] = pe_thumb($v['product_logo'], 400, 400);
			$list[$k]['product_money'] = product_money($v);
		}
		$info['list'] = $list;
		//品牌列表
		$brand_list = category_brand($category_id);
		//当前路径
		if (isset($_g_keyword)) {
			$nowpath = category_path(0, array('搜索 “'.htmlspecialchars($_g_keyword).'”'));
			$seo = pe_seo('站内搜索');
		}
		else {
			$nowpath = $category_id ? category_path($category_id) : category_path($category_id, array('全部商品'));
			$seo = pe_seo($info['category_title']?$info['category_title']:$info['category_name'], $info['category_keys'], pe_text($info['category_desc']));
		}
		pe_fixurl(pe_url("/page/index/product_list?id={$category_id}", 'app'));
		include(pe_tpl('product_list.html'));
	break;
	//####################// 商品规格 //####################//
	case 'prodata_list':
		$product_id = intval($id);
		$info = $db->pe_select('product', array('product_id'=>$product_id));
		$info['product_logo'] = pe_thumb($info['product_logo']);
		$info['product_rule'] = $info['product_rule'] ? json_decode($info['product_rule'], true) : array();
		foreach ($info['product_rule'] as $k=>$v) {
			$info['product_rule'][$k]['logo'] = $v['logo'] ? pe_thumb($v['logo'], 100, 100) : ''; 
		}
		$prodata_list = $db->pe_selectall('prodata', array('product_id'=>$product_id, 'order by'=>'product_order asc'), 'product_guid, product_ruleid, product_money, product_ymoney, product_num');
		if ($info['huodong_id']) {
			$huodong_prodata = $db->index('product_guid')->pe_selectall('huodong_prodata', array('huodong_id'=>$info['huodong_id'], 'product_id'=>$product_id));		
			foreach ($prodata_list as $k=>$v) {
				$prodata_list[$k]['product_money'] = $huodong_prodata[$v['product_guid']]['product_money'];
				$prodata_list[$k]['product_num'] = $huodong_prodata[$v['product_guid']]['product_num'];
			}
		}
		$info['prodata_list'] = $prodata_list;
		if (count($info['product_rule'])) {
			$info['prodata_sel'] = array_fill(0, count($info['product_rule']), '');
			$info['product_guid'] = '';
		}
		else {
			$info['prodata_sel'] = array();
		}
		pe_apidata(array('code'=>1, 'data'=>$info));
	break;
	//####################// 商品内容 //####################//
	default:
		$product_id = intval($act);
		//更新点击
		product_jsnum($product_id, 'clicknum');
		$info = $db->pe_select('product', array('product_id'=>$product_id));
		if (!$info['product_id']) pe_404();
		add_footprint($info);
		$info['product_logo'] = pe_thumb($info['product_logo']);
		$info['product_money'] = product_money($info);
		$info['product_point'] = intval($info['product_money'] * $cache_setting['point_order']);
		//商品相册
		$album_list = $info['product_album'] ? explode(',', $info['product_album']) : array();
		foreach ($album_list as $k=>$v) {
			$album_list[$k] = pe_thumb($v, 800, 800);
		}
		$info['album_list'] = $album_list;
		//商品规格
		$info['product_rule'] = $info['product_rule'] ? json_decode($info['product_rule'], true) : array();
		foreach ($info['product_rule'] as $k=>$v) {
			$info['product_rule'][$k]['logo'] = $v['logo'] ? pe_thumb($v['logo'], 100, 100) : ''; 
		}
		$prodata_list = $db->pe_selectall('prodata', array('product_id'=>$product_id, 'order by'=>'product_order asc'), 'product_guid, product_ruleid, product_money, product_ymoney, product_num');
		if ($info['huodong_id']) {
			$huodong_prodata = $db->index('product_guid')->pe_selectall('huodong_prodata', array('huodong_id'=>$info['huodong_id'], 'product_id'=>$product_id));		
			foreach ($prodata_list as $k=>$v) {
				$prodata_list[$k]['product_money'] = $huodong_prodata[$v['product_guid']]['product_money'];
				$prodata_list[$k]['product_num'] = $huodong_prodata[$v['product_guid']]['product_num'];
			}
		}
		$info['prodata_list'] = $prodata_list;
		if (count($info['product_rule'])) {
			$info['prodata_sel'] = array_fill(0, count($info['product_rule']), '');
			$info['product_guid'] = '';
		}
		else {
			$info['prodata_sel'] = array();
		}
		//检测收藏
		$collect = $db->pe_num('collect', array('product_id'=>$product_id, 'user_id'=>$user['user_id']));
		$info['collect'] = $collect ? true : false;
		//优惠券列表
		$quan_list = $db->pe_selectall('quan', " and `quan_type` = 'online' and `quan_state` = 1 and (`product_id` = '' or find_in_set({$product_id}, `product_id`)) order by `quan_money` asc");
		$my_quanlist = $db->index('quan_id')->pe_selectall('quanlog', array('user_id'=>$user['user_id'], 'group by'=>'quan_id'), 'quan_id, count(1) as num');
		foreach ($quan_list as $k=>$v) {
			$quan_list[$k]['quan_money'] = pe_num($v['quan_money'], 'floor', 1);
			$quan_list[$k]['quan_limit'] = pe_num($v['quan_limit'], 'floor', 1);
			if ($v['quan_limitnum'] == 0 or intval($my_quanlist[$v['quan_id']]['num']) < $v['quan_limitnum']) {
				$quan_list[$k]['get'] = true;
			}
			else {
				$quan_list[$k]['get'] = false;			
			}
		}
		$info['quan_list'] = $quan_list;
		$info['quan_num'] = count($quan_list);
		$info['cart_num'] = cart_num();
		//购买记录
		$buylog_list = $db->pe_selectall('buylog', array('product_id'=>$info['product_id'], 'order by'=>'buylog_atime desc'), '*', array(20));
		foreach ($buylog_list as $k=>$v) {
			$buylog_list[$k]['buylog_atime'] = pe_date($v['buylog_atime'], 'Y-m-d');
			$buylog_list[$k]['user_name'] = mb_substr($v['user_name'], 0, 1, 'utf8').'**'.mb_substr($v['user_name'], -1, 1, 'utf8');
			$buylog_list[$k]['user_logo'] = pe_thumb($v['user_logo'], 100, 100, 'avatar');
		}
		$info['buylog_list'] = $buylog_list;
		$product_hotlist = product_hotlist();
		//新品推荐
		$product_newlist = product_newlist(2);
		//当前路径
		$nowpath = category_path($info['category_id'], array($info['product_name']));
		$info['product_desc'] = $info['product_desc'] ? $info['product_desc'] : pe_cut(pe_text($info['product_text']), 200);
		pe_fixurl(pe_url("/page/index/product?id={$product_id}", 'app'));
		$seo = pe_seo($info['product_name'], $info['product_keys'], $info['product_desc']);
		//检测拼团
		if ($info['huodong_type'] == 'pintuan') {
			$pintuan = $db->pe_select('huodong_product', array('product_id'=>$info['product_id'], 'huodong_id'=>$info['huodong_id']));
			$info['product_ptnum'] = $pintuan['product_ptnum'];
			//待成团团单
			$pintuan_list = $db->pe_selectall('pintuan', array('product_id'=>$info['product_id'], 'huodong_id'=>$info['huodong_id'], 'pintuan_state'=>'wtuan', 'order by'=>'pintuan_id asc'), '*', array(20));
			$pintuan_num = $db->pe_num('pintuan', array('product_id'=>$info['product_id'], 'huodong_id'=>$info['huodong_id'], 'pintuan_state'=>'wtuan'));
			include(pe_tpl('product_pintuan.html'));
		}
		else {
			include(pe_tpl('product_view.html'));
		}
	break;
}
?>