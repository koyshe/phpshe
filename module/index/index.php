<?php
/**
 * @copyright   2008-2014 简好网络 <http://www.phpshe.com>
 * @creatdate   2012-0501 koyshe <koyshe@gmail.com>
 */

//网站公告
$info['notice_list'] = $db->pe_selectall('article', array('category_id'=>1, 'order by'=>'`article_atime` desc'), '*', array(2));

//焦点图广告
if (is_array($cache_ad['pc']['index_jdt'])) {
	foreach ($cache_ad['pc']['index_jdt'] as $k=>$v) {
		$ad_list[$k]['ad_logo'] = pe_thumb($v['ad_logo']);
	}
}
$info['ad_list'] = is_array($ad_list) ? $ad_list : array();

//秒杀商品
$miaosha = $db->pe_select('huodong', " and huodong_type = 'miaosha' and huodong_state in('wstart', 'work') order by huodong_stime asc");
$miaosha['time'] = pe_date($miaosha['huodong_stime'], 'H:i');
if ($miaosha['huodong_stime'] > time()) {
	$miaosha['desc'] = '距离开始还有';
	$miaosha['etime'] = $miaosha['huodong_stime'];
}
else {
	$miaosha['desc'] = '距离结束还有';
	$miaosha['etime'] = $miaosha['huodong_etime'];
}
$miaosha_list = $db->pe_selectall('huodong_product', array('huodong_id'=>$miaosha['huodong_id'], 'order by'=>'huodong_id asc'), '*', array(4));
foreach ($miaosha_list as $k=>$v) {
	$miaosha_list[$k]['product_logo'] = pe_thumb($v['product_logo'], 400, 400);
}
$info['miaosha_list'] = $miaosha_list;

//分类商品
$info['category_list'] = array();
foreach((array)$cache_category_arr[0] as $k=>$v) {
	if (!$v['category_state']) continue;
	$product_list = $db->pe_selectall('product', category_get_product($v['category_id'])." and `product_state` = 1 order by `product_tuijian` desc, `product_order` asc, `product_id` desc", '*', array(10));
	foreach ($product_list as $kk=>$vv) {
		$product_list[$kk]['product_logo'] = pe_thumb($vv['product_logo'], 400, 400);
		$product_list[$kk]['product_money'] = product_money($vv);
	}
	$v['product_list'] = $product_list;
	$info['category_list'][] = $v;
}
pe_fixurl(pe_url("/page/index/index", 'app'));
$seo = pe_seo();
include(pe_tpl('index.html'));
?>