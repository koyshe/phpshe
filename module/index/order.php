<?php
/**
 * @copyright   2008-2014 简好网络 <http://www.phpshe.com>
 * @creatdate   2012-0501 koyshe <koyshe@gmail.com>
 */
pe_lead('hook/useraddr.hook.php');
$cache_payment = cache::get('payment');
//设置游客信息
if (!$user['user_id']) $user = user_guest();
switch ($act) {
	//####################// 提交订单 //####################//
	case 'add':
		if ($_g_from =='choujiang') {
			$id = intval($_g_id);
			$choujiang = $db->pe_select('choujianglog', array('choujianglog_id'=>$id, 'choujianglog_state'=>'success', 'jiang_type'=>'product', 'user_id'=>$user['user_id']));
			if (!$choujiang['choujianglog_id']) pe_apidata(array('code'=>0, 'msg'=>'奖品无效'));
			if ($choujiang['order_id']) pe_apidata(array('code'=>0, 'msg'=>'请勿重复领取'));
			$product['product_id'] = 0;
			$product['product_guid'] = 0;
			$product['product_name'] = $choujiang['jiang_name'];
			$product['product_logo'] = $choujiang['jiang_logo'];
			$product['product_money'] = 0;			
			$product['product_num'] = 1;
			$product['product_allmoney'] = $product['product_money'] * $product['product_num'];
			$info['list'][0] = $product;
			$info['order_type'] = 'fixed';
			$info['order_name'] = $product['product_name'];
			$info['order_product_money'] = $product['product_allmoney'];
			$info['order_wl_money'] = 0;
		}
		else {
			$cart_id = $_g_id ? explode(',', $_g_id) : '';
			$info = cart($cart_id);	
		}
		$payment_list = payment_list('order_add', $info['order_virtual']);
		//计算使用积分		
		$info['user_point'] = intval($user['user_point']) - intval($info['order_point']);
		$info['point_bili'] = intval($cache_setting['point_bili']);		
		$info['point_maxlimit'] = pe_num($cache_setting['point_maxlimit'], 'round', 2);
		//读取优惠券
		$quan_list = user_quanlist($info);
		if (isset($_p_pesubmit)) {
			$address_id = pe_dbhold($_p_address_id);
			$order_quan_id = intval($_p_order_quan_id);
			$order_point_use = intval($_p_order_point_use);
			$order_payment = pe_dbhold($_p_order_payment);	
			if (!$user['user_id']) pe_apidata(array('code'=>'nologin', 'msg'=>'请先登录'));
 			if (!count($info['list'])) pe_apidata(array('code'=>0, 'msg'=>'购物车商品为空'));
 			//输出购买错误信息
			foreach ($info['list'] as $v) {
				if ($v['error']) pe_apidata(array('code'=>0, 'msg'=>"【{$v['error']['msg']}】{$v['product_name']}"));
			}
			if (!array_key_exists($order_payment, $payment_list)) pe_apidata(array('code'=>0, 'msg'=>'请选择付款方式'));
			//虚拟商品不用检测收货地址
 			if (!$info['order_virtual']) {
	 			$address = get_useraddr_info($address_id);
				if (!$address['address_id']) pe_apidata(array('code'=>0, 'msg'=>'请选择收货地址'));			
 			}
 			else {
 				$address = array();
 			}
			if ($order_quan_id && !$quan_list[$order_quan_id]['quan_id']) pe_apidata(array('code'=>0, 'msg'=>'优惠券无效'));
			$sql_order['order_id'] = $order_id = pe_guid('order|order_id');
			$sql_order['order_type'] = $info['order_type'];	
			$sql_order['order_virtual'] = $info['order_virtual'];
			$sql_order['order_name'] = $info['order_name'];	
			$sql_order['order_product_money'] = $info['order_product_money'];
			//$sql_order['order_wl_money'] = $info['order_wl_money'];
			$sql_order['order_wl_money'] = order_wlmoney($info['list'], $address['address_province']);		
			//$sql_order['order_point_get'] = $info['order_point_get'];
			$sql_order['order_atime'] = time();
			$sql_order['order_payment'] = $order_payment;
			$sql_order['order_payment_name'] = $cache_payment[$order_payment]['payment_name'];
			$sql_order['order_state'] = $order_payment == 'cod' ? 'wsend' : 'wpay';
			$sql_order['order_text'] = $_p_order_text;
			$sql_order['huodong_id'] = $info['huodong_id'];	
			$sql_order['pintuan_id'] = $info['pintuan_id'];	
			$sql_order['user_id'] = $user['user_id'];
			$sql_order['user_name'] = $user['user_name'];
			$sql_order['user_address'] = "{$address['address_province']}{$address['address_city']}{$address['address_area']}{$address['address_text']}";
			$sql_order['user_tname'] = $address['user_tname'];
			$sql_order['user_phone'] = $address['user_phone'];
			if ($order_quan_id) {
				$sql_order['order_quan_id'] = $order_quan_id;
				$sql_order['order_quan_name'] = $quan_list[$order_quan_id]['quan_name'];
				$sql_order['order_quan_money'] = $quan_list[$order_quan_id]['quan_money'];
			}
			if ($order_point_use) {
				$order_point_use = pe_num(($sql_order['order_product_money']  - $sql_order['order_quan_money']) * $info['point_maxlimit'] * $info['point_bili'], 'floor');
				if ($order_point_use > $info['user_point']) $order_point_use = $info['user_point'];
				$order_point_money = $info['point_bili'] ? pe_num($order_point_use / $info['point_bili'], 'round', 2) : 0;
				if ($order_point_money > 0) {
					$sql_order['order_point_use'] = $order_point_use;
					$sql_order['order_point_money'] = $order_point_money;				
				}
			}
			$sql_order['order_money'] = $sql_order['order_product_money'] + $sql_order['order_wl_money'] - $sql_order['order_quan_money'] - $sql_order['order_point_money'];
			if ($sql_order['order_money'] < 0) $sql_order['order_money'] = 0;
			$sql_order['order_point_get'] = intval(($sql_order['order_product_money'] - $sql_order['order_quan_money'] - $sql_order['order_point_money']) * $cache_setting['point_order']);
			if ($db->pe_insert('order', pe_dbhold($sql_order))) {
				foreach ($info['list'] as $v) {
					$sql_orderdata['product_id'] = $v['product_id'];
					$sql_orderdata['product_guid'] = $v['product_guid'];
					$sql_orderdata['product_name'] = $v['product_name'];
					$sql_orderdata['product_rule'] = $v['product_rule'];
					$sql_orderdata['product_logo'] = $v['product_logo'];
					$sql_orderdata['product_money'] = $v['product_money'];
					$sql_orderdata['product_allmoney'] = $v['product_allmoney'];
					$sql_orderdata['product_num'] = $v['product_num'];
					$sql_orderdata['product_sn'] = $v['product_sn'];
					$sql_orderdata['huodong_id'] = $v['huodong_id'];
					$sql_orderdata['huodong_type'] = $v['huodong_type'];
					$sql_orderdata['huodong_tag'] = $v['huodong_tag'];
					$sql_orderdata['order_id'] = $order_id;
					$db->pe_insert('orderdata', pe_dbhold($sql_orderdata, array('product_rule')));
				}
				order_add_callback($order_id, $cart_id);
				//更新抽奖order_id
				if ($_g_from =='choujiang') {
					$db->pe_update('choujianglog', array('choujianglog_id'=>$id), array('order_id'=>$order_id));					
				}
				//生成付款单
				$order = array();
				$order['pay_type'] = 'order';
				$order['order_id'] = $sql_order['order_id'];
				$order['order_name'] = $sql_order['order_name'];	
				$order['order_money'] = $sql_order['order_money'];
				$order['order_payment'] = $sql_order['order_payment'];	
				$pay_id = pay_add($user, $order);
				if (!$pay_id) pe_apidata(array('code'=>0, 'msg'=>'系统异常，请重试'));	
				$data = array('id'=>$pay_id, 'close'=>true);
				pe_apidata(array('code'=>1, 'msg'=>'订单已提交', 'data'=>$data));
			}
			else {
				pe_apidata(array('code'=>0, 'msg'=>'下单失败'));
			}	
		}
		$info['quan_list'] = array_values($quan_list);
		foreach ($info['list'] as $k=>$v) {
			$info['list'][$k]['product_logo'] = pe_thumb($v['product_logo'], 100, 100);
			$info['list'][$k]['product_rule'] = order_skushow($v['product_rule']);
		}
		$info['list'] = array_values($info['list']);
		pe_fixurl(pe_url("/page/index/order_add?id={$_g_id}&from={$_g_from}", 'app'));
		$seo = pe_seo($menutitle='提交订单');
		include(pe_tpl('order_add.html'));
	break;
	//####################// 拼团详情 //####################//
	case 'pintuan':
		$pintuan_id = pe_dbhold($id);
		$info = $db->pe_select('pintuan', array('pintuan_id'=>$pintuan_id));
		if (!$info['pintuan_id'] or $info['pintuan_state'] == 'wpay') pe_404();
		$info['product_money'] = product_money($info['product_money']);
		$info['pintuan_money'] = product_money($info['pintuan_money']);
		//参团用户记录
		$info_list = $db->pe_selectall('pintuanlog', array('pintuan_id'=>$info['pintuan_id'], 'order by'=>'pintuanlog_id asc'));
		//$sql = "select a.*, b.`user_logo` from `".dbpre."order` a left join `".dbpre."user` b on a.`user_id` = b.`user_id` where a.`order_ptid` = '{$order['order_ptid']}' and a.`order_state` in('wtuan', 'wsend', 'wget', 'success') order by a.`order_ptime` asc";
		//$info_list = $db->sql_selectall($sql);
		//成团剩余人数
		$need_num = intval($info['pintuan_num'] - $info['pintuan_joinnum']);
		$my = $db->pe_select('pintuanlog', array('user_id'=>$user_id, 'pintuan_id'=>$info['pintuan_id']), 'order_id');
		//热门拼团
		$pintuan_list = $db->pe_selectall('product', array('huodong_type'=>'pintuan', 'order by'=>'product_sellnum desc'), '*', array(5));
		$seo = pe_seo($menutitle='拼团详情');
		include(pe_tpl('order_pintuan.html'));
	break;
}
?>