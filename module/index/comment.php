<?php
switch ($act) {
	//####################// 评价列表 //####################//
	default:
		$product_id = intval($id);
		$num = $_g_num ? intval($_g_num) : 20;
		$info = $db->pe_select('product', array('product_id'=>$product_id));
		$product_commentrate = explode(',', $info['product_commentrate']);
		$info['hao_num'] = intval($product_commentrate[0]);
		$info['zhong_num'] = intval($product_commentrate[1]);		
		$info['cha_num'] = intval($product_commentrate[2]);
		$info['all_num'] = $info['product_commentnum'];
		if ($info['all_num']) {
			$info['star'] = pe_num($info['product_commentstar']/$info['all_num'], 'round', 1, true);
			$info['hao_rate'] = intval($info['hao_num']/$info['all_num']*100);
			$info['zhong_rate'] = intval($info['zhong_num']/$info['all_num']*100);
			$info['cha_rate'] = intval($info['cha_num']/$info['all_num']*100);
		}
		else {
			$info['star'] = '5.0';
			$info['hao_rate'] = '100';
			$info['zhong_rate'] = '0';
			$info['cha_rate'] = '0';
		}
		$info['point'] = $cache_setting['point_comment'] ? "(+{$cache_setting['point_comment']}积分)":'';
		//评价列表
		$star_arr = array('hao'=>array(4,5), 'zhong'=>3, 'cha'=>array(1,2));	
		if (array_key_exists($_g_star, $star_arr)) $sql_where['comment_star'] = $star_arr[$_g_star];
		$product_id && $sql_where['product_id'] = $product_id;
		$sql_where['order by'] = "`comment_atime` desc";
		$sql_filed = "comment_text,comment_logo,comment_star,comment_atime,comment_reply,comment_reply_text,comment_reply_time,user_name,user_logo";
		$list = $db->pe_selectall('comment', $sql_where, $sql_filed, array($num, $_g_page));
		foreach ($list as $k=>$v) {
			$list[$k]['comment_star'] = pe_comment($v['comment_star']);
			$list[$k]['comment_atime'] = pe_date($v['comment_atime'], 'Y-m-d');
			$list[$k]['comment_reply_time'] = pe_date($v['comment_reply_time'], 'Y-m-d');
			$list[$k]['user_logo'] = pe_thumb($v['user_logo'], 100, 100, 'avatar');
			//评价晒图
			$comment_logo = $v['comment_logo'] ? explode(',', $v['comment_logo']) : array();
			foreach ($comment_logo as $kk=>$vv) {
				$comment_logo[$kk] = array();
				$comment_logo[$kk]['logo'] = pe_thumb($vv, '_400', '_400');
				$comment_logo[$kk]['url'] = pe_url("comment-logo?id={$v['comment_id']}&num={$kk}");
			}
			$list[$k]['comment_logo'] = $comment_logo;
		}
		$info['list'] = $list;
		$info['page'] = $db->page->ajax('comment_list', $_g_star); 
		pe_apidata(array('code'=>1, 'data'=>$info));
	break;
}