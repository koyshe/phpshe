<?php
$menumark = 'useraddr';
pe_lead('hook/useraddr.hook.php');
//设置游客信息
if (!$user['user_id']) $user = user_guest();
switch($act) {
	//####################// 添加地址 //####################//
	case 'add':
		if (isset($_p_pesubmit)) {
			pe_token_match();
			if (!$user['user_id']) pe_apidata(array('code'=>'nologin', 'msg'=>'请先登录'));
			if (!$_p_user_tname) pe_apidata(array('code'=>0, 'msg'=>'请填写收货人'));
			if (!pe_formcheck('phone', $_p_user_phone)) pe_apidata(array('code'=>0, 'msg'=>'请填写正确的手机号'));
			if (!$_p_address_province) pe_apidata(array('code'=>0, 'msg'=>'请选择省份'));
			if (!$_p_address_city) pe_apidata(array('code'=>0, 'msg'=>'请选择城市'));
			if (!$_p_address_area) pe_apidata(array('code'=>0, 'msg'=>'请选择区县'));		
			if (!$_p_address_text) pe_apidata(array('code'=>0, 'msg'=>'请填写详细地址'));
			$sql_set['address_province'] = $_p_address_province;
			$sql_set['address_city'] = $_p_address_city;
			$sql_set['address_area'] = $_p_address_area;
			$sql_set['address_text'] = $_p_address_text;
			$sql_set['address_atime'] = time();
			$sql_set['address_default'] = intval($_p_address_default);
			$sql_set['user_id'] = $user['user_id'];
			$sql_set['user_name'] = $user['user_name'];
			$sql_set['user_tname'] = $_p_user_tname;
			$sql_set['user_phone'] = $_p_user_phone;
			add_useraddr($sql_set);
		}
		$seo = pe_seo($menutitle='新增地址');
		include(pe_tpl('useraddr_add.html'));
	break;
	//####################// 商品详情获取运费价格 //####################//
	case 'wlmoney':
		$product_id = intval($_g_id);
		$product = $db->pe_select('product', array('product_id'=>$product_id), 'wlmb_id');
		$addr = file_get_contents("http://www.phpshe.com/index.php?mod=api&act=ipaddr&ip=".pe_ip());
		$city_list = $db->pe_selectall('city', array('city_pid'=>0));
		foreach ($city_list as $v) {
			if (stripos($addr, $v['city_name']) !== false) $province = $v['city_name'];
		}
		$product_list = array(array('wlmb_id'=>$product['wlmb_id'], 'product_num'=>1));
		$info['wlmoney'] = order_wlmoney($product_list, $province);
		$info['wlmoney'] = $info['wlmoney'] > 0 ? "¥ {$info['wlmoney']}" : '包邮';
		pe_apidata(array('code'=>1, 'data'=>$info));
	break;
	//####################// 地址列表 //####################//
	default:
		$address_id = pe_dbhold($_g_id);
		$cart_id = $_g_cart_id ? explode(',', pe_dbhold($_g_cart_id)) : 0;
		$info = get_useraddr_list($address_id);
		$cart = cart($cart_id);
		$info['wlmoney'] = order_wlmoney($cart['list'], $info['address_province']);
		pe_apidata(array('code'=>1, 'data'=>$info));
	break;
}
?>