<?php
/**
 * @copyright   2008-2014 简好网络 <http://www.phpshe.com>
 * @creatdate   2012-0501 koyshe <koyshe@gmail.com>
 */
//设置游客信息
if (!$user['user_id']) $user = user_guest();
switch ($act) {
	//####################// 加入购物车/立即购买 //####################//
	case 'add':
	case 'buy':
	case 'pintuan':
		$product_id = intval($_g_id);
		$product_guid = pe_dbhold($_g_guid);
		$product_num = intval($_g_num);
		if (!$user['user_id']) pe_apidata(array('code'=>'nologin', 'msg'=>'请先登录'));
		if (!$product_guid) pe_apidata(array('code'=>0, 'msg'=>'请选择商品规格'));
		//检测库存		
		$product = product_buyinfo($product_guid);
		if (!$product['product_id']) pe_apidata(array('code'=>0, 'msg'=>'商品已下架/失效'));
		if ($product['product_num'] < $product_num) pe_apidata(array('code'=>0, 'msg'=>"库存仅剩{$product['product_num']}件"));
		//检测虚拟商品
		if ($act == 'add' && $product['product_virtual']) pe_apidata(array('code'=>0, 'msg'=>'不能加入购物车'));
		//检测拼团
		if ($act == 'add' && $product['huodong_type'] == 'pintuan') pe_apidata(array('code'=>0, 'msg'=>'不能加入购物车'));
		if ($act == 'pintuan') {
			$ret = pintuan_check($product['huodong_id'], $_g_pintuan_id);
			if ($ret['code'] != 1) pe_apidata(array('code'=>0, 'msg'=>$ret['msg']));
		}
		$cart = $db->pe_select('cart', array('cart_act'=>'add', 'user_id'=>$user['user_id'], 'product_guid'=>$product_guid));
		if ($act == 'add' && $cart['cart_id']) {
			$sql_set['product_num'] = $cart['product_num'] + $product_num;
			if ($product['product_num'] < $sql_set['product_num']) pe_apidata(array('code'=>0, 'msg'=>"库存仅剩{$product['product_num']}件"));		
			if (!$db->pe_update('cart', array('cart_id'=>$cart['cart_id']), $sql_set)) pe_apidata(array('code'=>0, 'msg'=>'异常请重新操作'));
		}
		else {
			if ($act == 'pintuan') {
				$sql_set['cart_type'] = 'pintuan';
				$sql_set['pintuan_id'] = $_g_pintuan_id;
			}
			else {
				$sql_set['cart_type'] = 'fixed';
			}
			$sql_set['cart_act'] = $act;
			$sql_set['cart_atime'] = time();
			$sql_set['product_id'] = $product['product_id'];
			$sql_set['product_guid'] = $product['product_guid'];
			$sql_set['product_name'] = $product['product_name'];
			$sql_set['product_rule'] = $product['product_rule'];
			$sql_set['product_logo'] = $product['product_logo'];
			$sql_set['product_money'] = $product['product_money'];
			$sql_set['product_num'] = $product_num;
			$sql_set['user_id'] = $user['user_id'];
			$cart['cart_id'] = $db->pe_insert('cart', pe_dbhold($sql_set, array('product_rule')));
			if (!$cart['cart_id']) pe_apidata(array('code'=>0, 'msg'=>'异常请重新操作'));
		}
		$data['id'] = $cart['cart_id'];
		$data['cart_num'] = cart_num();
		pe_apidata(array('code'=>1, 'data'=>$data));
	break;
	//####################// 购物车修改(为零就删除吧) //####################//
	case 'edit':
		$cart_id = intval($_g_id);
		$product_num = intval($_g_num);
		if (!$user['user_id']) pe_apidata(array('code'=>'nologin', 'msg'=>'请先登录'));
		$cart = $db->pe_select('cart', array('cart_id'=>$cart_id, 'user_id'=>$user['user_id']));		
		if ($product_num) {			
			$product = product_buyinfo($cart['product_guid']);
			if (!$product['product_id']) pe_apidata(array('code'=>0, 'msg'=>'商品已下架/失效', 'data'=>array('num'=>$product_num)));
			if ($product['product_num'] < $product_num) pe_apidata(array('code'=>0, 'msg'=>"库存仅剩{$product['product_num']}件", 'data'=>array('num'=>$product['product_num'])));
			if (!$db->pe_update('cart', array('cart_id'=>$cart['cart_id']), array('product_num'=>$product_num))) {
				pe_apidata(array('code'=>0, 'msg'=>'修改失败', 'data'=>array('num'=>$cart['product_num'])));
			}
		}
		else {
			if (!$db->pe_delete('cart', array('cart_id'=>$cart['cart_id']))) {
				pe_apidata(array('code'=>0, 'msg'=>'删除失败', 'data'=>array('num'=>$cart['product_num'])));
			}	
		}
		$info['cart_num'] = cart_num();
		$info['num'] = $product_num;
		pe_apidata(array('code'=>1, 'data'=>$info));
	break;
	//####################// 购物车列表 //####################//
	default:
		$info = cart();
		if (isset($_p_pesubmit)) {
			if (!$user['user_id']) pe_apidata(array('code'=>'nologin', 'msg'=>'请先登录'));
			$cart_id = is_array($_p_cart_id) ? $_p_cart_id : array();
			foreach ($info['list'] as $k=>$v) {
				if (!in_array($v['cart_id'], $cart_id)) continue;
				if ($v['error']) pe_apidata(array('code'=>0, 'msg'=>"【{$v['error']['msg']}】{$v['product_name']}"));
				$cart_idarr[] = $v['cart_id'];		
			}
			if (!is_array($cart_idarr)) pe_apidata(array('code'=>0, 'msg'=>'请选择商品'));
			$data['id'] = implode(',', $cart_idarr);
			pe_apidata(array('code'=>1, 'data'=>$data));
		}
		foreach ($info['list'] as $k=>$v) {
			$info['list'][$k]['product_logo'] = pe_thumb($v['product_logo'], 100, 100);
			$info['list'][$k]['product_rule'] = order_skushow($v['product_rule']);
			$info['list'][$k]['sel'] = false;
		}
		$info['list'] = array_values($info['list']);
		pe_fixurl(pe_url("/page/index/cart", 'app'));
		$seo = pe_seo($menutitle='我的购物车');
		include(pe_tpl('cart.html'));
	break;
}
?>