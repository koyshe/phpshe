<?php
/**
 * @copyright   2008-2014 简好网络 <http://www.phpshe.com>
 * @creatdate   2012-0501 koyshe <koyshe@gmail.com>
 */
$menumark = 'quan';
switch ($act) {
	//####################// 优惠券列表 //####################//
	case 'list':
		user_quancheck();
		$list = $db->pe_selectall('quan', array('quan_type'=>'online', 'quan_state'=>1, 'order by'=>'`quan_id` desc'));
		foreach ($list as $k=>$v) {
			$list[$k]['quan_money'] = pe_num($v['quan_money'], 'floor', 1);
			$list[$k]['quan_limit'] = pe_num($v['quan_limit'], 'floor', 1);
			$list[$k]['quan_limitshow'] = quan_limitshow($v);
			$list[$k]['quan_btn'] = quan_btn($v);
			if ($v['quan_datetype'] == 'relative') {
				$list[$k]['quan_sdate'] = date('Y-m-d');
				$list[$k]['quan_edate'] = date('Y-m-d', strtotime("+{$v['quan_day']} day"));	
			}
			$list[$k]['product_logo'] = pe_thumb($v['product_logo'], 400, 400);
		}
		$info['list'] = $list;
		pe_fixurl(pe_url("/page/index/quan_list", 'app'));
		$seo = pe_seo($menutitle='领券中心');
		include(pe_tpl('quan_list.html'));
	break;
	//####################// 领取优惠券 //####################//
	case 'get':
		$info = $db->pe_select('quan', array('quan_id'=>intval($_g_id)));
		if (!$user['user_id']) pe_apidata(array('code'=>'nologin', 'msg'=>'请先登录'));
		if (!$info['quan_id']) pe_apidata(array('code'=>0, 'msg'=>'优惠券无效'));
		if ($info['quan_type'] != 'online') pe_apidata(array('code'=>0, 'msg'=>'不可领取'));
		if ($info['quan_num'] <= $info['quan_num_get']) pe_apidata(array('code'=>0, 'msg'=>'优惠券已领完'));
		if (!$info['quan_state']) pe_apidata(array('code'=>0, 'msg'=>'优惠券已失效'));
		$quanlog_num = $db->pe_num('quanlog', array('quan_id'=>$info['quan_id'], 'user_id'=>$user['user_id']));
		if ($info['quan_limitnum'] && $quanlog_num >= $info['quan_limitnum']) pe_apidata(array('code'=>0, 'msg'=>'请勿重复领取'));
		if (add_quanlog($user, $info['quan_id'])) {
			$quan_btn = quan_btn($info);
			pe_apidata(array('code'=>1, 'msg'=>'领取成功', 'data'=>$quan_btn));
		}
		else {
			pe_apidata(array('code'=>0, 'msg'=>'领取失败'));	
		}
	break;
	//####################// 优惠券详情 //####################//
	default:
		$quan_id = intval($act);
		$info = $db->pe_select('quan', array('quan_id'=>$quan_id));
		if (!$info['quan_id']) pe_404();
		$info['quan_money'] = pe_num($info['quan_money'], 'floor', 1);
		$info['quan_limit'] = pe_num($info['quan_limit'], 'floor', 1);
		$info['quan_limitshow'] = quan_limitshow($info);
		$info['quan_btn'] = quan_btn($info);
		if ($info['quan_datetype'] == 'relative') {
			$info['quan_sdate'] = date('Y-m-d');
			$info['quan_edate'] = date('Y-m-d', strtotime("+{$info['quan_day']} day"));	
		}
		if ($info['product_id']) {
			$list = $db->pe_selectall('product', array('product_state'=>1, 'product_id'=>explode(',', $info['product_id'])), '*', array(20));		
		}
		else {
			$list = $db->pe_selectall('product', array('product_state'=>1, 'order by'=>'product_sellnum desc'), '*', array(20));
		}
		foreach ($list as $k=>$v) {
			$list[$k]['product_logo'] = pe_thumb($v['product_logo'], 400, 400);
			$list[$k]['product_money'] = product_money($v);
		}
		$info['list'] = $list;
		pe_fixurl(pe_url("/page/index/quan?id={$quan_id}", 'app'));
		$seo = pe_seo($menutitle='领取优惠券');
		include(pe_tpl('quan_view.html'));
	break;
}

//使用条件
function quan_limitshow($info) {
	if ($info['quan_limit'] > 0 && $info['product_id']) {
		$quan_limit = "满{$info['quan_limit']}元指定商品可用";			
	}
	elseif ($info['quan_limit'] > 0 && !$info['product_id']) {
		$quan_limit = "满{$info['quan_limit']}元可用";			
	}
	elseif ($info['quan_limit'] == 0 && $info['product_id']) {
		$quan_limit = "指定商品可用";			
	}
	else {
		$quan_limit = '全店通用';
	}
	return $quan_limit;
}

//优惠券按钮
function quan_btn($info) {
	global $db, $user;
	$quanlog_num = $db->pe_num('quanlog', array('quan_id'=>$info['quan_id'], 'user_id'=>$user['user_id']));
	if ($info['quan_state'] == 0) {
		$json = array('result'=>false, 'show'=>'已过期');
	}
	elseif ($info['quan_num'] - $info['quan_num_get'] <= 0) {	
		$json = array('result'=>false, 'show'=>'领光啦');
	}
	elseif ($info['quan_limitnum'] && $quanlog_num >= $info['quan_limitnum']) {
		$json = array('result'=>false, 'show'=>'已领取');
	}
	else {
		$json = array('result'=>true, 'show'=>'点击领取');
	}
	return $json;
}
?>