<?php
/**
 * @copyright   2008-2014 简好网络 <http://www.phpshe.com>
 * @creatdate   2012-0501 koyshe <koyshe@gmail.com>
 */
$cache_payment = cache::get('payment');
//设置游客信息
if (!$user['user_id']) $user = user_guest();
switch ($act) {
	//####################// 支付收银台 //####################//
	default:
		$pay_id = pe_dbhold($_g_id);
		$info = $db->pe_select('pay', array('pay_id'=>$pay_id, 'user_id'=>$user['user_id']));
		$payment_list = payment_list($info['pay_type']);
		$user['user_money'] = pe_num($user['user_money'], 'floor', 2);
		if (isset($_p_pesubmit)) {
			if (!$info['pay_id']) pe_apidata(array('code'=>0, 'msg'=>'订单无效'));
			if ($info['order_pstate']) pe_apidata(array('code'=>0, 'msg'=>'订单已支付'));
			//if ($info['order_type'] == 'pintuan' && !pintuan_check($info['huodong_id'], $info['pintuan_id'])) pe_apidata(array('code'=>0, 'msg'=>'拼团无效或结束'));
			$order_payment = $_p_order_payment ? pe_dbhold($_p_order_payment) : $info['order_payment'];
			if ($order_payment == 'cod' or !array_key_exists($order_payment, $payment_list)) pe_apidata(array('code'=>0, 'msg'=>'支付方式错误'));
			if ($order_payment == 'balance') {
				if ($info['order_money'] > 0) {
					if (!$_p_user_paypw) pe_apidata(array('code'=>0, 'msg'=>'请填写支付密码'));
					if ($user['user_paypw'] != md5($_p_user_paypw)) pe_apidata(array('code'=>0, 'msg'=>'支付密码错误'));
					if ($user['user_money'] < $info['order_money']) pe_apidata(array('code'=>0, 'msg'=>'账户余额不足'));				
				}
				pay_success($pay_id, $order_payment);
				$info['url'] = pay_jump($pay_id);
				pe_apidata(array('code'=>1, 'data'=>$info, 'msg'=>'支付成功'));
			}
			else {
				$info['url'] = "{$pe['host']}include/plugin/payment/{$order_payment}/pay.php?id={$pay_id}";
				pe_apidata(array('code'=>1, 'data'=>$info, 'msg'=>'提交成功，跳转中...'));
			}			
		}
		$info['payment_list'] = array_values($payment_list);
		if ($info['order_money'] == 0) {
			foreach ($info['payment_list'] as $k=>$v) {
				if ($v['payment_type'] == 'balance') {
					$info['payment_list'] = array();
					$info['payment_list'][0] = $v;
					$info['order_payment'] = $v['payment_type'];
					break;
				}
			}		
		}
		pe_fixurl(pe_url("/page/index/pay?id={$pay_id}", 'app'));
		$seo = pe_seo($menutitle='收银台');
		include(pe_tpl('pay.html'));
	break;
}
?>