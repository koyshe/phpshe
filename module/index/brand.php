<?php
$cache_brand = cache::get('brand');
switch ($act) {
	//####################// 品牌列表 //####################//
	case 'list':
		$list = $db->pe_selectall('brand', array('order by'=>'brand_order asc, brand_id desc'), '*', array(20, $_g_page));
		foreach ($list as $k=>$v) {
			$list[$k]['brand_logo'] = pe_thumb($v['brand_logo']);
			$list[$k]['brand_banner'] = pe_thumb($v['brand_banner']);
			$product_list = $db->pe_selectall('product', array('product_state'=>1, 'brand_id'=>$v['brand_id'], 'order by'=>'product_sellnum desc'), '*', array(3));
			foreach ($product_list as $kk=>$vv) {
				$product_list[$kk]['product_logo'] = pe_thumb($vv['product_logo'], 400, 400);
				$product_list[$kk]['product_money'] = product_money($vv);
			}
			$list[$k]['product_list'] = $product_list;
		}
		$info['list'] = $list;
		pe_fixurl(pe_url("/page/index/brand_list?page={$_g_page}", 'app'));
		$seo = pe_seo($menutitle='品牌专区');
		include(pe_tpl('brand_list.html'));
	break;
	//####################// 品牌详情 //####################//
	default:
		$brand_id = intval($act);
		$info = $db->pe_select('brand', array('brand_id'=>$brand_id));
		$info['brand_logo'] = pe_thumb($info['brand_logo']);
		$info['brand_banner'] = pe_thumb($info['brand_banner']);
		//列表筛选
		$sql_where .= " and `brand_id` = {$brand_id}";
		$orderby_arr = array('sellnum_desc', 'clicknum_desc', 'money_desc', 'money_asc', 'commentnum_desc');
		if (in_array($_g_orderby, $orderby_arr)) {
			$orderby = explode('_', $_g_orderby);
			$sql_where .= " order by `product_{$orderby[0]}` {$orderby[1]}";
		}
		else {
			$sql_where .= " order by `product_order` asc, `product_id` desc";
		}
		$list = $db->pe_selectall('product', $sql_where, '*', array(40, $_g_page));
		foreach ($list as $k=>$v) {
			$list[$k]['product_logo'] = pe_thumb($v['product_logo'], 400, 400);
			$list[$k]['product_money'] = product_money($v);
		}
		$info['list'] = $list;
		//当前路径
		$nowpath = category_path(0, array(pe_url('brand-list')=>'品牌专区', $info['brand_name']));
		pe_fixurl(pe_url("/page/index/brand?id={$brand_id}", 'app'));
		$seo = pe_seo($info['brand_name']);
		include(pe_tpl('brand_view.html'));
	break;
}
?>